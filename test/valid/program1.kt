/*
* Sample program 1
*/
fun main() {
	var i = 0;
	while (i < 10) {
		print(i);
		print('\n');
		i = i + 1;
	}
	if (i == 10) {
		print("i is now 10");
		print('\n');
	}
}

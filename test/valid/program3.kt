/*
* Sample program 3
*/
fun sum1(x: Int): Int {
	var res = 0;
	var a = x;
	for(i in 1..a)
		res = res + i;
	return res;
}
fun main() { // main function
	val x = 10;
	val limit = 100;
	if (x in 1..(limit-1) ){
		print("OK");
		print('\n');
	}
	var total: Int;
	total = sum1(limit);
	print("Sum is ");
	print(total);
	print('\n');
}

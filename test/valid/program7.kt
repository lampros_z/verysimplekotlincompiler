fun a():Int{
    return 5;
}

fun stepFun():Int{
    return 1;
}

fun main() {
    for(i in 1..a() step stepFun()){
        print("*");
        print('\n');
    }
}

fun main() {

    val num = 15;
    var factorial = 1f;

    for (i in 1..num) {
        factorial = factorial * i;
    }

    print("Factorial of ");
    print(num);
    print(" is: ");
    print(factorial);
    print('\n');
}
/*
* Sample program 2
*/
fun sum(i: Int): Int {
	if (i <= 0) {
		return 0;
	}
	return i + sum(i-1);
}
fun main() { // sum should be 5050
	var total: Int;
	total = sum(100);
	print("Sum is: ");
	print(total);
	print('\n');
}

fun main() {
    val rows = 5;
    var k = 0;
    var count = 0;
    var count1 = 0;

    for (i in 1..rows) {
        for (space in 1..(rows - i)) {
            print("  ");
            ++count;
        }

        while (k != (2 * i - 1)) {
            if (count <= (rows - 1)) {
                print((i + k));
                print(' ');
                ++count;
            } else {
                ++count1;
                print((i + k - 2 * count1));
                print(' ');
            }

            ++k;
        }
        k = 0;
        count = k;
        count1 = count;

        print('\n');
    }
}
fun main(){
    print(fibonacci(10));
    print('\n');
    return;
}

fun fibonacci(a:Int):Int{
    if(a == 0){
        return 0;
    }
    if(a == 1){
        return 1;
    }
    return fibonacci(a-1) + fibonacci(a-2);
}
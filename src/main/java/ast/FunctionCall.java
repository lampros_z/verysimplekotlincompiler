/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;

import java.util.List;
import java.util.ArrayList;
/**
 * A class holding all the necessary information to model a function call.
 */
public class FunctionCall extends ASTNode{
    
    private String identifier;
    private List<FunctionCallParameter> functionCallParameters;

    public FunctionCall(String identifier){
        this.identifier = identifier;
        this.functionCallParameters = new ArrayList<>();
    }

    public FunctionCall(String identifier, List<FunctionCallParameter> functionCallParameters) {
        this.identifier = identifier;
        this.functionCallParameters = functionCallParameters;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public List<FunctionCallParameter> getFunctionCallParameters() {
        return functionCallParameters;
    }

    public void setFunctionCallParameters(List<FunctionCallParameter> functionCallParameters) {
        this.functionCallParameters = functionCallParameters;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

}
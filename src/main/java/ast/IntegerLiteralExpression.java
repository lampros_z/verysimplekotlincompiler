/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;
/**
 * A class for an integer literal.
 */
public class IntegerLiteralExpression extends Expression {

    private Integer literal;

    public IntegerLiteralExpression(Integer literal) {
        this.literal = literal;
    }

    public Integer getLiteral() {
        return literal;
    }

    public void setLiteral(Integer literal) {
        this.literal = literal;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

}

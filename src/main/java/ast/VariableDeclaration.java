/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;
/**
 * A class that holds all the necessary information for a variable declaration.
 */
public class VariableDeclaration extends ASTNode{
    
    private String identifier;
    private TypeK type;

    public VariableDeclaration(String identifier){
        this.identifier = identifier;
        this.type = null;
    }

    public VariableDeclaration(String identifier, TypeK type) {
        this.identifier = identifier;
        this.type = type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public TypeK getType() {
        return type;
    }

    public void setType(TypeK type) {
        this.type = type;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

    
    
}
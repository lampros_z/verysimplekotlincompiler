/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;
/**
 * A subclass of property declaration that represents a value delclaration(e.g. val a = 0;).
 */
public class ValDeclarationAssignment extends PropertyDeclaration{

    private Expression expression;

    public ValDeclarationAssignment(VariableDeclaration variableDeclaration, Expression expression) {
        super(variableDeclaration);
        this.expression = expression;
    }


    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);

    }

}
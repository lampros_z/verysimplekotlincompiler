/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;
/**
 * The root node of the program that holds the compilation unit.
 */
public class KotlinFile extends ASTNode{

    CompUnit compUnit;

    public KotlinFile(){
        
    }

    public KotlinFile(CompUnit compUnit) {
        this.compUnit = compUnit;
    }

    public CompUnit getCompUnit() {
        return compUnit;
    }

    public void setCompUnit(CompUnit compUnit) {
        this.compUnit = compUnit;
    }
    
    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);

    }

}
/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;
/**
 * A class modeling a double literal.
 */
public class DoubleLiteralExpression extends Expression {

    private Double literal;

    public DoubleLiteralExpression(Double literal) {
        this.literal = literal;
    }

    public Double getLiteral() {
        return literal;
    }

    public void setLiteral(Double literal) {
        this.literal = literal;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

}

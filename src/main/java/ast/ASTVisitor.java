/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;

/**
 * Abstract syntax tree visitor.
 */
public interface ASTVisitor {

    void visit(KotlinFile node) throws ASTVisitorException;

	void visit(CompUnit node) throws ASTVisitorException;

	void visit(VariableDeclaration node) throws ASTVisitorException;

	void visit(TypeK node) throws ASTVisitorException;

	void visit(VarDeclaration node) throws ASTVisitorException;

	void visit(ValDeclaration node) throws ASTVisitorException;

	void visit(VarDeclarationAssignment node) throws ASTVisitorException;

	void visit(ValDeclarationAssignment node) throws ASTVisitorException;

	void visit(FunctionDeclaration node) throws ASTVisitorException;
	
	void visit(FunctionParameters node) throws ASTVisitorException;

	void visit(StringLiteralExpression node) throws ASTVisitorException;

	void visit(DoubleLiteralExpression node) throws ASTVisitorException;

	void visit(IntegerLiteralExpression node) throws ASTVisitorException;

	void visit(CharLiteralExpression node) throws ASTVisitorException;

	void visit(BooleanLiteralExpression node) throws ASTVisitorException;

	void visit(NullLiteralExpression node) throws ASTVisitorException;

	void visit(IdentifierExpression node) throws ASTVisitorException;

	void visit(ParenthesisExpression node) throws ASTVisitorException;

	void visit(BinaryExpression node) throws ASTVisitorException;

	void visit(PrefixUnaryExpression node) throws ASTVisitorException;

	void visit(PostfixUnaryExpression node) throws ASTVisitorException;

	void visit(AssignmentExpression node) throws ASTVisitorException;

	void visit(FunctionCallParameter node) throws ASTVisitorException;

	void visit(FunctionCall node) throws ASTVisitorException;

	void visit(FunctionCallExpression node) throws ASTVisitorException;

	void visit(AssignmentStatement node) throws ASTVisitorException;

	void visit(CompoundStatement node) throws ASTVisitorException;

	void visit(ExpressionStatement node) throws ASTVisitorException;

	void visit(PropertyDeclarationStatement node) throws ASTVisitorException;

	void visit(FunctionDeclarationStatement node) throws ASTVisitorException;

	void visit(IfStatement node) throws ASTVisitorException;

	void visit(IfElseStatement node) throws ASTVisitorException;

	void visit(ReturnStatement node) throws ASTVisitorException;

	void visit(ContinueStatement node) throws ASTVisitorException;

	void visit(BreakStatement node) throws ASTVisitorException;

	void visit(WhileStatement node) throws ASTVisitorException;

	void visit(DoWhileStatement node) throws ASTVisitorException;

	void visit(ForStatement node) throws ASTVisitorException;

	void visit(ForStatementWithStep node) throws ASTVisitorException;

	void visit(Parameter node) throws ASTVisitorException;

}

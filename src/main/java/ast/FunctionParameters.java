/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;

import java.util.List;
import java.util.ArrayList;

public class FunctionParameters extends ASTNode{

    private List<Parameter> parameters;

    public FunctionParameters(){
        this.parameters = new ArrayList<>();
    }

    public FunctionParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);

    }
}
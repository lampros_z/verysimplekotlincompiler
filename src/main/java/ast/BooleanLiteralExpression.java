/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;
/**
 * A class representing a boolean literal.
 */
public class BooleanLiteralExpression extends Expression{

    private boolean literal;

    public BooleanLiteralExpression(boolean literal) {
        this.literal = literal;
    }

    public boolean isLiteral() {
        return literal;
    }

    public void setLiteral(boolean literal) {
        this.literal = literal;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

    

}
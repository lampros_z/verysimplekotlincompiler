/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;
/**
 * A class for character literals.
 */
public class CharLiteralExpression extends Expression{
    
    private char literal;

    public CharLiteralExpression(char literal) {
        this.literal = literal;
    }

    public char getLiteral() {
        return literal;
    }

    public void setLiteral(char literal) {
        this.literal = literal;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);

    }

    

}
/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;

import java.util.ArrayList;
import java.util.List;
/**
 * A Compound statement is simply a class that holds a list of statements.
 */
public class CompoundStatement extends Statement {

    private List<Statement> statements;

    public CompoundStatement() {
        this.statements = new ArrayList<Statement>();
    }

    public CompoundStatement(List<Statement> statements) {
        this.statements = statements;
    }

    public List<Statement> getStatements() {
        return statements;
    }

    public void setStatements(List<Statement> statements) {
        this.statements = statements;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

}

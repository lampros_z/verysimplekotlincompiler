/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;

import org.objectweb.asm.Type;
/**
 * A class that holds all the necessary information for a type.
 */
public class TypeK extends ASTNode{
    /**
     * org.objectweb.asm.Type class
     */
    private Type type;
    /**
     * If it is nullable or not.
     */
    private boolean nullable;

    public TypeK(Type type, boolean nullable) {
        this.type = type;
        this.nullable = nullable;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);

    }

}
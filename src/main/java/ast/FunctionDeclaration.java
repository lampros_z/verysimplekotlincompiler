/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;
/**
 * A class that holds all the necessary information of a function declaration statement.
 */
public class FunctionDeclaration extends ASTNode{
    
    private String identifier;
    private TypeK returnType;
    private FunctionParameters functionParamaters;
    private Statement statement;


    public FunctionDeclaration(String identifier, FunctionParameters functionParamaters, Statement statement) {
        this.identifier = identifier;
        this.returnType = null;
        this.functionParamaters = functionParamaters;
        this.statement = statement;
    }

    public FunctionDeclaration(String identifier, TypeK returnType, FunctionParameters functionParamaters, Statement statement) {
        this.identifier = identifier;
        this.returnType = returnType;
        this.functionParamaters = functionParamaters;
        this.statement = statement;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public TypeK getReturnType() {
        return returnType;
    }

    public void setReturnType(TypeK returnType) {
        this.returnType = returnType;
    }


    public FunctionParameters getFunctionParamaters() {
        return functionParamaters;
    }

    public void setFunctionParamaters(FunctionParameters functionParamaters) {
        this.functionParamaters = functionParamaters;
    }
    
    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

}
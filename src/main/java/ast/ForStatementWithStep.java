/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;
/**
 * A class holding all the necessary information to model a for loop.
 */
public class ForStatementWithStep extends Statement{

    private VariableDeclaration variableDeclaration;
    private Expression expression;
    private Expression stepExpression;
    private Statement statement;

    public ForStatementWithStep(VariableDeclaration variableDeclaration, Expression expression,
        Expression stepExpression, Statement statement) {
        this.variableDeclaration = variableDeclaration;
        this.expression = expression;
        this.stepExpression = stepExpression;
        this.statement = statement;
    }

    public VariableDeclaration getVariableDeclaration() {
        return variableDeclaration;
    }

    public void setVariableDeclaration(VariableDeclaration variableDeclaration) {
        this.variableDeclaration = variableDeclaration;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public Expression getStepExpression() {
        return stepExpression;
    }

    public void setStepExpression(Expression stepExpression) {
        this.stepExpression = stepExpression;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

}
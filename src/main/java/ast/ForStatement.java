/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;
/**
 * A class holding all the necessary information to model a for loop.
 */
public class ForStatement extends Statement {

    private VariableDeclaration variableDeclaration;
    private Expression expression;
    private Statement statement;

    public ForStatement(VariableDeclaration variableDeclaration, Expression expression, Statement statement) {
        this.variableDeclaration = variableDeclaration;
        this.expression = expression;
        this.statement = statement;
    }

    public VariableDeclaration getVariableDeclaration() {
        return variableDeclaration;
    }

    public void setVariableDeclaration(VariableDeclaration variableDeclaration) {
        this.variableDeclaration = variableDeclaration;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }
    
}
/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;
/*
 * An enumeration for all the supported operators of the language. 
 * */
public enum Operator {

    PLUS("+"),
    MINUS("-"),
    MULTIPLY("*"),
    DIVISION("/"),
    MODULO("%"),
    EQUAL("=="),
    NOTEQUAL("!="),
    LESS("<"),
    GREATER(">"),
    LESSEQ("<="),
    GREATEREQ(">="),
    AND("&&"),
    OR("||"),
    NOT("!"),
    PLUSPLUS("++"),
    MINUSMINUS("--"),
    RANGE_TO(".."),
    IN("in"),
    NOT_IN("!in"),
    IS("is"),
    DOWN_TO("downTo")
    ;

    private String type;

    private Operator(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return type;
    }

    public boolean isPrefixUnary() {
        return this.equals(Operator.MINUS)
            || this.equals(Operator.PLUSPLUS)
            || this.equals(Operator.MINUSMINUS)
            || this.equals(Operator.NOT)
        ;
    }

    public boolean isPostfixUnary() {
        return this.equals(Operator.PLUSPLUS)
            || this.equals(Operator.MINUSMINUS)
        ;
    }

    public boolean isRelational() {
        return this.equals(Operator.EQUAL) || this.equals(Operator.NOTEQUAL)
                || this.equals(Operator.GREATER) || this.equals(Operator.GREATEREQ)
                || this.equals(Operator.LESS) || this.equals(Operator.LESSEQ);
    }

    public boolean isLogical(){
        return this.equals(Operator.AND) || this.equals(Operator.OR);
    }

    public boolean isRange(){
        return this.equals(Operator.RANGE_TO) || this.equals(Operator.DOWN_TO);
    }

    public boolean isContains(){
        return this.equals(Operator.IN);
    }

    public boolean isNotContains(){
        return this.equals(Operator.NOT_IN);
    }

    public boolean isIncremental(){
        return this.equals(Operator.PLUSPLUS) || this.equals(Operator.MINUSMINUS);
    }

}

/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package ast;

import java.util.List;
import java.util.ArrayList;
/**
 * A class that hold a list for all function declarations of the program as well as all property
 * declarations. As the name suggests it models the compilation unit of the program.
 */
public class CompUnit extends ASTNode {

    private List<PropertyDeclaration> propertyDeclarations;
    private List<FunctionDeclaration> functionDeclarations;

    public CompUnit(){
        this.propertyDeclarations = new ArrayList<>();
        this.functionDeclarations = new ArrayList<>();
    }

    public CompUnit(List<PropertyDeclaration> propertyDeclarations, List<FunctionDeclaration> functionDeclarations) {
        this.propertyDeclarations = propertyDeclarations;
        this.functionDeclarations = functionDeclarations;
    }

    public List<PropertyDeclaration> getPropertyDeclarations() {
        return propertyDeclarations;
    }

    public void setPropertyDeclarations(List<PropertyDeclaration> propertyDeclarations) {
        this.propertyDeclarations = propertyDeclarations;
    }

    public List<FunctionDeclaration> getFunctionDeclarations() {
        return functionDeclarations;
    }

    public void setFunctionDeclarations(List<FunctionDeclaration> functionDeclarations) {
        this.functionDeclarations = functionDeclarations;
    }


    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

}

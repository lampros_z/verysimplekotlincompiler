package ast;

/**
 * Compilers 2020 Harokopio University of Athens, Dept. of Informatics and
 * Telematics.
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * A class modeling an assignment expression(e.g. a = a +1;) of a kotlin program.
 * 
 */
public class AssignmentStatement extends Statement {

    private Expression expression1;
    private Expression expression2;

    public AssignmentStatement(Expression expression1, Expression expression2) {
        this.expression1 = expression1;
        this.expression2 = expression2;
    }
    
    public Expression getExpression1() {
        return expression1;
    }

    public void setExpression1(Expression expression1) {
        this.expression1 = expression1;
    }

    public Expression getExpression2() {
        return expression2;
    }

    public void setExpression2(Expression expression2) {
        this.expression2 = expression2;
    }


    @Override
    public void accept(ASTVisitor visitor) throws ASTVisitorException {
        visitor.visit(this);
    }

}

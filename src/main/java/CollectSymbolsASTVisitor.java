import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import org.objectweb.asm.Type;
import ast.ASTUtils;
import ast.ASTVisitor;
import ast.ASTVisitorException;
import ast.AssignmentExpression;
import ast.AssignmentStatement;
import ast.BinaryExpression;
import ast.BooleanLiteralExpression;
import ast.BreakStatement;
import ast.CharLiteralExpression;
import ast.CompUnit;
import ast.CompoundStatement;
import ast.ContinueStatement;
import ast.DoWhileStatement;
import ast.DoubleLiteralExpression;
import ast.ExpressionStatement;
import ast.ForStatement;
import ast.ForStatementWithStep;
import ast.FunctionCall;
import ast.FunctionCallExpression;
import ast.FunctionCallParameter;
import ast.FunctionDeclaration;
import ast.FunctionDeclarationStatement;
import ast.FunctionParameters;
import ast.IdentifierExpression;
import ast.IfElseStatement;
import ast.IfStatement;
import ast.IntegerLiteralExpression;
import ast.KotlinFile;
import ast.NullLiteralExpression;
import ast.Parameter;
import ast.ParenthesisExpression;
import ast.PostfixUnaryExpression;
import ast.PrefixUnaryExpression;
import ast.PropertyDeclaration;
import ast.PropertyDeclarationStatement;
import ast.ReturnStatement;
import ast.Statement;
import ast.StringLiteralExpression;
import ast.TypeK;
import ast.ValDeclaration;
import ast.ValDeclarationAssignment;
import ast.VarDeclaration;
import ast.VarDeclarationAssignment;
import ast.VariableDeclaration;
import ast.WhileStatement;
import symbol.FunctionInfo;
import symbol.FunctionUtils;
import symbol.Identifier;
import symbol.IdentifierType;
import symbol.Info;
import symbol.ParameterInfo;
import symbol.SymbolTable;
import symbol.VariableInfo;

/**
 * Compilers 2020 Harokopio University of Athens, Dept. of Informatics and
 * Telematics.
 * @teacher michail@hua.gr
 * @author it21622
 */

 /**
  * CollectSymbolsASTVisitor: A visitor class that collects symbols(variables, function identifiers).
  * This ASTVisitor is responsible for semantic check also. It checks wheteher an identifer has been redacler
  * or if the function parameters of a fucntion are incorrect. Some standard library functions such as print are
  * declared at the primary node (KotlinFile) of the AST.
  * WARNING: It is not a thread safe class.
  */
public class CollectSymbolsASTVisitor implements ASTVisitor {
    
    /**
     * A temporary field for org.objectweb.asm.Type class.
     */
    private Type type;
    /**
     * A temporary field for FunctionParameters.
     */
    private FunctionParameters functionParameters;
    /**
     * Set data structure is used in order to make sure that adding a unique identifier 
     * takes O(1) time. In other words, redeclared identifiers in function parameters or in a statement
     * will be recocnised in constant time.
     */
    private Set<String> parametersIdentifiers;
    /**
     * Used to check if there is an illegal mix of named with positional parameters.
     * False key value represents positional parameter.
     * True key value represents a named parameter.
     * The Integer value represents the count for the positional and named parameters.
     */
    Map<Boolean, Integer> namedParameterMappings;
    /**
     * A stack useful when the visitor encounters nested function calls.
     * The boolean value declares that the visitor is in function call state.
     * When a nested function call [ e.g. foo( a = bar() ) ] is active this stack ensures that
     * the last activation record (nth record) will not alter the function call state to false but it will alter
     * the state at the top of the stack hence the first or second or nth - 1 state will remain true. 
     */
    private final Deque<Boolean> functionCallStack;

    public CollectSymbolsASTVisitor(){
        parametersIdentifiers = new HashSet<>();
        namedParameterMappings = new HashMap<>();
        functionCallStack = new ArrayDeque<>();
    }

    @Override
    public void visit(KotlinFile node) throws ASTVisitorException {
        node.getCompUnit().accept(this);

        SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);

        String id = "print";
        int identifierType = IdentifierType.FUNCTION.value();
        String BoolParam = "(" + Type.BOOLEAN_TYPE.toString() + ")";
        String IntParam = "(" + Type.INT_TYPE.toString() + ")";
        String DoubleParam = "(" + Type.DOUBLE_TYPE.toString() + ")";
        String CharParam = "(" + Type.CHAR_TYPE.toString() + ")";
        String StringParam = "(" + Type.getType(String.class).toString() + ")";

        String signBool = id + BoolParam;
        String signInt = id + IntParam;
        String signDouble = id + DoubleParam;
        String signChar = id + CharParam;
        String signString = id + StringParam;

        String descBool = BoolParam + Type.VOID_TYPE.toString();
        String descInt = IntParam + Type.VOID_TYPE.toString();
        String descDouble = DoubleParam + Type.VOID_TYPE.toString();
        String descChar = CharParam + Type.VOID_TYPE.toString();
        String descString = StringParam + Type.VOID_TYPE.toString();

        Map<String, ParameterInfo> map1 = new HashMap<>();
        Map<String, ParameterInfo> map2 = new HashMap<>();
        Map<String, ParameterInfo> map3 = new HashMap<>();
        Map<String, ParameterInfo> map4 = new HashMap<>();
        Map<String, ParameterInfo> map5 = new HashMap<>();
        map1.put("val", new ParameterInfo(Type.BOOLEAN_TYPE));
        map2.put("val", new ParameterInfo(Type.INT_TYPE));
        map3.put("val", new ParameterInfo(Type.DOUBLE_TYPE));
        map4.put("val", new ParameterInfo(Type.CHAR_TYPE));
        map5.put("val", new ParameterInfo(Type.getType(String.class)));

        Identifier identifier = new Identifier(id, identifierType, signBool);
        Identifier identifier1 = new Identifier(id, identifierType, signInt);
        Identifier identifier2 = new Identifier(id, identifierType, signDouble);
        Identifier identifier3 = new Identifier(id, identifierType, signChar);
        Identifier identifier4 = new Identifier(id, identifierType, signString);

        FunctionInfo functionInfo = new FunctionInfo(descBool, map1);
        FunctionInfo functionInfo1 = new FunctionInfo(descInt, map2);
        FunctionInfo functionInfo2 = new FunctionInfo(descDouble, map3);
        FunctionInfo functionInfo3 = new FunctionInfo(descChar, map4);
        FunctionInfo functionInfo4 = new FunctionInfo(descString, map5);

        Info info = symbolTable.put(identifier, functionInfo);
        Info info1 = symbolTable.put(identifier1, functionInfo1);
        Info info2 = symbolTable.put(identifier2, functionInfo2);
        Info info3 = symbolTable.put(identifier3, functionInfo3);
        Info info4 = symbolTable.put(identifier4, functionInfo4);

        if (info != null)
            ASTUtils.error(node, "Illegal overload of print function");

        if (info1 != null)
            ASTUtils.error(node, "Illegal overload of print function");

        if (info2 != null)
            ASTUtils.error(node, "Illegal overload of print function");

        if (info3 != null)
            ASTUtils.error(node, "Illegal overload of print function");

        if (info4 != null)
            ASTUtils.error(node, "Illegal overload of print function");
    }

    @Override
    public void visit(CompUnit node) throws ASTVisitorException {
        for (PropertyDeclaration propertyDeclaration : node.getPropertyDeclarations()) {
            propertyDeclaration.accept(this);
        }

        for (FunctionDeclaration functionDeclaration : node.getFunctionDeclarations()) {
            functionDeclaration.accept(this);
        }
    }

    @Override
    public void visit(VariableDeclaration node) throws ASTVisitorException {

        SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);

        String id = node.getIdentifier();
        Identifier identifier = new Identifier(id, IdentifierType.VARIABLE.value());

        VariableInfo info = new VariableInfo();

        if (symbolTable.lookupOnlyInTop(identifier) != null)
            ASTUtils.error(node, "Redeclared identifier");

        /* Necessary */
        type = null;
        TypeK typeK = node.getType();
        if (typeK != null) {
            typeK.accept(this);
        }
        info.setType(type);
        symbolTable.put(identifier, info);
    }

    @Override
    public void visit(TypeK node) throws ASTVisitorException {
        type = node.getType();
    }

    @Override
    public void visit(VarDeclaration node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);
    }

    @Override
    public void visit(ValDeclaration node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);
    }

    @Override
    public void visit(VarDeclarationAssignment node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);
        node.getExpression().accept(this);
    }

    @Override
    public void visit(ValDeclarationAssignment node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);
        node.getExpression().accept(this);
    }

    @Override
    public void visit(FunctionDeclaration node) throws ASTVisitorException {
        SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);

        /* Necessary */
        String id = node.getIdentifier();
        int identifierType = IdentifierType.FUNCTION.value();
        node.getFunctionParamaters().accept(this);

        // Aqquire function signature
        String functionSignature = FunctionUtils.aqquireSingnature(id, functionParameters);

        type = null;
        if (node.getReturnType() != null)
            node.getReturnType().accept(this);
        // Aqquire function type descriptor
        String methodDescriptor = FunctionUtils.aqquireMethodDescriptor(functionParameters, type);
        Map<String, ParameterInfo> formalParametersMappings = FunctionUtils.getFormalParametersMappings(functionParameters);
        FunctionInfo info = new FunctionInfo(methodDescriptor, formalParametersMappings);

        node.getStatement().accept(this);

        // Create identifier object
        Identifier identifier = new Identifier(id, identifierType, functionSignature);
        // check overloading
        /*
         * fun main(){ var a = 5; fun a(b:Int){ println(b); } a(a); } -> This is a legal
         * program
         */

        if (symbolTable.put(identifier, info) != null)
            ASTUtils.error(node, "Conflicting overloads");

    }

    @Override
    public void visit(FunctionParameters node) throws ASTVisitorException {
        functionParameters = new FunctionParameters();
        parametersIdentifiers.clear();
        List<Parameter> fpList  = node.getParameters();
        ListIterator<Parameter> iterator = fpList.listIterator(fpList.size());
        while(iterator.hasPrevious()){
            iterator.previous().accept(this);
        }
    }

    @Override
    public void visit(StringLiteralExpression node) throws ASTVisitorException {
        /* Nothing */
    }

    @Override
    public void visit(DoubleLiteralExpression node) throws ASTVisitorException {
        /* Nothing */
    }

    @Override
    public void visit(IntegerLiteralExpression node) throws ASTVisitorException {
        /* Nothing */
    }

    @Override
    public void visit(CharLiteralExpression node) throws ASTVisitorException {
        /* Nothing */
    }

    @Override
    public void visit(BooleanLiteralExpression node) throws ASTVisitorException {
        /* Nothing */
    }

    @Override
    public void visit(NullLiteralExpression node) throws ASTVisitorException {
        /* Nothing */
    }

    @Override
    public void visit(IdentifierExpression node) throws ASTVisitorException {
        /* Nothing */
    }

    @Override
    public void visit(ParenthesisExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(BinaryExpression node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        node.getExpression2().accept(this);
    }

    @Override
    public void visit(PrefixUnaryExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(PostfixUnaryExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(AssignmentExpression node) throws ASTVisitorException {
        if (functionCallStack.peekFirst() != null && functionCallStack.peekFirst()) {
            if (!(node.getExpression1() instanceof IdentifierExpression)) {
                ASTUtils.error(node.getExpression1(),
                        "Assignments are not expressions, and only expressions are allowed in this context");
            }
            IdentifierExpression idexp = (IdentifierExpression) node.getExpression1();
            String identifier = idexp.getIdentifier();
            if (!parametersIdentifiers.add(identifier))
                ASTUtils.error(idexp, "An argument is already passed for this parameter");
        } else {
            if (!(node.getExpression1() instanceof IdentifierExpression)) {
                ASTUtils.error(node.getExpression1(), "Variable expected!");
            }
            node.getExpression2().accept(this);
        }
    }

    @Override
    public void visit(FunctionCallParameter node) throws ASTVisitorException {
        if (node.getExpression() instanceof AssignmentExpression)
            namedParameterMappings.replace(true, ((namedParameterMappings.get(true)) + 1));
        else
            namedParameterMappings.replace(false, ((namedParameterMappings.get(false)) + 1));
        node.getExpression().accept(this);
    }

    @Override
    public void visit(FunctionCall node) throws ASTVisitorException {
        functionCallStack.push(true);;
        List<FunctionCallParameter> fplist = node.getFunctionCallParameters();
        int listSize = fplist.size() - 1;

        if (listSize >= 0) {
            namedParameterMappings.clear();
            parametersIdentifiers.clear();
            namedParameterMappings.put(true, 0);
            namedParameterMappings.put(false, 0);

            for (int i = listSize; i > 0; i--) {
                fplist.get(i).accept(this);
            }

            fplist.get(0).accept(this);

            if (namedParameterMappings.get(true) > 0) {
                if (namedParameterMappings.get(false) > 0) {
                    ASTUtils.error(node, "Mixing named and positioned arguments is not allowed");
                }
            }
        }
        // set named parameters list mappings
        functionCallStack.pop();
    }

    @Override
    public void visit(FunctionCallExpression node) throws ASTVisitorException {
        node.getFunctionCall().accept(this);
    }

    @Override
    public void visit(AssignmentStatement node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        node.getExpression2().accept(this);
    }

    @Override
    public void visit(CompoundStatement node) throws ASTVisitorException {
        for (Statement st : node.getStatements()) {
            st.accept(this);
        }
    }

    @Override
    public void visit(ExpressionStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(PropertyDeclarationStatement node) throws ASTVisitorException {
        node.getPropertyDeclaration().accept(this);

    }

    @Override
    public void visit(FunctionDeclarationStatement node) throws ASTVisitorException {
        node.getFunctionDeclaration().accept(this);
    }

    @Override
    public void visit(IfStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
        node.getStatement().accept(this);
    }

    @Override
    public void visit(IfElseStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
        node.getStatement1().accept(this);
        node.getStatement2().accept(this);
    }

    @Override
    public void visit(ReturnStatement node) throws ASTVisitorException {
        if (node.getExpression() != null)
            node.getExpression().accept(this);
    }

    @Override
    public void visit(ContinueStatement node) throws ASTVisitorException {
        /* Nothing */
    }

    @Override
    public void visit(BreakStatement node) throws ASTVisitorException {
        /* Nothing */
    }

    @Override
    public void visit(WhileStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
        node.getStatement().accept(this);
    }

    @Override
    public void visit(DoWhileStatement node) throws ASTVisitorException {
        node.getStatement().accept(this);
        node.getExpression().accept(this);
    }

    @Override
    public void visit(ForStatement node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);
        node.getExpression().accept(this);
        node.getStatement().accept(this);
    }

    @Override
    public void visit(ForStatementWithStep node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);
        node.getExpression().accept(this);
        node.getStepExpression().accept(this);
        node.getStatement().accept(this);
    }

    @Override
    public void visit(Parameter node) throws ASTVisitorException {
        String identifier = node.getIdentifier();
        // fun add(a:Int, a:Int) -> Conflicting declarations
        if (!parametersIdentifiers.add(identifier))
            throw new ASTVisitorException("Conflicting declarations : " + identifier);
        type = null;
        if (node.getType() != null) {
            node.getType().accept(this);
            functionParameters.getParameters()
                    .add(new Parameter(identifier, new TypeK(type, node.getType().isNullable())));
        } else
            throw new ASTVisitorException("Invalid function declaration");
    }

}
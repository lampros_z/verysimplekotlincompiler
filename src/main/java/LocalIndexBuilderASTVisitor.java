import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

import ast.ASTUtils;
import ast.ASTVisitor;
import ast.ASTVisitorException;
import ast.AssignmentExpression;
import ast.AssignmentStatement;
import ast.BinaryExpression;
import ast.BooleanLiteralExpression;
import ast.BreakStatement;
import ast.CharLiteralExpression;
import ast.CompUnit;
import ast.CompoundStatement;
import ast.ContinueStatement;
import ast.DoWhileStatement;
import ast.DoubleLiteralExpression;
import ast.ExpressionStatement;
import ast.ForStatement;
import ast.ForStatementWithStep;
import ast.FunctionCall;
import ast.FunctionCallExpression;
import ast.FunctionCallParameter;
import ast.FunctionDeclaration;
import ast.FunctionDeclarationStatement;
import ast.FunctionParameters;
import ast.IdentifierExpression;
import ast.IfElseStatement;
import ast.IfStatement;
import ast.IntegerLiteralExpression;
import ast.KotlinFile;
import ast.NullLiteralExpression;
import ast.Parameter;
import ast.ParenthesisExpression;
import ast.PostfixUnaryExpression;
import ast.PrefixUnaryExpression;
import ast.PropertyDeclaration;
import ast.PropertyDeclarationStatement;
import ast.ReturnStatement;
import ast.Statement;
import ast.StringLiteralExpression;
import ast.TypeK;
import ast.ValDeclaration;
import ast.ValDeclarationAssignment;
import ast.VarDeclaration;
import ast.VarDeclarationAssignment;
import ast.VariableDeclaration;
import ast.WhileStatement;
import symbol.LocalIndexPool;
/**
 * Compilers 2020 Harokopio University of Athens, Dept. of Informatics and
 * Telematics.
 * @teacher michail@hua.gr
 * @author it21622@hua.gr
 */

/**
 * Build index pools for each node of the AST. Use a stack to maintain the
 * history while building.
 */
public class LocalIndexBuilderASTVisitor implements ASTVisitor {

	private final Deque<LocalIndexPool> env;

	public LocalIndexBuilderASTVisitor() {
		env = new ArrayDeque<>();
	}


    @Override
    public void visit(KotlinFile node) throws ASTVisitorException {
        env.push(new LocalIndexPool());
		ASTUtils.setLocalIndexPool(node, env.element());
        node.getCompUnit().accept(this);
        env.pop();
    }

    @Override
    public void visit(CompUnit node) throws ASTVisitorException {
        
        for (FunctionDeclaration functionDeclaration : node.getFunctionDeclarations()) {
			functionDeclaration.accept(this);
		}
        
        for (PropertyDeclaration propertyDeclaration : node.getPropertyDeclarations()) {
			propertyDeclaration.accept(this);
		}
    }

    @Override
    public void visit(VariableDeclaration node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getType() != null)
            node.getType().accept(this);
    }

    @Override
    public void visit(TypeK node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        
    }

    @Override
    public void visit(VarDeclaration node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getVariableDeclaration().accept(this);
    }

    @Override
    public void visit(ValDeclaration node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getVariableDeclaration().accept(this);
    }

    @Override
    public void visit(VarDeclarationAssignment node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getVariableDeclaration().accept(this);
        node.getExpression().accept(this);
    }

    @Override
    public void visit(ValDeclarationAssignment node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getVariableDeclaration().accept(this);
        node.getExpression().accept(this);
    }

    @Override
    public void visit(FunctionDeclaration node) throws ASTVisitorException {
        env.push(new LocalIndexPool());
		ASTUtils.setLocalIndexPool(node, env.element());
        node.getFunctionParamaters().accept(this);
		if (node.getReturnType() != null)
			node.getReturnType().accept(this);
        node.getStatement().accept(this);
        env.pop();
    }

    @Override
    public void visit(FunctionParameters node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        int listSize = node.getParameters().size() - 1;

		if (listSize >= 0) {

			for (int i = listSize; i > 0; i--) {
				node.getParameters().get(i).accept(this);
				System.out.print(", ");
			}

			node.getParameters().get(0).accept(this);
		}

    }

    @Override
    public void visit(StringLiteralExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        

    }

    @Override
    public void visit(DoubleLiteralExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        

    }

    @Override
    public void visit(IntegerLiteralExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        

    }

    @Override
    public void visit(CharLiteralExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        

    }

    @Override
    public void visit(BooleanLiteralExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(NullLiteralExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        

    }

    @Override
    public void visit(IdentifierExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(ParenthesisExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression().accept(this);
    }

    @Override
    public void visit(BinaryExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression1().accept(this);
		node.getExpression2().accept(this);
    }

    @Override
    public void visit(PrefixUnaryExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression().accept(this);
    }

    @Override
    public void visit(PostfixUnaryExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression().accept(this);
    }

    @Override
    public void visit(AssignmentExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression1().accept(this);
		node.getExpression2().accept(this);
    }

    @Override
    public void visit(FunctionCallParameter node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression().accept(this);
    }

    @Override
    public void visit(FunctionCall node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        List<FunctionCallParameter> fplist = node.getFunctionCallParameters();
		int listSize = fplist.size() - 1;

		if (listSize >= 0) {

			for (int i = listSize; i > 0; i--) {
				fplist.get(i).accept(this);
			}

			fplist.get(0).accept(this);
		}
    }

    @Override
    public void visit(FunctionCallExpression node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getFunctionCall().accept(this);
    }

    @Override
    public void visit(AssignmentStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression1().accept(this);
		node.getExpression2().accept(this);
    }

    @Override
    public void visit(CompoundStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        for (Statement s : node.getStatements()) {
			s.accept(this);
		}
    }

    @Override
    public void visit(ExpressionStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression().accept(this);
    }

    @Override
    public void visit(PropertyDeclarationStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getPropertyDeclaration().accept(this);
    }

    @Override
    public void visit(FunctionDeclarationStatement node) throws ASTVisitorException {
        node.getFunctionDeclaration().accept(this);
    }

    @Override
    public void visit(IfStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression().accept(this);
		node.getStatement().accept(this);
    }

    @Override
    public void visit(IfElseStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression().accept(this);
		node.getStatement1().accept(this);
		node.getStatement2().accept(this);
    }

    @Override
    public void visit(ReturnStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getExpression() != null)
            node.getExpression().accept(this);
    }

    @Override
    public void visit(ContinueStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(BreakStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
    }

    @Override
    public void visit(WhileStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getExpression().accept(this);
		node.getStatement().accept(this);
    }

    @Override
    public void visit(DoWhileStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
		node.getStatement().accept(this);
        node.getExpression().accept(this);
    }

    @Override
    public void visit(ForStatement node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
		node.getVariableDeclaration().accept(this);
		node.getExpression().accept(this);
		node.getStatement().accept(this);
    }

    @Override
    public void visit(ForStatementWithStep node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        node.getVariableDeclaration().accept(this);
		node.getExpression().accept(this);
		node.getStepExpression().accept(this);
		node.getStatement().accept(this);
    }

    @Override
    public void visit(Parameter node) throws ASTVisitorException {
        ASTUtils.setLocalIndexPool(node, env.element());
        if (node.getType() != null)
            node.getType().accept(this);
    }

}
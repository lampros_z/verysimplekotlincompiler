
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.util.TraceClassVisitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ast.ASTNode;
import ast.ASTVisitor;
import org.apache.commons.lang3.text.WordUtils;

/**
 * Compilers 2020 Harokopio University of Athens, Dept. of Informatics and
 * Telematics.
 * @teacher michail@hua.gr
 * @author it21622@hua.gr
 */
public class Compiler {

    private static final Logger LOGGER = LoggerFactory.getLogger(Compiler.class);

    public static void main(String[] args) {
        if (args.length == 0) {
            LOGGER.info("Usage : java Compiler [ --encoding <name> ] <inputfile(s)>");
        } else {
            Map<String, Boolean> usedClasses = new HashMap<>();
            int firstFilePos = 0;
            String encodingName = "UTF-8";
            if (args[0].equals("--encoding")) {
                firstFilePos = 2;
                encodingName = args[1];
                try {
                    java.nio.charset.Charset.forName(encodingName); // Side-effect: is encodingName valid?
                } catch (Exception e) {
                    LOGGER.error("Invalid encoding '" + encodingName + "'");
                    return;
                }
            }
            for (int i = firstFilePos; i < args.length; i++) {
                VerySimpleKotlinLexer scanner = null;
                try {
                    String fileName = args[i];
                    java.io.FileInputStream stream = new java.io.FileInputStream(fileName);
                    LOGGER.info("Scanning file " + fileName);
                    java.io.Reader reader = new java.io.InputStreamReader(stream, encodingName);
                    scanner = new VerySimpleKotlinLexer(reader);

                    // parse
                    parser p = new parser(scanner);
                    ASTNode kotlinFile = (ASTNode) p.parse().value;
                    LOGGER.info("Constructed AST");

                    // keep global instance of program
                    Registry.getInstance().setRoot(kotlinFile);

                    // build symbol table
                    LOGGER.info("Building system table");
                    ASTVisitor symTableVisitor = new SymTableBuilderASTVisitor();
                    kotlinFile.accept(symTableVisitor);
                    LOGGER.info("Building local variables index");
                    kotlinFile.accept(new LocalIndexBuilderASTVisitor());


                    // construct types
                    LOGGER.info("Semantic check");
                    ASTVisitor collectSymbolsVisitor = new CollectSymbolsASTVisitor();
                    kotlinFile.accept(collectSymbolsVisitor);
                    ASTVisitor collectTypesVisitor = new CollectTypesASTVisitor();
                    kotlinFile.accept(collectTypesVisitor);
                    LOGGER.info("Collecting all indexes");
                    kotlinFile.accept(new CollectIndexesASTVisitor());

                    // print program
                    LOGGER.info("Input:");
                    ASTVisitor printVisitor = new PrintASTVisitor();
                    kotlinFile.accept(printVisitor);
                    
                    // convert to java bytecode
                    LOGGER.info("Bytecode:");
                    String className = WordUtils.capitalizeFully(fileName.substring(0, fileName.indexOf(".")));
                    BytecodeGeneratorASTVisitor bytecodeVisitor = new BytecodeGeneratorASTVisitor(className);
                    kotlinFile.accept(bytecodeVisitor);
                    ReloadingClassLoader rcl = new ReloadingClassLoader(ClassLoader.getSystemClassLoader());
                    
                    Map<String, byte[]> map = bytecodeVisitor.getAllNestedClassNodes();
                    for(String key : map.keySet()){
                        LOGGER.info("Generating class " + key + ".class");
                        byte[] bytecode = map.get(key);
                        FileOutputStream mainFos = new FileOutputStream(key+".class");
                        mainFos.write(bytecode);
                        mainFos.close();
                        rcl.register(key, bytecode);
                        usedClasses.put(key + ".class", true);
                    }
                    
                    ClassNode cn = bytecodeVisitor.getClassNode();

                    ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS + ClassWriter.COMPUTE_FRAMES);
                    TraceClassVisitor cv = new TraceClassVisitor(cw, new PrintWriter(System.out));
                    cn.accept(cv);
                    // get code
                    byte code[] = cw.toByteArray();

                    // update to file
                    LOGGER.info("Writing class to file"+ className +".class");
                    FileOutputStream fos = new FileOutputStream(className + ".class");
                    fos.write(code);
                    fos.close();
                    LOGGER.info("Compilation done");

                    // instantiate class
                    LOGGER.info("Loading class "+className+".class");
                    rcl.register(className, code);
                    Class<?> programClass = rcl.loadClass(className);

                    // run main method
                    Method method = programClass.getMethod("main", String[].class);
                    String[] params = null;
                    LOGGER.info("Executing");
                    method.invoke(null, (Object) params);
                    usedClasses.put(className + ".class", true);
                } catch (java.io.FileNotFoundException e) {
                    LOGGER.error("File not found : \"" + args[i] + "\"");
                } catch (java.io.IOException e) {
                    LOGGER.error("IO error scanning file \"" + args[i] + "\"");
                    LOGGER.error(e.toString());
                } catch (Exception e) {
                    LOGGER.error(e.getMessage());
                }
            }
            //purge unwanted class files
            File dir = new File(".");
            File[] filesList = dir.listFiles();
            for (File file : filesList) {
                if (file.isFile() && usedClasses.size() > 0 ) {
                    if(file.getName().contains(".class") && usedClasses.get(file.getName()) == null ){
                        LOGGER.info("Purging unwanted class file: " + file.getName());
                        file.delete();
                    }
                }
            }
    
        }
    }

}

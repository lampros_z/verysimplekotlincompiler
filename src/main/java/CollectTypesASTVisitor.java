
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import org.objectweb.asm.Type;
import ast.*;
import symbol.FunctionInfo;
import symbol.FunctionUtils;
import symbol.Identifier;
import symbol.IdentifierType;
import symbol.Info;
import symbol.SymbolTable;
import symbol.VariableInfo;
import types.TypeException;
import types.TypeUtils;

/**
 * Compilers 2020 Harokopio University of Athens, Dept. of Informatics and
 * Telematics.
 * @teacher michail@hua.gr
 * @author it21622
 */

 /**
  * CollectTypesASTVisitor: A visitor class that collects types(from variables, function return type).
  * This ASTVisitor is responsible for semantic check also. It checks wheteher an assignment expression is valid
  * or if the function parameters of a fucntion are incorrect.
  * WARNING: It is not a thread safe class.
  */
public class CollectTypesASTVisitor implements ASTVisitor{
    /**
     * Used in var/val declaration node to check if the variable identifier has 
     * type annotation or a value.
     */
    private boolean errorstate;
     /**
     * A stack useful when the visitor encounters nested function calls.
     * The boolean value declares that the visitor is in function call state.
     * When a nested function call [ e.g. foo( a = bar() ) ] is active this stack ensures that
     * the last activation record (nth record) will not alter the function call state to false but it will alter
     * the state at the top of the stack hence the first or second or nth - 1 state will remain true. 
     */
    private final Deque<Boolean> functionCallStack;
    /**
     * A stack useful when the visitor encounters nested function declarations.
     * The boolean value declares that the visitor is in function declaration state.
     * When a nested function declaration [ e.g. fun foo(){fun bar(){}} ] is active this stack ensures that
     * the last activation record (nth record) will not alter the function declaration state to false but it will alter
     * the state at the top of the stack hence the first or second or nth - 1 state will remain true. 
     */
    private Deque<Boolean> functionDeclaration;
    /**
     * A stack useful when the visitor encounters nested function declarations.
     * The List<Parameter> data strucuture holds the function parameters.
     * When needed, pop() operation is used to retrieve the data and store them to symbol table.
     */
    private Deque<List<Parameter>> functionParameters;
    /**
     * A stack useful when the visitor encounters nested loop statements(for, while, do-while).
     * The boolean value declares that the visitor is inside loop statements.
     * When inside nested loops [ e.g. for(){for(){}} ] this stack ensures that
     * the last activation record (nth record) will not alter the  state to false but it will alter
     * the state at the top of the stack hence the first or second or nth - 1 state will remain unmodified. 
     */
    private Deque<Boolean> insideLoop;
    /**
     * Temporary field for function return type.
     */
    private Type funcReturnType;
    /**
     * A mapping between a function parameter and its type(e.g. a:Int). 
     */
    private Map<String, Type> namedParameters;
    /**
     * A stack holding  map data structures for named parameters. Useful due to the recursive
     * calls that occur due to the AST traversal.
     */
    private Deque<Map<String, Type>> namedParamsStack;

    public CollectTypesASTVisitor(){
        errorstate = false;
        functionCallStack = new ArrayDeque<>();
        functionDeclaration = new ArrayDeque<>();
        functionParameters = new ArrayDeque<>();
        insideLoop = new ArrayDeque<>();
        funcReturnType = null;
        namedParameters = new HashMap<>();
        namedParamsStack = new ArrayDeque<>();
    }

    @Override
    public void visit(KotlinFile node) throws ASTVisitorException { 
        node.getCompUnit().accept(this);
        SymbolTable<Info> safeSymbolTable = ASTUtils.getSafeSymbolTable(node);
        Collection<Identifier> keys = safeSymbolTable.getKeys();
        int counter = 0;
        for(Identifier identifier : keys){
            if(identifier.getIdentifierType() == IdentifierType.FUNCTION.value()){
                if(identifier.getIdentifier().equals("main"))
                    counter++;
            }
        }
        if(counter == 0)
            ASTUtils.error(node, "No main method declared!");
        if(counter > 1)
            ASTUtils.error(node, "Illegal overloading of main method!");

		ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(CompUnit node) throws ASTVisitorException {
        for(FunctionDeclaration functionDeclaration : node.getFunctionDeclarations())
            functionDeclaration.accept(this);
        for(PropertyDeclaration propertyDeclaration : node.getPropertyDeclarations())
            propertyDeclaration.accept(this);
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(VariableDeclaration node) throws ASTVisitorException {
        if( node.getType() == null && errorstate)
            ASTUtils.error(node, "This variable must either have a type annotation or be initialized");
        if(node.getType() != null)
            node.getType().accept(this);
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(TypeK node) throws ASTVisitorException {
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(VarDeclaration node) throws ASTVisitorException {
        errorstate = true;
        node.getVariableDeclaration().accept(this);
        errorstate = false;
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(ValDeclaration node) throws ASTVisitorException {
        errorstate = true;
        node.getVariableDeclaration().accept(this);
        errorstate = false;
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(VarDeclarationAssignment node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);
        SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
        String id = node.getVariableDeclaration().getIdentifier();
        Identifier identifier = new Identifier(id, IdentifierType.VARIABLE.value());
        VariableInfo info  = (VariableInfo)symbolTable.lookup(identifier); //util class
        if( info == null )
            ASTUtils.error(node, "Internal compiler error");
        
        Type leftType = info.getType();
        node.getExpression().accept(this);
        
        if( leftType == null ){
            leftType = ASTUtils.getSafeType(node.getExpression());
        }else{
            Type rightType = ASTUtils.getSafeType(node.getExpression());
            if( !TypeUtils.isAssignable(leftType, rightType) )
                ASTUtils.error(node, "Cannot assign type " + rightType + " to " + leftType);
        }
    
        info.setType(leftType);
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(ValDeclarationAssignment node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);
        SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
        String id = node.getVariableDeclaration().getIdentifier();
        Identifier identifier = new Identifier(id, IdentifierType.VARIABLE.value());
        VariableInfo info  = (VariableInfo)symbolTable.lookup(identifier); //utility class
        if( info == null )
            ASTUtils.error(node, "Internal compiler error");
        
        Type leftType = info.getType();
        node.getExpression().accept(this);
        
        if( leftType == null ){
            leftType = ASTUtils.getSafeType(node.getExpression());
        }else{
            Type rightType = ASTUtils.getSafeType(node.getExpression());
            if( !TypeUtils.isAssignable(leftType, rightType) )
                ASTUtils.error(node, "Cannot assign type " + rightType + " to " + leftType);
        }
        info.setType(leftType);
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(FunctionDeclaration node) throws ASTVisitorException {        
        functionDeclaration.add(true);
        if( node.getReturnType() == null )
            funcReturnType = Type.VOID_TYPE;
        else{
            funcReturnType = node.getReturnType().getType();
        }
        

        List<Parameter> parameters = node.getFunctionParamaters().getParameters();
 
        functionParameters.push(parameters);


        node.getStatement().accept(this);

        functionDeclaration.removeLast();
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(FunctionParameters node) throws ASTVisitorException {
        List<Parameter> paramtersList = node.getParameters();
        ListIterator<Parameter> iterator = paramtersList.listIterator(paramtersList.size());
        while(iterator.hasPrevious()){
            iterator.previous().accept(this);
        }
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(StringLiteralExpression node) throws ASTVisitorException {
		ASTUtils.setType(node, Type.getType(String.class));    
    }

    @Override
    public void visit(DoubleLiteralExpression node) throws ASTVisitorException {
		ASTUtils.setType(node, Type.DOUBLE_TYPE);
    }

    @Override
    public void visit(IntegerLiteralExpression node) throws ASTVisitorException {
		ASTUtils.setType(node, Type.INT_TYPE);
    }

    @Override
    public void visit(CharLiteralExpression node) throws ASTVisitorException {
		ASTUtils.setType(node, Type.CHAR_TYPE);
    }

    @Override
    public void visit(BooleanLiteralExpression node) throws ASTVisitorException {
		ASTUtils.setType(node, Type.BOOLEAN_TYPE);
    }

    @Override
    public void visit(NullLiteralExpression node) throws ASTVisitorException {
		ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(IdentifierExpression node) throws ASTVisitorException {
        SymbolTable<Info> symbolTable =  ASTUtils.getSafeSymbolTable(node);
        Identifier identifier = new Identifier(node.getIdentifier(), IdentifierType.VARIABLE.value());
        VariableInfo info = (VariableInfo) symbolTable.lookup(identifier);
		if(info == null)
			ASTUtils.error(node, "Undeclared identifier " + node.getIdentifier());
		Type type = info.getType();
		ASTUtils.setType(node, type);
    }

    @Override
    public void visit(ParenthesisExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
		ASTUtils.setType(node, ASTUtils.getSafeType(node.getExpression()));
    }

    @Override
    public void visit(BinaryExpression node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        Type lType = ASTUtils.getSafeType(node.getExpression1());
        node.getExpression2().accept(this);
        Type rType = ASTUtils.getSafeType(node.getExpression2());
        Operator operator = node.getOperator();
        try{
            Type resultType = TypeUtils.applyBinary(operator, lType, rType);
            ASTUtils.setType(node, resultType);
        } catch (TypeException e){
            ASTUtils.error(node, e.getMessage());
        }
    }

    @Override
    public void visit(PrefixUnaryExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
        Type type = ASTUtils.getSafeType(node.getExpression());
		try {
			ASTUtils.setType(node, TypeUtils.applyPrefixUnary(node.getOperator(), type));
		} catch (TypeException e) {
			ASTUtils.error(node, e.getMessage());
		}
    }

    @Override
    public void visit(PostfixUnaryExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
        Type type = ASTUtils.getSafeType(node.getExpression());
		try {
			ASTUtils.setType(node, TypeUtils.applyPostfixUnary(node.getOperator(), type));
		} catch (TypeException e) {
			ASTUtils.error(node, e.getMessage());
		}
    }

    @Override
    public void visit(AssignmentExpression node) throws ASTVisitorException {
        if(functionCallStack.peekFirst() != null && functionCallStack.peekFirst() ){
            node.getExpression2().accept(this);
            Type rightType =  ASTUtils.getSafeType(node.getExpression2());
            IdentifierExpression idexp = (IdentifierExpression)node.getExpression1();
            namedParamsStack.getLast().put(idexp.getIdentifier(), rightType);
            ASTUtils.setType(node, rightType);
        }else{
            SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
            node.getExpression1().accept(this);
            //1st semantic check allready checks for expression 1 instanceof
            IdentifierExpression idexp = (IdentifierExpression)node.getExpression1();
            String id = idexp.getIdentifier();
            Identifier identifier = new Identifier(id, IdentifierType.VARIABLE.value());
            VariableInfo info = (VariableInfo)symbolTable.lookup(identifier);
            if( info == null ){
                ASTUtils.error(idexp, "Undeclared identifier");
            }
            Type lType = info.getType();
            node.getExpression2().accept(this);
            Type rType = ASTUtils.getSafeType(node.getExpression2());
            if(!(TypeUtils.isAssignable(lType, rType)))
                ASTUtils.error(node, "Cannot assign type " + rType + " to " + lType);
            ASTUtils.setType(node, lType);
        }
        
    }

    @Override
    public void visit(FunctionCallParameter node) throws ASTVisitorException {
        node.getExpression().accept(this);
        Type type = ASTUtils.getSafeType(node.getExpression());
        ASTUtils.setType(node, type);
    }

    @Override
    public void visit(FunctionCall node) throws ASTVisitorException {
        functionCallStack.push(true);

        List<FunctionCallParameter> fplist = node.getFunctionCallParameters();
        namedParameters = new HashMap<>();
        namedParamsStack.add(namedParameters);
        int listSize = fplist.size();
            

        for(int i = listSize; i > 0; i--){
            fplist.get(i-1).accept(this);
        }

        String id = node.getIdentifier();
        String inferedSignature = FunctionUtils.aqquireSingnature(fplist, id);
        Identifier identifier = new Identifier(id, IdentifierType.FUNCTION.value(), inferedSignature);
        SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
        FunctionInfo info = (FunctionInfo)symbolTable.lookup(identifier);
        if( info == null ){
            Collection<Identifier> keys = symbolTable.getKeys();
            String promotedSignature;
            try {
                promotedSignature = FunctionUtils.typePromotion(symbolTable, keys, inferedSignature,
                        namedParamsStack.pollLast());
                if(promotedSignature == null){
                    ASTUtils.error(node, "Function: " + inferedSignature + " has not been declared");
                }
                inferedSignature = promotedSignature;
                Identifier temp = new Identifier(id, IdentifierType.FUNCTION.value(), inferedSignature);
                if(symbolTable.lookup(temp)==null)
                    throw new ASTVisitorException("Internal error");
                info = (FunctionInfo)symbolTable.lookup(temp);
            } catch (TypeException e) {
                throw new ASTVisitorException(e.getMessage());
            }
        }
        ASTUtils.setFunctionSignatureProperty(node, inferedSignature);

        if(info.getReturnType() == null)
            ASTUtils.setType(node, Type.VOID_TYPE);
        else
            ASTUtils.setType(node, info.getReturnType());
        functionCallStack.pop();
    }

    @Override
    public void visit(FunctionCallExpression node) throws ASTVisitorException {
        node.getFunctionCall().accept(this);
        Type type = ASTUtils.getSafeType(node.getFunctionCall());
        ASTUtils.setType(node, type);
    }

    @Override
    public void visit(AssignmentStatement node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        Type type1 = ASTUtils.getSafeType(node.getExpression1());
        node.getExpression2().accept(this);
        Type type2 = ASTUtils.getSafeType(node.getExpression2());
        if(!TypeUtils.isAssignable(type1, type2))
            ASTUtils.error(node, "Cannot assign type " + type2 + " to " + type1);
        ASTUtils.setType(node, type1);
    }

    @Override
    public void visit(CompoundStatement node) throws ASTVisitorException {
        if(functionDeclaration.peekLast() != null && functionDeclaration.peekLast()){
            if(functionParameters.size() > 0){
                SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
                List<Parameter> parameters = functionParameters.pop();
                for(Parameter param : parameters){
                    symbolTable.put(new Identifier(param.getIdentifier(), IdentifierType.VARIABLE.value()), new VariableInfo(param.getType().getType()));
                }
            }
        }
        for (Statement st : node.getStatements()) {
			st.accept(this);
        }
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(ExpressionStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
        Type type = ASTUtils.getSafeType(node.getExpression());
        ASTUtils.setType(node, type);
    }

    @Override
    public void visit(PropertyDeclarationStatement node) throws ASTVisitorException {
        node.getPropertyDeclaration().accept(this);
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(FunctionDeclarationStatement node) throws ASTVisitorException {
        node.getFunctionDeclaration().accept(this);
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(IfStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
        if (!ASTUtils.getSafeType(node.getExpression()).equals(Type.BOOLEAN_TYPE)) {
                ASTUtils.error(node.getExpression(), "Invalid expression, should be boolean");
        }
        node.getStatement().accept(this);
        ASTUtils.setType(node, Type.VOID_TYPE);

    }

    @Override
    public void visit(IfElseStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
        if (!ASTUtils.getSafeType(node.getExpression()).equals(Type.BOOLEAN_TYPE)) {
                ASTUtils.error(node.getExpression(), "Invalid expression, should be boolean");
        }
        node.getStatement1().accept(this);
        node.getStatement2().accept(this);
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(ReturnStatement node) throws ASTVisitorException {
        if(functionDeclaration.peekLast() == null)
            ASTUtils.error(node, "Return statement out of funcion declaration!");
        if( !functionDeclaration.peekLast())
            ASTUtils.error(node, "Return statement out of funcion declaration!");
        if(node.getExpression() == null)
            ASTUtils.setType(node, Type.VOID_TYPE);
        else{
            node.getExpression().accept(this);
            Type type = ASTUtils.getSafeType(node.getExpression());
            if(!(TypeUtils.isAssignable(funcReturnType, type)))
                ASTUtils.error(node, "Return statement type " + type + " is incompatible with function's return type " + funcReturnType);
            ASTUtils.setType(node, type);
        }
    }

    @Override
    public void visit(ContinueStatement node) throws ASTVisitorException {
        if( insideLoop.peekLast() == null )
            ASTUtils.error(node, "Continue statement outside loop");
        if(!insideLoop.peekLast())
            ASTUtils.error(node, "Continue statement outside loop");
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(BreakStatement node) throws ASTVisitorException {
        if( insideLoop.peekLast() == null )
            ASTUtils.error(node, "Continue statement outside loop");
        if(!insideLoop.peekLast())
            ASTUtils.error(node, "Break statement outside loop");
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(WhileStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
		if (!ASTUtils.getSafeType(node.getExpression()).equals(Type.BOOLEAN_TYPE)) {
			ASTUtils.error(node.getExpression(), "Invalid expression, should be boolean");
        }
        insideLoop.add(true);
        node.getStatement().accept(this);
        insideLoop.pop();
		ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(DoWhileStatement node) throws ASTVisitorException {
        insideLoop.push(true);
        node.getStatement().accept(this);
        insideLoop.pop();
        node.getExpression().accept(this);
		if (!ASTUtils.getSafeType(node.getExpression()).equals(Type.BOOLEAN_TYPE)) 
			ASTUtils.error(node.getExpression(), "Invalid expression, should be boolean");
		ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(ForStatement node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);

        TypeK typeK = node.getVariableDeclaration().getType();
        Type type;
        node.getExpression().accept(this);
        if( typeK != null){
            Type resultType = ASTUtils.getSafeType(node.getExpression());
            boolean validTypes = TypeUtils.applyForLoopTypeChecking(typeK.getType(), resultType);
            if(!validTypes)
                ASTUtils.error(node, "The loop iterates over values of type " + resultType + " but parameter is declared to be" + typeK.getType().toString());
            type = resultType;
        }else{
            type = ASTUtils.getSafeType(node.getExpression());
        }
        String id = node.getVariableDeclaration().getIdentifier();
        SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
        Identifier identifier = new Identifier(id, IdentifierType.VARIABLE.value());
        VariableInfo info = (VariableInfo)symbolTable.lookup(identifier);
        if(info != null)
            info.setType(type);
        insideLoop.push(true);
        node.getStatement().accept(this);
        insideLoop.pop();
        ASTUtils.setType(node, Type.VOID_TYPE);
   }

    @Override
    public void visit(ForStatementWithStep node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);

        TypeK typeK = node.getVariableDeclaration().getType();
        Type type;
        node.getExpression().accept(this);
        if( typeK != null){
            Type resultType = ASTUtils.getSafeType(node.getExpression());
            boolean validTypes = TypeUtils.applyForLoopTypeChecking(typeK.getType(), resultType);
            if(!validTypes)
                ASTUtils.error(node, "The loop iterates over values of type " + resultType + " but parameter is declared to be" + typeK.getType().toString());
            type = resultType;
        }else{
            type = ASTUtils.getSafeType(node.getExpression());
        }
        String id = node.getVariableDeclaration().getIdentifier();
        SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
        Identifier identifier = new Identifier(id, IdentifierType.VARIABLE.value());
        VariableInfo info = (VariableInfo)symbolTable.lookup(identifier);
        if(info != null)
            info.setType(type);
        node.getStepExpression().accept(this);
        Type stepExpressionType = ASTUtils.getSafeType(node.getStepExpression());
        if(!(stepExpressionType.equals(Type.INT_TYPE)))
            ASTUtils.error(node, "The " + stepExpressionType.toString() + " literal does not conform to the expected type Int");
        insideLoop.push(true);
        node.getStatement().accept(this);
        insideLoop.pop();
        ASTUtils.setType(node, Type.VOID_TYPE);
    }

    @Override
    public void visit(Parameter node) throws ASTVisitorException {
        ASTUtils.setType(node, Type.VOID_TYPE);
        if(node.getType() != null)
            node.getType().accept(this);
    }
    
}

//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Wed Jun 24 20:14:24 EEST 2020
//----------------------------------------------------

/** CUP generated class containing symbol constants. */
public class sym {
  /* terminals */
  public static final int PLUSPLUS = 35;
  public static final int VAL = 2;
  public static final int STRING_LITERAL = 56;
  public static final int GTEQ = 46;
  public static final int INCREMENT = 33;
  public static final int CHAR = 7;
  public static final int CHAR_LITERAL = 57;
  public static final int LTEQ = 45;
  public static final int LOG_OR = 50;
  public static final int UMINUS = 39;
  public static final int UNIT = 28;
  public static final int LPAREN = 16;
  public static final int CONTINUE = 31;
  public static final int INT = 6;
  public static final int MINUS = 38;
  public static final int FOR = 23;
  public static final int DECREMENT = 34;
  public static final int RPAREN = 17;
  public static final int BOOLEAN_LITERAL = 58;
  public static final int SEMICOLON = 10;
  public static final int IS = 29;
  public static final int LT = 43;
  public static final int FUN = 4;
  public static final int IN = 24;
  public static final int COMMA = 12;
  public static final int PLUS = 37;
  public static final int NULL_LITERAL = 59;
  public static final int ASSIGN = 15;
  public static final int IF = 19;
  public static final int RANGE_TO = 52;
  public static final int NOT_IN = 25;
  public static final int EOF = 0;
  public static final int BOOLEAN = 5;
  public static final int MINUSMINUS = 36;
  public static final int RETURN = 32;
  public static final int error = 1;
  public static final int LCURLY = 13;
  public static final int MODULO = 41;
  public static final int NEQ = 48;
  public static final int BREAK = 30;
  public static final int EQ = 47;
  public static final int RCURLY = 14;
  public static final int TIMES = 11;
  public static final int COLON = 18;
  public static final int ELSE = 20;
  public static final int WHILE = 22;
  public static final int FLOAT = 9;
  public static final int LOG_AND = 49;
  public static final int LOG_NOT = 42;
  public static final int QUESTION = 51;
  public static final int STRING = 8;
  public static final int FLOAT_LITERAL = 55;
  public static final int DOWN_TO = 27;
  public static final int DIVISION = 40;
  public static final int GT = 44;
  public static final int VAR = 3;
  public static final int DO = 21;
  public static final int STEP = 26;
  public static final int INTEGER_LITERAL = 54;
  public static final int IDENTIFIER = 53;
}


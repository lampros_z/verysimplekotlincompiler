/*import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ast.ASTUtils;
import ast.ASTVisitor;
import ast.ASTVisitorException;
import ast.AssignmentExpression;
import ast.AssignmentStatement;
import ast.BinaryExpression;
import ast.BooleanLiteralExpression;
import ast.BreakStatement;
import ast.CharLiteralExpression;
import ast.CompUnit;
import ast.CompoundStatement;
import ast.ContinueStatement;
import ast.DoWhileStatement;
import ast.DoubleLiteralExpression;
import ast.ExpressionStatement;
import ast.ForStatement;
import ast.ForStatementWithStep;
import ast.FunctionCall;
import ast.FunctionCallExpression;
import ast.FunctionCallParameter;
import ast.FunctionDeclaration;
import ast.FunctionDeclarationStatement;
import ast.FunctionParameters;
import ast.IdentifierExpression;
import ast.IfElseStatement;
import ast.IfStatement;
import ast.IntegerLiteralExpression;
import ast.KotlinFile;
import ast.NullLiteralExpression;
import ast.Operator;
import ast.Parameter;
import ast.ParenthesisExpression;
import ast.PostfixUnaryExpression;
import ast.PrefixUnaryExpression;
import ast.PropertyDeclaration;
import ast.PropertyDeclarationStatement;
import ast.ReturnStatement;
import ast.Statement;
import ast.StringLiteralExpression;
import ast.TypeK;
import ast.ValDeclaration;
import ast.ValDeclarationAssignment;
import ast.VarDeclaration;
import ast.VarDeclarationAssignment;
import ast.VariableDeclaration;
import ast.WhileStatement;
import threeaddr.*;

public class IntermediateCodeASTVisitor implements ASTVisitor {
    private static final Logger LOGGER = LoggerFactory.getLogger(IntermediateCodeASTVisitor.class);
    private  Program program;
    private  Deque<String> stack;
    private  Deque<Instruction> instructionStack;
    private int temp;
    private Deque<Boolean> forLoopControlStack;
    private Deque<Boolean> loopCounterDirectionStack;
    private Deque<Runnable> postfixIncrementStack;

    public IntermediateCodeASTVisitor() {
        program = new Program();
        stack = new ArrayDeque<>();
        instructionStack = new ArrayDeque<>();
        forLoopControlStack = new ArrayDeque<>();
        loopCounterDirectionStack = new ArrayDeque<>();
        postfixIncrementStack = new ArrayDeque<>();
        temp = 0;
    }

    private String createTemp() {
        return "t" + Integer.toString(temp++);
    }

    public Program getProgram() {
        return program;
    }

    @Override
    public void visit(KotlinFile node) throws ASTVisitorException {
        node.getCompUnit().accept(this);
    }

    @Override
    public void visit(CompUnit node) throws ASTVisitorException {
        for (PropertyDeclaration propertyDeclaration : node.getPropertyDeclarations()) {
            propertyDeclaration.accept(this);
        }

        for (FunctionDeclaration functionDeclaration : node.getFunctionDeclarations()) {
            functionDeclaration.accept(this);
        }

    }

    @Override
    public void visit(VariableDeclaration node) throws ASTVisitorException {
        // Nothing to do here
    }

    @Override
    public void visit(TypeK node) throws ASTVisitorException {
        // Nothing to do here 
    }

    @Override
    public void visit(VarDeclaration node) throws ASTVisitorException {
        // Nothing to do here
    }

    @Override
    public void visit(ValDeclaration node) throws ASTVisitorException {
        // Nothing to do here
    }

    @Override
    public void visit(VarDeclarationAssignment node) throws ASTVisitorException {
        node.getExpression().accept(this);
        String t = stack.pop();
        program.add(new AssignInstr(t, node.getVariableDeclaration().getIdentifier()));
    }

    @Override
    public void visit(ValDeclarationAssignment node) throws ASTVisitorException {
        node.getExpression().accept(this);
        String t = stack.pop();
        program.add(new AssignInstr(t, node.getVariableDeclaration().getIdentifier()));
    }

    @Override
    public void visit(FunctionDeclaration node) throws ASTVisitorException {
        node.getStatement().accept(this);
    }

    @Override
    public void visit(FunctionParameters node) throws ASTVisitorException {
        // Nothing to do here
    }

    @Override
    public void visit(StringLiteralExpression node) throws ASTVisitorException {
        if (ASTUtils.isBooleanExpression(node)) {
            ASTUtils.error(node, "Strings cannot be used as boolean expressions");
        } else {
            String t = createTemp();
            stack.push(t);
            program.add(new AssignInstr("\"" + StringEscapeUtils.escapeJava(node.getLiteral()) + "\"", t));
        }
    }

    @Override
    public void visit(DoubleLiteralExpression node) throws ASTVisitorException {
        if (ASTUtils.isBooleanExpression(node)) {
            if (node.getLiteral() != 0d) {
                GotoInstr i = new GotoInstr();
                program.add(i);
                ASTUtils.getTrueList(node).add(i);
            } else {
                GotoInstr i = new GotoInstr();
                program.add(i);
                ASTUtils.getFalseList(node).add(i);
            }
        } else {
            String t = createTemp();
            stack.push(t);
            program.add(new AssignInstr(node.getLiteral().toString(), t));
        }
    }

    @Override
    public void visit(IntegerLiteralExpression node) throws ASTVisitorException {
        if (ASTUtils.isBooleanExpression(node)) {
            if (node.getLiteral() != 0) {
                GotoInstr i = new GotoInstr();
                program.add(i);
                ASTUtils.getTrueList(node).add(i);
            } else {
                GotoInstr i = new GotoInstr();
                program.add(i);
                ASTUtils.getFalseList(node).add(i);
            }
        } else {
            String t = createTemp();
            program.add(new AssignInstr(node.getLiteral().toString(), t));
            stack.push(t);
        }
    }

    @Override
    public void visit(CharLiteralExpression node) throws ASTVisitorException {
        if (ASTUtils.isBooleanExpression(node)) {
            ASTUtils.error(node, "Chars cannot be used as boolean expressions");
        } else {
            String t = createTemp();
            program.add(new AssignInstr("'" + node.getLiteral() + "'", t));
            stack.push(t);
        }
    }

    @Override
    public void visit(BooleanLiteralExpression node) throws ASTVisitorException {
        if (ASTUtils.isBooleanExpression(node)) {
            if (node.isLiteral()) {
                GotoInstr i = new GotoInstr();
                program.add(i);
                ASTUtils.getTrueList(node).add(i);
            } else {
                GotoInstr i = new GotoInstr();
                program.add(i);
                ASTUtils.getFalseList(node).add(i);
            }
        } else {
            String t = createTemp();
            program.add(new AssignInstr(String.valueOf(node.isLiteral()), t));
            stack.push(t);
        }
    }

    @Override
    public void visit(NullLiteralExpression node) throws ASTVisitorException {
        if (ASTUtils.isBooleanExpression(node)) {
            ASTUtils.error(node, "Nulls cannot be used as boolean expressions");
        } else {
            String t = createTemp();
            program.add(new AssignInstr("null", t));
            stack.push(t);
        }
    }

    @Override
    public void visit(IdentifierExpression node) throws ASTVisitorException {
        stack.push(node.getIdentifier());
    }

    @Override
    public void visit(ParenthesisExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
        String t1 = stack.pop();
        String t = createTemp();
        program.add(new AssignInstr(t1, t));
        stack.push(t);
    }

    @Override
    public void visit(BinaryExpression node) throws ASTVisitorException {

        //Not clean code version
        if (ASTUtils.isBooleanExpression(node)) {
            if (!node.getOperator().isRelational() && !node.getOperator().isContains() && !node.getOperator().isRange() ) {
                ASTUtils.error(node, "A not boolean expression used as boolean.");
            }
            ASTUtils.setBooleanExpression(node.getExpression1(), true);
            node.getExpression1().accept(this);
            ASTUtils.setBooleanExpression(node.getExpression1(), false);
            if(node.getExpression1() instanceof PostfixUnaryExpression){
                Runnable methodCall = () -> {
                    try {
                        node.getExpression1().accept(this);
                    } catch (ASTVisitorException e) {
                        LOGGER.info(e.getMessage());
                    }
                };
                postfixIncrementStack.push(methodCall);
            }
                
            String t1 = stack.pop();
            if(node.getOperator().isRange()){
                node.getExpression2().accept(this);
                String t2 = stack.pop();
                instructionStack.push(new CondJumpInstr(Operator.LESS, "", t1) );
                instructionStack.push(new CondJumpInstr(Operator.GREATER, "", t2));
                if(forLoopControlStack.peekLast() != null && forLoopControlStack.peekLast()){
                    stack.push(t1);
                    if(node.getOperator().equals(Operator.RANGE_TO)){
                        loopCounterDirectionStack.push(true);
                    } else {
                        loopCounterDirectionStack.push(false);
                    }
                }
            } else
            if(node.getOperator().isContains()){
                ASTUtils.setBooleanExpression(node.getExpression2(), true);
                node.getExpression2().accept(this);                
                CondJumpInstr rcondJumpInstr = (CondJumpInstr) instructionStack.pop();
                CondJumpInstr lcondJumpInstr = (CondJumpInstr) instructionStack.pop();
                rcondJumpInstr.setArg1(t1);
                lcondJumpInstr.setArg1(t1);
                GotoInstr gotoInstr = new GotoInstr();
                program.add(lcondJumpInstr);
                program.add(rcondJumpInstr);
                program.add(gotoInstr);
                ASTUtils.getFalseList(node).add(rcondJumpInstr);
                ASTUtils.getFalseList(node).add(lcondJumpInstr);
                ASTUtils.getTrueList(node).add(gotoInstr);
    
            }else{
                node.getExpression2().accept(this);
                String t2 = stack.pop();
                // create new CondJumpInstr with null target
                CondJumpInstr condJumpInstr = new CondJumpInstr(node.getOperator(), t1, t2);
                // create new GotoInstr with null target
                GotoInstr gotoInstr = new GotoInstr();
                // add both to program
                program.add(condJumpInstr);
                program.add(gotoInstr);
                // add first instruction into truelist of node
                ASTUtils.getTrueList(node).add(condJumpInstr);
                // add second instruction into falselist of node
                ASTUtils.getFalseList(node).add(gotoInstr);
            }
        } else {
            node.getExpression1().accept(this);
            String t1 = stack.pop();
            node.getExpression2().accept(this);
            String t2 = stack.pop();
            // create new temporary
            String t = createTemp();
            // add binary operation instruction
            BinaryOpInstr binaryOpInstr = new BinaryOpInstr(node.getOperator(), t1, t2, t);
            program.add(binaryOpInstr);
            // add new temporary in stack
            stack.push(t);
        }
    }

    @Override
    public void visit(PrefixUnaryExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
        String t1 = stack.pop();
        if(node.getOperator().isIncremental()){
            String t2 = createTemp();
            String t3 = createTemp();
            program.add(new AssignInstr("1", t3));
            if(node.getOperator().equals(Operator.PLUSPLUS))
                program.add(new BinaryOpInstr(Operator.PLUS, t1, t3, t2));
              else 
                program.add(new BinaryOpInstr(Operator.MINUS, t1, t3, t2));
            program.add(new AssignInstr(t2, t1));
            stack.push(t1);
        }
        else{
            String t = createTemp();
            program.add(new PrefixUnaryOpInstr(node.getOperator(), t1, t));
            stack.push(t);
        }
    }

    @Override
    public void visit(PostfixUnaryExpression node) throws ASTVisitorException {
        if (ASTUtils.isBooleanExpression(node)) {
            node.getExpression().accept(this);
            String t1 = stack.pop();
            stack.push(t1);
        }else{
            node.getExpression().accept(this);
            String t1 = stack.pop();
            if(node.getOperator().isIncremental()){
                String t2 = createTemp();
                String t3 = createTemp();
                program.add(new AssignInstr("1", t3));
                if(node.getOperator().equals(Operator.PLUSPLUS))
                    program.add(new BinaryOpInstr(Operator.PLUS, t1, t3, t2));
                else 
                    program.add(new BinaryOpInstr(Operator.MINUS, t1, t3, t2));
                program.add(new AssignInstr(t2, t1));
                stack.push(t1);
            }
            else{
                String t = createTemp();
                program.add(new PrefixUnaryOpInstr(node.getOperator(), t1, t));
                stack.push(t);
            }
        }
    }

    @Override
    public void visit(AssignmentExpression node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        String t = stack.pop();
        node.getExpression2().accept(this);
        String t1 = stack.pop();
        program.add(new AssignInstr(t1, t));
    }

    @Override
    public void visit(FunctionCallParameter node) throws ASTVisitorException {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(FunctionCall node) throws ASTVisitorException {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(FunctionCallExpression node) throws ASTVisitorException {
        node.getFunctionCall().accept(this);
    }

    @Override
    public void visit(AssignmentStatement node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        String t = stack.pop();
        node.getExpression2().accept(this);
        String t1 = stack.pop();
        program.add(new AssignInstr(t1, t));
    }

    @Override
    public void visit(CompoundStatement node) throws ASTVisitorException {
        Statement s = null, ps;
        Iterator<Statement> it = node.getStatements().iterator();
        while (it.hasNext()) {
            ps = s;
            s = it.next();

            if (ps != null && !ASTUtils.getNextList(ps).isEmpty()) {
                Program.backpatch(ASTUtils.getNextList(ps), program.addNewLabel());
            }

            s.accept(this);

            if (!ASTUtils.getBreakList(s).isEmpty()) {
                ASTUtils.error(s, "Break detected without a loop.");
            }

            if (!ASTUtils.getContinueList(s).isEmpty()) {
                ASTUtils.error(s, "Continue detected without a loop.");
            }
        }
        if (s != null && !ASTUtils.getNextList(s).isEmpty()) {
            Program.backpatch(ASTUtils.getNextList(s), program.addNewLabel());
        }
    }

    @Override
    public void visit(ExpressionStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(PropertyDeclarationStatement node) throws ASTVisitorException {
        node.getPropertyDeclaration().accept(this);
    }

    @Override
    public void visit(FunctionDeclarationStatement node) throws ASTVisitorException {
        node.getFunctionDeclaration().accept(this);
    }

    @Override
    public void visit(IfStatement node) throws ASTVisitorException {
        ASTUtils.setBooleanExpression(node.getExpression(), true);
        node.getExpression().accept(this);
        LabelInstr trueLabel = program.addNewLabel();
        node.getStatement().accept(this);
        if(postfixIncrementStack.size() != 0)
            postfixIncrementStack.pop().run();
        Program.backpatch(ASTUtils.getTrueList(node.getExpression()), trueLabel);
        ASTUtils.getBreakList(node).addAll(ASTUtils.getBreakList(node.getStatement()));
        ASTUtils.getContinueList(node).addAll(ASTUtils.getContinueList(node.getStatement()));
        ASTUtils.getNextList(node).addAll(ASTUtils.getFalseList(node.getExpression()));
        ASTUtils.getNextList(node).addAll(ASTUtils.getNextList(node.getStatement()));
    }

    @Override
    public void visit(IfElseStatement node) throws ASTVisitorException {
        ASTUtils.setBooleanExpression(node.getExpression(), true);
        node.getExpression().accept(this);
        LabelInstr stmt1Label = program.addNewLabel();
        node.getStatement1().accept(this);
        if(postfixIncrementStack.size() != 0)
            postfixIncrementStack.pop().run();
        GotoInstr nextGoto = new GotoInstr();
        program.add(nextGoto);
        LabelInstr stmt2Label = program.addNewLabel();
        node.getStatement2().accept(this);
        Program.backpatch(ASTUtils.getTrueList(node.getExpression()), stmt1Label);
        Program.backpatch(ASTUtils.getFalseList(node.getExpression()), stmt2Label);
        ASTUtils.getBreakList(node).addAll(ASTUtils.getBreakList(node.getStatement1()));
        ASTUtils.getBreakList(node).addAll(ASTUtils.getBreakList(node.getStatement2()));
        ASTUtils.getContinueList(node).addAll(ASTUtils.getContinueList(node.getStatement1()));
        ASTUtils.getContinueList(node).addAll(ASTUtils.getContinueList(node.getStatement2()));
        ASTUtils.getNextList(node).addAll(ASTUtils.getNextList(node.getStatement1()));
        ASTUtils.getNextList(node).addAll(ASTUtils.getNextList(node.getStatement2()));
        ASTUtils.getNextList(node).add(nextGoto);
    }

    @Override
    public void visit(ReturnStatement node) throws ASTVisitorException {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(ContinueStatement node) throws ASTVisitorException {
        GotoInstr gotoInstr = new GotoInstr();
        ASTUtils.getContinueList(node).add(gotoInstr);
    }

    @Override
    public void visit(BreakStatement node) throws ASTVisitorException {
        GotoInstr gotoInstr = new GotoInstr();
        ASTUtils.getBreakList(node).add(gotoInstr);
    }

    @Override
    public void visit(WhileStatement node) throws ASTVisitorException {
        ASTUtils.setBooleanExpression(node.getExpression(), true);

        LabelInstr beginLabel = program.addNewLabel();
        node.getExpression().accept(this);
        LabelInstr beginStmtLabel = program.addNewLabel();
        Program.backpatch(ASTUtils.getTrueList(node.getExpression()), beginStmtLabel);
        node.getStatement().accept(this);
        if(postfixIncrementStack.size() != 0)
            postfixIncrementStack.pop().run();
        GotoInstr gotoInstr = new GotoInstr(beginLabel);
        program.add(gotoInstr);
        Program.backpatch(ASTUtils.getNextList(node.getStatement()), beginLabel);
        Program.backpatch(ASTUtils.getContinueList(node.getStatement()), beginLabel);
        ASTUtils.getNextList(node).addAll(ASTUtils.getFalseList(node.getExpression()));
        ASTUtils.getNextList(node).addAll(ASTUtils.getBreakList(node.getStatement()));

    }

    @Override
    public void visit(DoWhileStatement node) throws ASTVisitorException {
        ASTUtils.setBooleanExpression(node.getExpression(), true);
        //set do while
        LabelInstr beginLabel = program.addNewLabel();

        node.getStatement().accept(this);
        ASTUtils.getNextList(node).addAll(ASTUtils.getBreakList(node.getStatement()));

        LabelInstr beginExprLabel = program.addNewLabel();
        Program.backpatch(ASTUtils.getNextList(node.getStatement()), beginExprLabel);
        Program.backpatch(ASTUtils.getContinueList(node.getStatement()), beginExprLabel);

        node.getExpression().accept(this);
        ASTUtils.getNextList(node).addAll(ASTUtils.getFalseList(node.getExpression()));
        Program.backpatch(ASTUtils.getTrueList(node.getExpression()), beginLabel);
    }

    @Override
    public void visit(ForStatement node) throws ASTVisitorException {
        ASTUtils.setBooleanExpression(node.getExpression(), true);
        String loopVariable = node.getVariableDeclaration().getIdentifier();
        forLoopControlStack.push(true);
        node.getExpression().accept(this);
        if(instructionStack.size() == 0)
            ASTUtils.error(node, "Error with instruction stack");
        String t1 = stack.pop();
        program.add(new AssignInstr(t1, loopVariable));
        LabelInstr beginLabel = program.addNewLabel();
        CondJumpInstr rcondJumpInstr = (CondJumpInstr) instructionStack.pop();
        CondJumpInstr lcondJumpInstr = (CondJumpInstr) instructionStack.pop();
        rcondJumpInstr.setArg1(loopVariable);
        lcondJumpInstr.setArg1(loopVariable);
        program.add(lcondJumpInstr);
        program.add(rcondJumpInstr);
        GotoInstr gotoInstr = new GotoInstr();
        program.add(gotoInstr);
        LabelInstr beginStmtLabel = program.addNewLabel();
        
        Program.backpatch(ASTUtils.getTrueList(node.getExpression()), beginStmtLabel);
        node.getStatement().accept(this);
        String t2 = createTemp();
        String t3 = createTemp();
        program.add(new AssignInstr("1", t2));
        if(loopCounterDirectionStack.pop())
            program.add(new BinaryOpInstr(Operator.PLUS, loopVariable, t2, t3));
          else 
            program.add(new BinaryOpInstr(Operator.MINUS, loopVariable, t2, t3));
        program.add(new AssignInstr(t3, loopVariable));
        program.add(new GotoInstr(beginLabel));
        
        ASTUtils.getTrueList(node.getExpression()).add(gotoInstr);
        ASTUtils.getFalseList(node.getExpression()).add(lcondJumpInstr);
        ASTUtils.getFalseList(node.getExpression()).add(rcondJumpInstr);
       
        Program.backpatch(ASTUtils.getNextList(node.getStatement()), beginLabel);
        Program.backpatch(ASTUtils.getContinueList(node.getStatement()), beginLabel);
        Program.backpatch(ASTUtils.getTrueList(node.getExpression()), beginStmtLabel);
        
        ASTUtils.getNextList(node).addAll(ASTUtils.getFalseList(node.getExpression()));
        ASTUtils.getNextList(node).addAll(ASTUtils.getBreakList(node.getStatement()));
        
        forLoopControlStack.pop();
    }

    @Override
    public void visit(ForStatementWithStep node) throws ASTVisitorException {
        ASTUtils.setBooleanExpression(node.getExpression(), true);
        String loopVariable = node.getVariableDeclaration().getIdentifier();
        forLoopControlStack.push(true);
        node.getExpression().accept(this);
        if(instructionStack.size() == 0)
            ASTUtils.error(node, "Error with instruction stack");
        String t1 = stack.pop();
        program.add(new AssignInstr(t1, loopVariable));
        LabelInstr beginLabel = program.addNewLabel();
        CondJumpInstr rcondJumpInstr = (CondJumpInstr) instructionStack.pop();
        CondJumpInstr lcondJumpInstr = (CondJumpInstr) instructionStack.pop();
        rcondJumpInstr.setArg1(loopVariable);
        lcondJumpInstr.setArg1(loopVariable);
        program.add(lcondJumpInstr);
        program.add(rcondJumpInstr);
        GotoInstr gotoInstr = new GotoInstr();
        program.add(gotoInstr);
        LabelInstr beginStmtLabel = program.addNewLabel();
        
        Program.backpatch(ASTUtils.getTrueList(node.getExpression()), beginStmtLabel);
        node.getStatement().accept(this);
        String t2 = createTemp();
        String t3 = createTemp();
        node.getStepExpression().accept(this);
        program.add(new AssignInstr(stack.pop(), t2));
        if(loopCounterDirectionStack.pop())
            program.add(new BinaryOpInstr(Operator.PLUS, loopVariable, t2, t3));
          else 
            program.add(new BinaryOpInstr(Operator.MINUS, loopVariable, t2, t3));
        program.add(new AssignInstr(t3, loopVariable));
        program.add(new GotoInstr(beginLabel));
        
        ASTUtils.getTrueList(node.getExpression()).add(gotoInstr);
        ASTUtils.getFalseList(node.getExpression()).add(lcondJumpInstr);
        ASTUtils.getFalseList(node.getExpression()).add(rcondJumpInstr);
       
        Program.backpatch(ASTUtils.getNextList(node.getStatement()), beginLabel);
        Program.backpatch(ASTUtils.getContinueList(node.getStatement()), beginLabel);
        Program.backpatch(ASTUtils.getTrueList(node.getExpression()), beginStmtLabel);
        
        ASTUtils.getNextList(node).addAll(ASTUtils.getFalseList(node.getExpression()));
        ASTUtils.getNextList(node).addAll(ASTUtils.getBreakList(node.getStatement()));
        
        forLoopControlStack.pop();

    }

    @Override
    public void visit(Parameter node) throws ASTVisitorException {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(DummyStatement dummyStatement) throws ASTVisitorException{

    }
}
*/
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.ListIterator;
import org.objectweb.asm.Type;
import ast.ASTUtils;
import ast.ASTVisitor;
import ast.ASTVisitorException;
import ast.AssignmentExpression;
import ast.AssignmentStatement;
import ast.BinaryExpression;
import ast.BooleanLiteralExpression;
import ast.BreakStatement;
import ast.CharLiteralExpression;
import ast.CompUnit;
import ast.CompoundStatement;
import ast.ContinueStatement;
import ast.DoWhileStatement;
import ast.DoubleLiteralExpression;
import ast.ExpressionStatement;
import ast.ForStatement;
import ast.ForStatementWithStep;
import ast.FunctionCall;
import ast.FunctionCallExpression;
import ast.FunctionCallParameter;
import ast.FunctionDeclaration;
import ast.FunctionDeclarationStatement;
import ast.FunctionParameters;
import ast.IdentifierExpression;
import ast.IfElseStatement;
import ast.IfStatement;
import ast.IntegerLiteralExpression;
import ast.KotlinFile;
import ast.NullLiteralExpression;
import ast.Parameter;
import ast.ParenthesisExpression;
import ast.PostfixUnaryExpression;
import ast.PrefixUnaryExpression;
import ast.PropertyDeclaration;
import ast.PropertyDeclarationStatement;
import ast.ReturnStatement;
import ast.Statement;
import ast.StringLiteralExpression;
import ast.TypeK;
import ast.ValDeclaration;
import ast.ValDeclarationAssignment;
import ast.VarDeclaration;
import ast.VarDeclarationAssignment;
import ast.VariableDeclaration;
import ast.WhileStatement;
import symbol.FunctionInfo;
import symbol.FunctionUtils;
import symbol.Identifier;
import symbol.IdentifierType;
import symbol.Info;
import symbol.LocalIndexPool;
import symbol.ParameterInfo;
import symbol.SymbolTable;
import symbol.VariableInfo;


/**
 * Compilers 2020 Harokopio University of Athens, Dept. of Informatics and
 * Telematics.
 * @teacher michail@hua.gr
 * @author it21622@hua.gr
 */

/**
 * CollectIndexesASTVisitor: A visitor that traverses the AST using the index pool of each node to calculate
 * the value for an identifier(function param, variable) and assign this value to the symbol table.
 */
public class CollectIndexesASTVisitor implements ASTVisitor {
    /**
     * A stack useful when the visitor encounters nested function calls.
     * The boolean value declares that the visitor is in function call state.
     * When a nested function call [ e.g. foo( a = bar() ) ] is active this stack ensures that
     * the last activation record (nth record) will not alter the function call state to false but it will alter
     * the state at the top of the stack hence the first or second or nth - 1 state will remain true. 
     */
    private final Deque<Boolean> functionCallStack;
    /**
     * A stack useful when the visitor encounters nested function declarations.
     * The boolean value declares that the visitor is in function declaration state.
     * When a nested function declaration [ e.g. fun foo(){fun bar(){}} ] is active this stack ensures that
     * the last activation record (nth record) will not alter the function declaration state to false but it will alter
     * the state at the top of the stack hence the first or second or nth - 1 state will remain true. 
     */
    private final Deque<Boolean> functionDeclStack;
    /**
     * A stack holding  FunctionInfo objects. Useful due to the recursive
     * calls that occur due to the AST traversal. When needed, the top of the stack is retrieved
     * and used.
     */
    private final Deque<FunctionInfo> functionParStack;
    /**
     * A stack useful when the visitor encounters nested loop statements.
     * The boolean value declares that the visitor is inside loop statements.
     * When inside nested loops [ e.g. for(){for(){}} ] this stack ensures that
     * the last activation record (nth record) will not alter the  state to false but it will alter
     * the state at the top of the stack hence the first or second or nth - 1 state will remain unmodified. 
     */
    private final Deque<Boolean> forLoopControlStack;
    /**
     * A stack holding  FunctionInfo objects. Useful due to the recursive
     * calls that occur due to the AST traversal. When needed, the top of the stack is retrieved
     * and used.
     */
    private final Deque<FunctionInfo> functionInfoStack;
    /**
     * A field holding a boolean value whether the function declaration node is the main method.
     */
    private boolean isMainMethod;

    public CollectIndexesASTVisitor(){
        functionCallStack = new ArrayDeque<>();
        functionDeclStack = new ArrayDeque<>();
        functionParStack = new ArrayDeque<>();
        forLoopControlStack = new ArrayDeque<>();
        functionInfoStack = new ArrayDeque<>();
    }

    @Override
    public void visit(KotlinFile node) throws ASTVisitorException {
        node.getCompUnit().accept(this);
    }

    @Override
    public void visit(CompUnit node) throws ASTVisitorException {
        for (FunctionDeclaration functionDeclaration : node.getFunctionDeclarations()) {
			functionDeclaration.accept(this);
		}
        
        for (PropertyDeclaration propertyDeclaration : node.getPropertyDeclarations()) {
			propertyDeclaration.accept(this);
		}
    }

    @Override
    public void visit(VariableDeclaration node) throws ASTVisitorException {
        if(forLoopControlStack.peekFirst() != null && forLoopControlStack.peekFirst()){
            SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
            VariableInfo info = (VariableInfo)symbolTable.lookup(new Identifier(node.getIdentifier(), IdentifierType.VARIABLE.value()));
            LocalIndexPool localIndexPool = ASTUtils.getSafeLocalIndexPool(node);
            Integer index = localIndexPool.getLocalIndex(info.getType());
            info.setIndex(index);
        }
    }

    @Override
    public void visit(TypeK node) throws ASTVisitorException {
    }

    @Override
    public void visit(VarDeclaration node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);
    }

    @Override
    public void visit(ValDeclaration node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);
    }

    @Override
    public void visit(VarDeclarationAssignment node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);
        SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
        String id = node.getVariableDeclaration().getIdentifier();
        Identifier identifier = new Identifier(id, IdentifierType.VARIABLE.value());
        VariableInfo info  = (VariableInfo)symbolTable.lookup(identifier);
        if( info == null )
            ASTUtils.error(node, "Internal compiler error");
        Type type = info.getType();
        LocalIndexPool localIndexPool = ASTUtils.getSafeLocalIndexPool(node);
        int localIndex = localIndexPool.getLocalIndex(type);
        info.setIndex(localIndex);
    }

    @Override
    public void visit(ValDeclarationAssignment node) throws ASTVisitorException {
        node.getVariableDeclaration().accept(this);
        SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
        String id = node.getVariableDeclaration().getIdentifier();
        Identifier identifier = new Identifier(id, IdentifierType.VARIABLE.value());
        VariableInfo info  = (VariableInfo)symbolTable.lookup(identifier);
        if( info == null )
            ASTUtils.error(node, "Internal compiler error");
        Type type = info.getType();
        LocalIndexPool localIndexPool = ASTUtils.getSafeLocalIndexPool(node);
        int localIndex = localIndexPool.getLocalIndex(type);
        info.setIndex(localIndex);
    }

    @Override
    public void visit(FunctionDeclaration node) throws ASTVisitorException {
        String id = node.getIdentifier();
        String signature = FunctionUtils.aqquireSingnatureRev(id, node.getFunctionParamaters());
        Identifier identifier = new Identifier(id, IdentifierType.FUNCTION.value(), signature);
        SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);

        FunctionInfo info = (FunctionInfo)symbolTable.lookup(identifier);
        
        if( info == null )
            ASTUtils.error(node, "Internal compiler error function");
        //function decls stack upgrade
        functionDeclStack.push(true);
        functionParStack.push(info);
        if(id.equals("main"))
            isMainMethod = true;
        node.getStatement().accept(this);
        if(isMainMethod && id.equals("main"))
            isMainMethod = false;
        functionDeclStack.pop();
    }

    @Override
    public void visit(FunctionParameters node) throws ASTVisitorException {
    }

    @Override
    public void visit(StringLiteralExpression node) throws ASTVisitorException {
    }

    @Override
    public void visit(DoubleLiteralExpression node) throws ASTVisitorException {
    }

    @Override
    public void visit(IntegerLiteralExpression node) throws ASTVisitorException {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(CharLiteralExpression node) throws ASTVisitorException {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(BooleanLiteralExpression node) throws ASTVisitorException {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(NullLiteralExpression node) throws ASTVisitorException {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(IdentifierExpression node) throws ASTVisitorException {
        if(functionCallStack.peekFirst() != null && functionCallStack.peekFirst() ){
            SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
            VariableInfo info = (VariableInfo)symbolTable.lookup(new Identifier(node.getIdentifier(), IdentifierType.VARIABLE.value()));
            Integer index;
            Type ltype;
            if(info == null){
                if( functionInfoStack.peekFirst() != null ){
                    FunctionInfo funcInfo = functionInfoStack.peekFirst();
                    ParameterInfo parameterInfo = funcInfo.getFormalParameters().get(node.getIdentifier());
                    if(parameterInfo == null)
                        ASTUtils.error(node, "Cannot find a parameter with this name: " + node.getIdentifier());
                    index = parameterInfo.getIndex();
                    ltype = parameterInfo.getType();
                }else {
                    index = -1;
                    ltype = Type.VOID_TYPE;
                    ASTUtils.error(node, "Internal compiler errror : Identifier Expression node");
                }
            }else{
                index = info.getIndex();
                ltype = info.getType();
            }
            ASTUtils.setParamIndexProperty(node, index);
            ASTUtils.setType(node, ltype);
        }
    }

    @Override
    public void visit(ParenthesisExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(BinaryExpression node) throws ASTVisitorException {
        node.getExpression1().accept(this);
		node.getExpression2().accept(this);
    }

    @Override
    public void visit(PrefixUnaryExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(PostfixUnaryExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(AssignmentExpression node) throws ASTVisitorException {
        if(functionCallStack.peekFirst() != null && functionCallStack.peekFirst()){
            node.getExpression1().accept(this);
            Type lType = ASTUtils.getSafeType(node.getExpression1());
            node.getExpression2().accept(this);
            Type rType = ASTUtils.getSafeType(node.getExpression2());
            if(!(types.TypeUtils.isAssignable(lType, rType))){
                ASTUtils.error(node, "Type mismatch: inferred type is " + rType.toString()+ " but "+ lType.toString() +" was expected");
            }
        }else{
            node.getExpression1().accept(this);
            IdentifierExpression idexp = (IdentifierExpression)node.getExpression1();
            String id = idexp.getIdentifier();
            SymbolTable<Info> safeSymbolTable = ASTUtils.getSafeSymbolTable(node);
            VariableInfo info = (VariableInfo)safeSymbolTable.lookup(new Identifier(id, IdentifierType.VARIABLE.value()));
            LocalIndexPool localIndexPool = ASTUtils.getSafeLocalIndexPool(node);
            int index = localIndexPool.getLocalIndex(info.getType());
            info.setIndex(index);
            node.getExpression2().accept(this);
        }
    }

    @Override
    public void visit(FunctionCallParameter node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(FunctionCall node) throws ASTVisitorException {
        SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
        FunctionInfo info = (FunctionInfo)symbolTable.lookup(new Identifier(node.getIdentifier(), IdentifierType.FUNCTION.value(), ASTUtils.getFunctionSignature(node)));
        if(info == null)
            ASTUtils.error(node, "Internal compiler error : function call node");
        List<FunctionCallParameter> fplist = node.getFunctionCallParameters();
        ListIterator<FunctionCallParameter> iterator = fplist.listIterator(fplist.size()); 
        functionCallStack.push(true);
        functionInfoStack.push(info);
        while(iterator.hasPrevious())
            iterator.previous().accept(this);
        functionInfoStack.pop();
        functionCallStack.pop();
    }

    @Override
    public void visit(FunctionCallExpression node) throws ASTVisitorException {
        node.getFunctionCall().accept(this);
    }

    @Override
    public void visit(AssignmentStatement node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        IdentifierExpression idexp = (IdentifierExpression)node.getExpression1();
        String id = idexp.getIdentifier();
        SymbolTable<Info> safeSymbolTable = ASTUtils.getSafeSymbolTable(node);
        VariableInfo info = (VariableInfo)safeSymbolTable.lookup(new Identifier(id, IdentifierType.VARIABLE.value()));
        LocalIndexPool localIndexPool = ASTUtils.getSafeLocalIndexPool(node);
        int index = localIndexPool.getLocalIndex(info.getType());
        info.setIndex(index);
		node.getExpression2().accept(this);
    }

    @Override
    public void visit(CompoundStatement node) throws ASTVisitorException {
        if( functionDeclStack.size() > 1 && functionDeclStack.peekFirst() != null && functionDeclStack.peekFirst() && !isMainMethod){
            FunctionInfo info = functionParStack.peekFirst();
            if(info != null){
                LocalIndexPool localIndexPool = ASTUtils.getSafeLocalIndexPool(node);
                int localIndex;
                SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
                for(String key : info.getFormalParameters().keySet()){
                    //getLocalIndexNested
                    localIndex = localIndexPool.getLocalIndexNested(info.getFormalParameters().get(key).getType());
                    VariableInfo vinfo = (VariableInfo)symbolTable.lookup(new Identifier(key, IdentifierType.VARIABLE.value()));
                    if(vinfo == null)
                        ASTUtils.error(node, "Parameter not found in sym table");
                    vinfo.setIndex(localIndex);
                    info.getFormalParameters().get(key).setIndex(localIndex);
                }
                info.setIndex(localIndexPool.getMaxLocals());
            }
            functionDeclStack.pop();
            functionDeclStack.push(false);
        }
        if ( (functionDeclStack.size() == 1 || isMainMethod) &&  functionDeclStack.peekFirst() != null && functionDeclStack.peekFirst()){
            FunctionInfo info = functionParStack.peekFirst();
            if(info != null){
                LocalIndexPool localIndexPool = ASTUtils.getSafeLocalIndexPool(node);
                int localIndex;
                SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
                for(String key : info.getFormalParameters().keySet()){
                    localIndex = localIndexPool.getLocalIndex(info.getFormalParameters().get(key).getType());
                    VariableInfo vinfo = (VariableInfo)symbolTable.lookup(new Identifier(key, IdentifierType.VARIABLE.value()));
                    if(vinfo == null)
                        ASTUtils.error(node, "Parameter not found in sym table");
                    vinfo.setIndex(localIndex);
                    info.getFormalParameters().get(key).setIndex(localIndex);
    
                }
                info.setIndex(localIndexPool.getMaxLocals());
            }
            //visited the node
            functionDeclStack.pop();
           functionDeclStack.push(false);
        }
        
        for (Statement s : node.getStatements()) {
			s.accept(this);
		}
    }

    @Override
    public void visit(ExpressionStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(PropertyDeclarationStatement node) throws ASTVisitorException {
        node.getPropertyDeclaration().accept(this);
    }

    @Override
    public void visit(FunctionDeclarationStatement node) throws ASTVisitorException {
        node.getFunctionDeclaration().accept(this);
    }

    @Override
    public void visit(IfStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
		node.getStatement().accept(this);
    }

    @Override
    public void visit(IfElseStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
		node.getStatement1().accept(this);
		node.getStatement2().accept(this);
    }

    @Override
    public void visit(ReturnStatement node) throws ASTVisitorException {
        if (node.getExpression() != null)
            node.getExpression().accept(this);
    }

    @Override
    public void visit(ContinueStatement node) throws ASTVisitorException {

    }

    @Override
    public void visit(BreakStatement node) throws ASTVisitorException {

    }

    @Override
    public void visit(WhileStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
		node.getStatement().accept(this);
    }

    @Override
    public void visit(DoWhileStatement node) throws ASTVisitorException {
        node.getStatement().accept(this);
        node.getExpression().accept(this);
    }

    @Override
    public void visit(ForStatement node) throws ASTVisitorException {
        forLoopControlStack.push(true);
        node.getVariableDeclaration().accept(this);
        forLoopControlStack.pop();
        node.getExpression().accept(this);
		node.getStatement().accept(this);
    }

    @Override
    public void visit(ForStatementWithStep node) throws ASTVisitorException {
        forLoopControlStack.push(true);
        node.getVariableDeclaration().accept(this);
        forLoopControlStack.pop();
		node.getExpression().accept(this);
		node.getStepExpression().accept(this);
		node.getStatement().accept(this);
    }

    @Override
    public void visit(Parameter node) throws ASTVisitorException {
        
    }
    
}
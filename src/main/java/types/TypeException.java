/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package types;
/**
 * A helper class for proper error messaging when type errors occur.
 */
public class TypeException extends Exception {

    private static final long serialVersionUID = 1L;

    public TypeException() {
        super();
    }

    public TypeException(String msg) {
        super(msg);
    }

    public TypeException(String msg, Throwable t) {
        super(msg, t);
    }

    public TypeException(Throwable t) {
        super(t);
    }

}

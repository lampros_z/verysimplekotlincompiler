/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package types;

import ast.Operator;
import symbol.ParameterInfo;
import java.util.Map;
import java.util.Set;
import org.objectweb.asm.Type;

/**
 * Helper class for type manipulations.
 */
public class TypeUtils {

	public static final Type STRING_TYPE = Type.getType(String.class);

	private TypeUtils() {
	}

	public static Type maxType(Type type1, Type type2) {
		if(type1.equals(STRING_TYPE)){
			return type1;
		}else if (type2.equals(STRING_TYPE)){
			return type2;
		}else if (type1.equals(Type.DOUBLE_TYPE)) {
			return type1;
		} else if (type2.equals(Type.DOUBLE_TYPE)) {
			return type2;
		} else if (type1.equals(Type.INT_TYPE)) {
			return type1;
		} else if (type2.equals(Type.INT_TYPE)) {
			return type2;
		} else if (type1.equals(Type.CHAR_TYPE)) {
			return type1;
		} else if (type2.equals(Type.CHAR_TYPE)) {
			return type2;
		} else if (type1.equals(Type.BOOLEAN_TYPE)) {
			return type1;
		} else if (type2.equals(Type.BOOLEAN_TYPE)) {
			return type2;
		} else {
			return type1;
		}
	}

	public static Type minType(Type type1, Type type2) {
		if (type1.equals(Type.BOOLEAN_TYPE)) {
			return type1;
		} else if (type2.equals(Type.BOOLEAN_TYPE)) {
			return type2;
		} else if (type1.equals(Type.CHAR_TYPE)) {
			return type1;
		} else if (type2.equals(Type.CHAR_TYPE)) {
			return type2;
		} else if (type1.equals(Type.INT_TYPE)) {
			return type1;
		} else if (type2.equals(Type.INT_TYPE)) {
			return type2;
		} else if (type1.equals(Type.DOUBLE_TYPE)) {
			return type1;
		} else if (type2.equals(Type.DOUBLE_TYPE)) {
			return type2;
		} else if (type1.equals(STRING_TYPE)) {
			return type1;
		} else if (type2.equals(STRING_TYPE)) {
			return type2;
		} else {
			return type1;
		}
	}

	public static boolean isLargerOrEqualType(Type type1, Type type2) {
		return type1.getSort() >= type2.getSort();
	}

	public static boolean isEqualType(Type type1, Type type2){
		return type1.getSort() == type2.getSort();
	}

	public static boolean isAssignable(Type target, Type source) {
		return isLargerOrEqualType(target, source);
	}

	public static Type maxType(Set<Type> types) {
		Type max = null;
		for (Type t : types) {
			if (max == null) {
				max = t;
			}
			max = maxType(max, t);
		}
		return max;
	}

	public static Type minType(Set<Type> types) {
		Type min = null;
		for (Type t : types) {
			if (min == null) {
				min = t;
			}
			min = minType(min, t);
		}
		return min;
	}

	public static boolean isUnaryCompatible(Operator op, Type type) {
		switch (op) {
			case MINUS:
				return isNumber(type);
			case MINUSMINUS:
				return isNumber(type);
			case PLUSPLUS:
				return isNumber(type);
			case NOT:
				return isBooleanType(type);
			default:
				return false;
		}
	}

	public static boolean isPosfixUnaryCompatible(Operator op, Type type) {
		switch (op) {
			case MINUSMINUS:
				return isNumber(type);
			case PLUSPLUS:
				return isNumber(type);
			default:
				return false;
		}
	}

	public static boolean isNumber(Type type) {
		return type.equals(Type.INT_TYPE) || type.equals(Type.DOUBLE_TYPE);
	}

	public static boolean isBooleanType(Type type){
		return type.equals(Type.BOOLEAN_TYPE);
	}

	public static boolean isNumber(Set<Type> types) {
		for (Type t : types) {
			if (t.equals(Type.INT_TYPE) || t.equals(Type.DOUBLE_TYPE)) {
				return true;
			}
		}
		return false;
	}

	public static Type applyPrefixUnary(Operator op, Type type) throws TypeException {
		if (!op.isPrefixUnary()) {
			throw new TypeException("Operator " + op + " is not unary");
		}
		if (!TypeUtils.isUnaryCompatible(op, type)) {
			throw new TypeException("Type " + type + " is not unary compatible");
		}
		return type;
	}

	public static Type applyPostfixUnary(Operator op, Type type) throws TypeException {
		if (!op.isPostfixUnary()) {
			throw new TypeException("Operator " + op + " is not unary");
		}
		if (!TypeUtils.isPosfixUnaryCompatible(op, type)) {
			throw new TypeException("Type " + type + " is not unary compatible");
		}
		return type;
	}

	public static Type applyBinary(Operator op, Type t1, Type t2) throws TypeException {
		if (op.isRelational()) {
			if (TypeUtils.areComparable(t1, t2)) {
				return Type.BOOLEAN_TYPE;
			} else {
				throw new TypeException("Expressions are not comparable");
			}
		} else if (op.equals(Operator.PLUS) || op.equals(Operator.MINUS) 
		|| op.equals(Operator.DIVISION) || op.equals(Operator.MULTIPLY) 
		|| op.equals(Operator.MODULO)) {
			if (t1.equals(TypeUtils.STRING_TYPE) || t2.equals(TypeUtils.STRING_TYPE)
			|| t1.equals(Type.CHAR_TYPE) || t2.equals(Type.CHAR_TYPE) 
			|| t1.equals(Type.VOID_TYPE) || t2.equals(Type.VOID_TYPE)
			|| t1.equals(Type.BOOLEAN_TYPE) || t2.equals(Type.BOOLEAN_TYPE)
			) {
				throw new TypeException("Expressions cannot be handled as numbers");
			}
			return maxType(t1, t2);
		} else if (op.isLogical()){
			if( !(isBooleanType(t1)) || !(isBooleanType(t2)) )
				throw new TypeException("Cannot apply logical operator on non boolean expresion");
			return Type.BOOLEAN_TYPE;
		} else if (op.equals(Operator.RANGE_TO) || op.equals(Operator.DOWN_TO)){
			if( !(t1.equals(Type.INT_TYPE)) || !(t2.equals(Type.INT_TYPE)) ){
				throw new TypeException("Range operators apply only on integers!");
			}
			return Type.INT_TYPE;
		} else if (op.equals(Operator.IN) || op.equals(Operator.NOT_IN)){
			if(TypeUtils.areComparableForInOperator(t1, t2)){
				return Type.BOOLEAN_TYPE;
			}else{
				throw new TypeException("Expressions are not comparable for in operator");	
			}
		} else if ( op.equals(Operator.IS) ){
			if(TypeUtils.isEqualType(t1, t2)){
				return Type.BOOLEAN_TYPE;
			}else{
				throw new TypeException("Incompatible types: " + t1.toString() + " " + t2.toString() );
			}
		} else {
			throw new TypeException("Operator " + op + " not supported");
		}
	}

	public static boolean applyForLoopTypeChecking(Type type1, Type type2){
		if (type1.equals(Type.INT_TYPE)) {
			return type2.equals(Type.INT_TYPE);
		} else if (type1.equals(Type.CHAR_TYPE)) {
			return type2.equals(Type.CHAR_TYPE) || type2.equals(STRING_TYPE);
		} else if (type1.equals(STRING_TYPE)) {
			return type2.equals(STRING_TYPE);
		} else {
			return false;
		}
	}

	public static boolean areComparable(Type type1, Type type2) {
		if(type1.equals(Type.BOOLEAN_TYPE)){
			return type2.equals(Type.BOOLEAN_TYPE);
		}
		if (type1.equals(Type.INT_TYPE)) {
			return type2.equals(Type.INT_TYPE) || type2.equals(Type.DOUBLE_TYPE);
		} else if (type1.equals(Type.DOUBLE_TYPE)) {
			return type2.equals(Type.INT_TYPE) || type2.equals(Type.DOUBLE_TYPE);
		} else {
			return false;
		}
	}

	public static boolean areComparableForInOperator(Type type1, Type type2) {
		if (type1.equals(Type.INT_TYPE)) {
			return type2.equals(Type.INT_TYPE);
		} else if (type1.equals(Type.CHAR_TYPE)) {
			return type2.equals(Type.CHAR_TYPE) || type2.equals(STRING_TYPE);
		} else if (type1.equals(STRING_TYPE)) {
			return type2.equals(STRING_TYPE);
		} else {
			return false;
		}
	}

	public static String applyTypePromotion(Type[] target, Type[] src) {
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		if(target.length != src.length)
			return null;
		for(int i = 0; i < target.length; i++){
			if(!isAssignable(target[i], src[i]))
				return null;
			else
				sb.append(target[i].toString());	
		}
		sb.append(")");
		return sb.toString();
	}

	public static String applyTypePromotion(Map<String, ParameterInfo> target, Map<String, Type> src, Map<String, Boolean> unknownParameters) {
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for(String key : src.keySet()){
			ParameterInfo targeType = target.get(key);
			Type sType = src.get(key);
			if(targeType==null){
				if(unknownParameters.get(key)==null){
					unknownParameters.put(key, true);
					return null;
				}
				if(unknownParameters.get(key)!=false)
					unknownParameters.put(key, true);
				return null;
			}
			unknownParameters.put(key, false);
			if(!isAssignable(targeType.getType(), sType)){
				return null;
			}else{
				sb.append(targeType.getType().toString());
			}

		}
		sb.append(")");
		return sb.toString();
	}

}

/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package symbol;

import java.util.Iterator;
import java.util.Map;
import org.objectweb.asm.Type;


/**
 * A subclass of Info class that holds specific information about a function identifier.
 */
public class FunctionInfo extends Info {

    private Type methodType;
    private Map<String, ParameterInfo> formalParameters;

    public FunctionInfo() {
        super(-1);
    }

    public FunctionInfo(String methodDescriptor, Map<String, ParameterInfo> formalParameters) {
        super(-1);
        this.methodType = Type.getMethodType(methodDescriptor);
        this.formalParameters = formalParameters;
    }

    public FunctionInfo(Type methodType) {
        super(-1);
        this.methodType = methodType;
    }

    public FunctionInfo(String methodDescriptor, Map<String, ParameterInfo> formalParameters, Integer index) {
        super(index);
        this.methodType = Type.getMethodType(methodDescriptor);
        this.formalParameters = formalParameters;
    }

    public FunctionInfo(Type methodType, Integer index) {
        super(index);
        this.methodType = methodType;
    }

    public Type getMethodType() {
        return methodType;
    }

    public void setMethodType(Type methodType) {
        this.methodType = methodType;
    }

    public void setMethodType(String methodDescriptor) {
        this.methodType = Type.getMethodType(methodDescriptor);
    }

    public Type getReturnType(){
        if( methodType != null )
            return methodType.getReturnType();
        return null;
    }

    public Map<String, ParameterInfo> getFormalParameters() {
        return formalParameters;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("\n");

        Iterator<String> keyIterator = formalParameters.keySet().iterator();
        if(formalParameters.size() > 0)
            sb.append("[\n");
        String key = "";
        while( keyIterator.hasNext() ){
            key = keyIterator.next();
            sb.append("    ");
            sb.append(formalParameters.get(key).getType().toString());
            sb.append(" : ");
            sb.append(formalParameters.get(key).getIndex());
            if(keyIterator.hasNext())
                sb.append(",\n");
            else
                sb.append("\n]");
        }
        return sb.toString();
    }

    

}
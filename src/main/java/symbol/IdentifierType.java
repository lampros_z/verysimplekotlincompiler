package symbol;
/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * An enumerator used by the Identifier class in order to distinguish variable identifiers from function identifiers.  
 **/
public enum IdentifierType {
	VARIABLE(0), 
	FUNCTION(1);

	private final int identifierType;

	private IdentifierType(int identifierType){
		this.identifierType = identifierType;
	}

	public int value() {
        return identifierType;
    }

}

/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package symbol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import types.*;
import org.objectweb.asm.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ast.ASTUtils;
import ast.ASTVisitorException;
import ast.FunctionCallParameter;
import ast.FunctionParameters;
import ast.Parameter;

/**
 * A helper class with static methods.
 * <p>The terms function and method are used interchangeably and means the same thing in this context.</p>
 */
public class FunctionUtils {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionUtils.class);
    
    /**
     * <p>This method is responsible for creating a string representation of a function signature</p>
     * <p>For example function declaration foo(a:Int) returns signature foo(I)</p>
     * @param identifier The function identifier/name(e.g. fun foo(){} || identifier -> foo).
     * @param functionParameters An object that contains a list of function parameters.
     * @return The method signature 
     */
    public static String aqquireSingnature(String identifier, FunctionParameters functionParameters) {
        StringBuilder sb = new StringBuilder();
        sb.append(identifier);
        sb.append("(");
        for (Parameter parameter : functionParameters.getParameters()) {
            sb.append(parameter.getType().getType().toString());
        }
        sb.append(")");
        return sb.toString();
    }

    /**
     * <p>This method is responsible for creating a string representation of a function signature</p>
     * <p>This method differs from aqquireSingnature method in receiveing a reversed list of function parameters.</p>
     * <p>For example function declaration foo(a:Int) returns signature foo(I)</p>
     * @param identifier The function identifier/name(e.g. fun foo(){} || identifier -> foo).
     * @param functionParameters An object that contains a list of function parameters.
     * @return The method signature 
     */
    public static String aqquireSingnatureRev(String identifier, FunctionParameters functionParameters) {
        StringBuilder sb = new StringBuilder();
        sb.append(identifier);
        sb.append("(");
        ListIterator<Parameter> li = functionParameters.getParameters().listIterator(functionParameters.getParameters().size());
        while(li.hasPrevious()) {
            sb.append(li.previous().getType().getType().toString());
          }
        sb.append(")");
        return sb.toString();
    }

    /**
     * <p>This method is responsible for creating a string representation of a function signature</p>
     * <p>For example function declaration foo(a:Int) returns signature foo(I)</p>
     * @param identifier The function identifier/name(e.g. fun foo(){} || identifier -> foo).
     * @param parameterTypes An object that contains a list of function parameter types.
     * @return The method signature 
     */
    public static String aqquireSingnature(String identifier, List<Type> parameterTypes) {
        StringBuilder sb = new StringBuilder();
        sb.append(identifier);
        sb.append("(");
        for (Type paramType : parameterTypes) {
            sb.append(paramType.toString());
        }
        sb.append(")");
        return sb.toString();
    }

    /**
     * <p>This method is responsible for creating a string representation of a function signature</p>
     * <p>For example function declaration foo(a:Int) returns signature foo(I)</p>
     * @param fplist An object that contains a list of function call parameters.
     * @param identifier The function identifier/name(e.g. fun foo(){} || identifier -> foo).
     * @return The method signature 
     */
    public static String aqquireSingnature(List<FunctionCallParameter> fplist, String identifier) {
        StringBuilder sb = new StringBuilder();
        sb.append(identifier);
        sb.append("(");

        for (int i = fplist.size(); i > 0; i--) {
            try {
                sb.append(ASTUtils.getSafeType(fplist.get(i-1)).toString());
            } catch (ASTVisitorException e) {
                LOGGER.info("actual parameters type infernece error!");
            }
        }
        sb.append(")");
        return sb.toString();
    }

    /**
     * <p>This method is responsible for creating a string representation of a method descriptor</p>
     * <p>For example function declaration foo(a:Int):Float returns descriptor (I)D</p>
     * @param functionParameters An object that contains a list of function  parameters.
     * @param returnType The return type of the method.
     * @return The method descriptor. 
     */
    public static String aqquireMethodDescriptor( FunctionParameters functionParameters, Type returnType){
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        for (Parameter parameter : functionParameters.getParameters()) {
            sb.append(parameter.getType().getType().toString());
        }
        sb.append(")");
        if( returnType == null)
            sb.append(Type.VOID_TYPE.toString());
        else
            sb.append(returnType.toString());
        return sb.toString();
    }

    /**
     * 
     * @param fparams an object that contains a list of function parameters.
     * @return Mappings between the parameter identifier and its type.
     */
    public static Map<String, ParameterInfo> getFormalParametersMappings(FunctionParameters fparams){
        Map<String, ParameterInfo> formalParameters = new LinkedHashMap<>();
        for(Parameter param : fparams.getParameters()){
            formalParameters.put(param.getIdentifier(), new ParameterInfo(param.getType().getType()));
        }
        return formalParameters;
    }

    /**
     * 
     * @param symbolTable A symbol table instance.
     * @param identifiers A collection of all the avaiable identifiers from a symbol table.
     * @param inferedSignature A valid function signature.
     * @param namedParams Mappings between named parameters and its' types.
     * @return A string representation of a promoted signature(e.g. signature foo(I) may become foo(D) should needed).
     * @throws ASTVisitorException
     * @throws TypeException
     */
    public static String typePromotion(SymbolTable<Info> symbolTable, Collection<Identifier> identifiers,
    String inferedSignature, Map<String, Type> namedParams) throws ASTVisitorException, TypeException {
        LOGGER.info(Arrays.toString(namedParams.keySet().toArray()));
        /*function call expression information*/
        String desc = convertSignatureToDesc(inferedSignature);
        String calleeId = getIdentifierFromSignature(inferedSignature); 
        Type[] calleeParameterTypes = Type.getArgumentTypes(desc);
        int identifierType = IdentifierType.FUNCTION.value();

        /*function call expression information*/
        if(namedParams.size() > 1){
            Collection<Map<String, ParameterInfo>> fplist = new LinkedList<>();
            Map<String, Boolean> unknownParameters = new HashMap<>();
            for(Identifier identifier : identifiers){
                if(identifier.getIdentifier().equals(calleeId)){
                    if(identifier.getIdentifierType() == identifierType){
                        FunctionInfo lookup = (FunctionInfo)symbolTable.lookup(identifier);
                        Map<String, ParameterInfo> formalParameters = lookup.getFormalParameters();
                        fplist.add(formalParameters);
                    }
                }
            }
            if(fplist.size() == 0){
                return null;
            }
            int counter = 0;
            StringBuilder sb = new StringBuilder();
            sb.append(calleeId);
            String promotedSignature;
            for(Map<String, ParameterInfo> fparams : fplist){
                promotedSignature =  TypeUtils.applyTypePromotion(fparams, namedParams, unknownParameters);
                if(promotedSignature!=null){
                    counter++;
                    sb.append(promotedSignature);
                }
            }
            for(String key : unknownParameters.keySet()){
                if(unknownParameters.get(key))
                    throw new TypeException("Unknown parameter with name: " + key);
            }
            if(counter > 1)
                throw new ASTVisitorException("Conflicting overloads:callee id " + calleeId);
            if( counter == 1)
                return sb.toString();
            else
                return null;
        }else{
            /*Declared functions information */
            Map<String, List<Type[]>> availableFunctions = new HashMap<>();
            for(Identifier identifier : identifiers){
                if(identifier.getIdentifier().equals(calleeId)){
                    if(identifier.getIdentifierType() == identifierType){
                        String idSign = identifier.getIdentifierSignature();
                        String idDesc = convertSignatureToDesc(idSign);
                        Type[] idTypes = Type.getArgumentTypes(idDesc);
                        insertAvaiableFunctions(identifier.getIdentifier(), idTypes, availableFunctions);
                    }
                }
            }
            if(availableFunctions.size() == 0){
                return null;
            }
            int counter = 0;
            StringBuilder sb = new StringBuilder();
            String promotedSignature;
            sb.append(calleeId);
            for(Type[] typeArray : availableFunctions.get(calleeId)){
                promotedSignature =  TypeUtils.applyTypePromotion(typeArray, calleeParameterTypes);
                if(promotedSignature!=null){
                    counter++;
                    sb.append(promotedSignature);
                }
            }
            if(counter > 1)
                throw new ASTVisitorException("Conflicting overloads:callee id " + calleeId);
            if( counter == 1)
                return sb.toString();
            else
                return null;
        }
        
    }
    /**
     * A utility function that receives as input a method signature and returns its descriptor.
     * @param signature A valid method signature.
     * @return A valid method descriptor.
     */
    public static String convertSignatureToDesc(String signature){
        String trimedString = signature.substring(signature.indexOf("(")).trim();
        StringBuilder sb = new StringBuilder(trimedString);
        sb.append(Type.VOID_TYPE.toString());
        return (signature == null)? null : sb.toString();
    }

    public static String getIdentifierFromSignature(String signature){
        String id = signature.substring(0, signature.indexOf("("));
        return (signature == null) ? null : id;
    }

    private static void insertAvaiableFunctions(String identifier, Type[] types,Map<String, List<Type[]>> map){
        if( map.get(identifier) != null ){
            map.get(identifier).add(types);
        }else{
            map.put(identifier, new ArrayList<Type[]>()
            {
                private static final long serialVersionUID = 1L;
                {
                add(types);
            }}
            );
        }
    }

}
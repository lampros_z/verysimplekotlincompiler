/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package symbol;


/**
 * Information about a symbol
 */

public abstract class Info {
    private Integer index;

    public Info(Integer index) {
        this.index = index;
    }
    
    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "Info [index=" + index + "]";
    }
    
    
}

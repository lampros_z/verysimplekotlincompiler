/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package symbol;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
/**
 * An implemention of SymbolTable.
 */
public class HashSymbolTable<E> implements SymbolTable<E> {

	private final Map<Identifier, E> table;
	private final SymbolTable<E> nextSymbolTable;

	public HashSymbolTable() {
		this(null);
	}

	public HashSymbolTable(SymbolTable<E> nextSymTable) {
		this.table = new HashMap<Identifier, E>();
		this.nextSymbolTable = nextSymTable;
	}

	@Override
	public E lookup(Identifier identifier) {
		E r = table.get(identifier);
		if (r != null) {
			return r;
		}
		if (nextSymbolTable != null) {
			return nextSymbolTable.lookup(identifier);
		}
		return null;
	}

	@Override
	public E lookupOnlyInTop(Identifier identifier) {
		return table.get(identifier);
	}

	@Override
	public E put(Identifier identifier, E symbol) {
		return table.put(identifier, symbol);
	}

	@Override
	public Collection<E> getSymbols() {
		List<E> symbols = new ArrayList<E>();
		symbols.addAll(table.values());
		if (nextSymbolTable != null) {
			symbols.addAll(nextSymbolTable.getSymbols());
		}
		return symbols;
	}

	@Override
	public Collection<E> getSymbolsOnlyInTop() {
		List<E> symbols = new ArrayList<E>();
		symbols.addAll(table.values());
		return symbols;
	}

	public Collection<Identifier> getKeys(){
		List<Identifier> keys = new LinkedList<>();
		keys.addAll(table.keySet());
		if (nextSymbolTable != null) {
			keys.addAll(nextSymbolTable.getKeys());
		}
		return keys;
	}

	public Map<Identifier, E> getTable(){
		return this.table;
	}

	@Override
	public void clearOnlyInTop() {
		table.clear();
	}

}

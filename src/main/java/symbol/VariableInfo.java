/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package symbol;

import org.objectweb.asm.Type;
/**
 * A subclass of Info class that holds specific information about a variable.
 */
public class VariableInfo extends Info {

    private Type type;

    public VariableInfo() {
        super(-1);
        this.type = null;
    }

    public VariableInfo(Type type) {
        super(-1);
        this.type = type;
    }

    public VariableInfo(Type type, Integer index) {
        super(index);
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
    
}
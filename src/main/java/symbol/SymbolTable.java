/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 */
package symbol;

import java.util.Collection;

/**
 * Symbol table
 *
 * @param <E> The type of objects that can be stored in the symbol table.
 */
public interface SymbolTable<E> {

	/**
	 * Lookup a symbol in the symbol table.
	 *
	 * @param identifier An object containing all the necessary information for a variable or function identifier. 
	 * @return The entry for the symbol or null if not found.
	 */
	public E lookup(Identifier identifier);

	/**
	 * Lookup a symbol in the symbol table.
	 *
	 * @param identifier An object containing all the necessary information for a variable or function identifier.
	 * @return The entry for the symbol or null if not found.
	 */
	public E lookupOnlyInTop(Identifier identifier);

	/**
	 * Add a new symbol table entry.
	 *
	 * @param identifier An object containing all the necessary information for a variable or function identifier.
	 * @param symbol The actual entry
	 * @return the value if key already exists in the table, null otherwise.
	 */
	public E put(Identifier identifier, E symbol);

	/**
	 * Get all the symbols available in this symbol table.
	 *
	 * @return A collection of symbols.
	 */
	public Collection<E> getSymbols();

	/**
	 * Get all the symbols available in this symbol table.
	 *
	 * @return A collection of symbols.
	 */
	public Collection<E> getSymbolsOnlyInTop();

	/**
	 * Clear all symbol entries.
	 */
	public void clearOnlyInTop();
	
	/**
	 * Get all keys from the symbol table.
	 * * @return A collection of keys.
	 */
	public Collection<Identifier> getKeys();

}

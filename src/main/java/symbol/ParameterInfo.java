package symbol;

import org.objectweb.asm.Type;
/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * A subclass of type Info that holds specific information about method parameters.
 **/
public class ParameterInfo extends Info {

    Type type;

    public ParameterInfo(Type type) {
        super(-1);
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {

        return "ParameterInfo [type=" + type + "]" + getIndex();
    }

    

}
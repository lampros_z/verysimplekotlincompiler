package symbol;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
/**
 * @instructor michail@hua.gr
 * @author it21622@hua.gr
 * Compilers 2019-2020
 * Harokopio University of Athens, Dept. of Informatics and Telematics.
 * <p>This class is responsible for seperating variable identifiers from function identifiers.</p>
 * <p>IMPORTANT: It's used as a key in the symbol table.</p>
 * <p>A valid variable identifier cosnists of an identifier an identifier type of type VARIABLE and finally its signature is its type.</p>
 * <p>A valid method identifier consists of and identifier(e.g foo) an identifier type of type FUNCTION and finally its signature(e.g. foo(IIF))</p>
 * <p>Thanks to this class, method overloading is accomplished in constant time O(1) due to the Hash Map implementation of the symbol table.</p>
 * <p>This class uses the org.apache.commons.lang3.builder.EqualsBuilder and HashCodeBuilder for the overloading of the hash code and equals methods.</p>
 */
public class Identifier {
    /**
     * A method or variable identifier.
     */
    private String identifier;
    /**
     * The type of the identifier FUNCTION OR VARIABLE (see IdentifierType enumeration).
     */
    private int identifierType;
    /**
     * Method signature in case of methods or org.objectweb.asm.Type for variables.
     */
    private String identifierSignature;

    public Identifier(String identifier, int identifierType) {
        this.identifier = identifier;
        this.identifierType = identifierType;
        this.identifierSignature = null;
    }

    public Identifier(String identifier, int identifierType, String identifierTypeDescriptor) {
        this.identifier = identifier;
        this.identifierType = identifierType;
        this.identifierSignature = identifierTypeDescriptor;
    }
    
    public String getIdentifier() {
        return identifier;
    }
    
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
    
    public int getIdentifierType() {
        return identifierType;
    }
    
    public void setIdentifierType(int identifierType) {
        this.identifierType = identifierType;
    }
    
    public String getIdentifierSignature() {
        return identifierSignature;
    }
    
    public void setIdentifierSignature(String identifierSignature) {
        this.identifierSignature = identifierSignature;
    }
    
    @Override
    public int hashCode() {
        
        if( identifierType == IdentifierType.VARIABLE.value() ){
            return new HashCodeBuilder(17, 37)
                .append(identifier)
                .append(identifierType)
                .toHashCode();
        }
        
        return new HashCodeBuilder(17, 37)
                .append(identifier)
                .append(identifierType)
                .append(identifierSignature)
                .toHashCode();
        
    }
    /**
     * Two methods are considered equal(case of illegal overloading) if they have equal signatures.
     */ 
    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Identifier)) {
            return false;
        }
        
        Identifier identifierObj = (Identifier) o;
        
        if( identifierType == IdentifierType.VARIABLE.value() ){
            return new EqualsBuilder()
                    .append(identifier, identifierObj.identifier)
                    .append(identifierType, identifierObj.identifierType)
                    .isEquals();
        }
        
        return new EqualsBuilder()
                .append(identifier, identifierObj.identifier)
                .append(identifierType, identifierObj.identifierType)
                .append(identifierSignature, identifierObj.identifierSignature)
                .isEquals();
    }
    
    @Override
    public String toString() {
        return "Identifier{" + "identifier : " + identifier + ", identifierType : " + identifierType + ", identifierSignature : " + identifierSignature + '}';
    }
    
    
    
}

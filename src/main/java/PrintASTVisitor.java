import java.util.List;
import java.util.ListIterator;
import ast.ASTVisitor;
import ast.ASTVisitorException;
import ast.AssignmentExpression;
import ast.AssignmentStatement;
import ast.BinaryExpression;
import ast.BooleanLiteralExpression;
import ast.BreakStatement;
import ast.CharLiteralExpression;
import ast.CompUnit;
import ast.CompoundStatement;
import ast.ContinueStatement;
import ast.DoWhileStatement;
import ast.DoubleLiteralExpression;
import ast.ExpressionStatement;
import ast.ForStatement;
import ast.ForStatementWithStep;
import ast.FunctionCall;
import ast.FunctionCallExpression;
import ast.FunctionCallParameter;
import ast.FunctionDeclaration;
import ast.FunctionDeclarationStatement;
import ast.FunctionParameters;
import ast.IdentifierExpression;
import ast.IfElseStatement;
import ast.IfStatement;
import ast.IntegerLiteralExpression;
import ast.KotlinFile;
import ast.NullLiteralExpression;
import ast.Parameter;
import ast.ParenthesisExpression;
import ast.PostfixUnaryExpression;
import ast.PrefixUnaryExpression;
import ast.PropertyDeclaration;
import ast.PropertyDeclarationStatement;
import ast.ReturnStatement;
import ast.Statement;
import ast.StringLiteralExpression;
import ast.TypeK;
import ast.ValDeclaration;
import ast.ValDeclarationAssignment;
import ast.VarDeclaration;
import ast.VarDeclarationAssignment;
import ast.VariableDeclaration;
import ast.WhileStatement;

/**
 * Compilers 2020 Harokopio University of Athens, Dept. of Informatics and
 * Telematics.
 * @teacher michail@hua.gr
 * @author it21622
 */

 /**
  * PrintASTVisitor: A visitor class that prints the input program by visiting each node
  * of the abstarct syntax tree that the LR parser has built.
  * WARNING: It is not a thread safe class.
  */
public class PrintASTVisitor implements ASTVisitor {
    /**
     * A field holding the identation level.
     * Identation level increases when the visitor enters at a compound statement.
     * Identation level decreases when the visitor leaves from a compound statement. 
     */
    private int identation = 0;

    @Override
    public void visit(KotlinFile node) throws ASTVisitorException {
        node.getCompUnit().accept(this);
    }

    @Override
    public void visit(CompUnit node) throws ASTVisitorException {
        for (PropertyDeclaration propertyDeclaration : node.getPropertyDeclarations())
            propertyDeclaration.accept(this);
        for (FunctionDeclaration functionDeclaration : node.getFunctionDeclarations())
            functionDeclaration.accept(this);
    }

    @Override
    public void visit(VariableDeclaration node) throws ASTVisitorException {
        System.out.print(node.getIdentifier());
        if (node.getType() != null)
            node.getType().accept(this);
    }

    @Override
    public void visit(TypeK node) throws ASTVisitorException {
        System.out.printf(":%s%s", node.getType().toString(), (node.isNullable() ? "?" : ""));
    }

    @Override
    public void visit(VarDeclaration node) throws ASTVisitorException {
        System.out.print("var ");
        node.getVariableDeclaration().accept(this);
    }

    @Override
    public void visit(ValDeclaration node) throws ASTVisitorException {
        System.out.print("val ");
        node.getVariableDeclaration().accept(this);
    }

    @Override
    public void visit(VarDeclarationAssignment node) throws ASTVisitorException {
        System.out.print("var ");
        node.getVariableDeclaration().accept(this);
        System.out.print(" = ");
        node.getExpression().accept(this);
    }

    @Override
    public void visit(ValDeclarationAssignment node) throws ASTVisitorException {
        System.out.print("val ");
        node.getVariableDeclaration().accept(this);
        System.out.print(" = ");
        node.getExpression().accept(this);
    }

    @Override
    public void visit(FunctionDeclaration node) throws ASTVisitorException {
        System.out.printf("fun %s(", node.getIdentifier());
        node.getFunctionParamaters().accept(this);
        System.out.printf(")");
        if (node.getReturnType() != null)
            node.getReturnType().accept(this);
        node.getStatement().accept(this);
    }

    @Override
    public void visit(FunctionParameters node) throws ASTVisitorException {
        List<Parameter> params = node.getParameters();
        ListIterator<Parameter> iterator = params.listIterator(params.size());
        while(iterator.hasPrevious()){
            iterator.previous().accept(this);
        }
    }

    @Override
    public void visit(StringLiteralExpression node) throws ASTVisitorException {
        System.out.print("\"" + node.getLiteral() + "\"");
    }

    @Override
    public void visit(DoubleLiteralExpression node) throws ASTVisitorException {
        System.out.print(node.getLiteral());
    }

    @Override
    public void visit(IntegerLiteralExpression node) throws ASTVisitorException {
        System.out.print(node.getLiteral());
    }

    @Override
    public void visit(CharLiteralExpression node) throws ASTVisitorException {
        if(node.getLiteral() == '\n'){
            System.out.print("'\\n'");
        }else if(node.getLiteral() == '\t'){
            System.out.print("'\\t'");
        }else if(node.getLiteral() == '\b'){
            System.out.print("'\\b'");
        }else if(node.getLiteral() == '\r'){
            System.out.print("'\\r'");
        }else{
            System.out.print(node.getLiteral());
        }
    }

    @Override
    public void visit(BooleanLiteralExpression node) throws ASTVisitorException {
        System.out.print(node.isLiteral());
    }

    @Override
    public void visit(NullLiteralExpression node) throws ASTVisitorException {
        System.out.print("null");
    }

    @Override
    public void visit(IdentifierExpression node) throws ASTVisitorException {
        System.out.print(node.getIdentifier());
    }

    @Override
    public void visit(ParenthesisExpression node) throws ASTVisitorException {
        System.out.print("( ");
        node.getExpression().accept(this);
        System.out.print(" )");
    }

    @Override
    public void visit(BinaryExpression node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        System.out.printf(" %s ", node.getOperator());
        node.getExpression2().accept(this);
    }

    @Override
    public void visit(PrefixUnaryExpression node) throws ASTVisitorException {
        System.out.print(node.getOperator());
        node.getExpression().accept(this);
    }

    @Override
    public void visit(PostfixUnaryExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
        System.out.print(node.getOperator());
    }

    @Override
    public void visit(AssignmentExpression node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        System.out.printf(" %s ", "=");
        node.getExpression2().accept(this);
    }

    @Override
    public void visit(FunctionCallParameter node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(FunctionCall node) throws ASTVisitorException {
        System.out.printf("%s(", node.getIdentifier());
        List<FunctionCallParameter> fplist = node.getFunctionCallParameters();
        ListIterator<FunctionCallParameter> iterator = fplist.listIterator(fplist.size());
        while(iterator.hasPrevious()){
            iterator.previous().accept(this);
        }
        System.out.print(")");
    }

    @Override
    public void visit(FunctionCallExpression node) throws ASTVisitorException {
        node.getFunctionCall().accept(this);
    }

    @Override
    public void visit(AssignmentStatement node) throws ASTVisitorException {
        node.getExpression1().accept(this);
        System.out.print(" = ");
        node.getExpression2().accept(this);
        System.out.println(";");

    }

    @Override
    public void visit(CompoundStatement node) throws ASTVisitorException {
        identation++;
        System.out.println("{");
        for (Statement st : node.getStatements()) {
            ident();
            st.accept(this);
        }
        identation--;
        ident();
        System.out.println("}");
    }

    @Override
    public void visit(ExpressionStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
        System.out.println(";");
    }

    @Override
    public void visit(PropertyDeclarationStatement node) throws ASTVisitorException {
        node.getPropertyDeclaration().accept(this);
        System.out.println(";");
    }

    @Override
    public void visit(FunctionDeclarationStatement node) throws ASTVisitorException {
        node.getFunctionDeclaration().accept(this);
    }

    @Override
    public void visit(IfStatement node) throws ASTVisitorException {
        System.out.print("if (");
        node.getExpression().accept(this);
        System.out.print(")");
        node.getStatement().accept(this);
    }

    @Override
    public void visit(IfElseStatement node) throws ASTVisitorException {
        System.out.print("if (");
        node.getExpression().accept(this);
        System.out.print(")");
        node.getStatement1().accept(this);
        System.out.print(" else ");
        node.getStatement2().accept(this);
    }

    @Override
    public void visit(ReturnStatement node) throws ASTVisitorException {
        if (node.getExpression() != null) {
            System.out.print("return ");
            node.getExpression().accept(this);
            System.out.println(";");
        } else
            System.out.println("return;");
    }

    @Override
    public void visit(ContinueStatement node) throws ASTVisitorException {
        System.out.println("continue;");
    }

    @Override
    public void visit(BreakStatement node) throws ASTVisitorException {
        System.out.println("break;");
    }

    @Override
    public void visit(WhileStatement node) throws ASTVisitorException {
        System.out.print("while (");
        node.getExpression().accept(this);
        System.out.print(")");
        node.getStatement().accept(this);
    }

    @Override
    public void visit(DoWhileStatement node) throws ASTVisitorException {
        System.out.print("do");
        node.getStatement().accept(this);
        System.out.print("while (");
        node.getExpression().accept(this);
        System.out.println(");");
    }

    @Override
    public void visit(ForStatement node) throws ASTVisitorException {
        System.out.print("for (");
        node.getVariableDeclaration().accept(this);
        System.out.print(" in ");
        node.getExpression().accept(this);
        System.out.print(" )");
        node.getStatement().accept(this);
    }

    @Override
    public void visit(ForStatementWithStep node) throws ASTVisitorException {
        System.out.print("for (");
        node.getVariableDeclaration().accept(this);
        System.out.print(" in ");
        node.getExpression().accept(this);
        System.out.print(" step ");
        node.getStepExpression().accept(this);
        System.out.print(" )");
        node.getStatement().accept(this);
    }

    @Override
    public void visit(Parameter node) throws ASTVisitorException {
        System.out.print(node.getIdentifier());
        if (node.getType() != null)
            node.getType().accept(this);
        else
            throw new ASTVisitorException("Invalid function declaration");
    }
    /**
     * A function that prints spaces in order to simulate the identation process.
     */
    private void ident() {
        for (int i = 0; i < identation; i++)
            System.out.print("    ");
    }

}

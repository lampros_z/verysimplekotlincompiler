import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.IincInsnNode;
import org.objectweb.asm.tree.InnerClassNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.VarInsnNode;
import org.objectweb.asm.util.TraceClassVisitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ast.ASTNode;
import ast.ASTUtils;
import ast.ASTVisitor;
import ast.ASTVisitorException;
import ast.AssignmentExpression;
import ast.AssignmentStatement;
import ast.BinaryExpression;
import ast.BooleanLiteralExpression;
import ast.BreakStatement;
import ast.CharLiteralExpression;
import ast.CompUnit;
import ast.CompoundStatement;
import ast.ContinueStatement;
import ast.DoWhileStatement;
import ast.DoubleLiteralExpression;
import ast.Expression;
import ast.ExpressionStatement;
import ast.ForStatement;
import ast.ForStatementWithStep;
import ast.FunctionCall;
import ast.FunctionCallExpression;
import ast.FunctionCallParameter;
import ast.FunctionDeclaration;
import ast.FunctionDeclarationStatement;
import ast.FunctionParameters;
import ast.IdentifierExpression;
import ast.IfElseStatement;
import ast.IfStatement;
import ast.IntegerLiteralExpression;
import ast.KotlinFile;
import ast.NullLiteralExpression;
import ast.Operator;
import ast.Parameter;
import ast.ParenthesisExpression;
import ast.PostfixUnaryExpression;
import ast.PrefixUnaryExpression;
import ast.PropertyDeclaration;
import ast.PropertyDeclarationStatement;
import ast.ReturnStatement;
import ast.Statement;
import ast.StringLiteralExpression;
import ast.TypeK;
import ast.ValDeclaration;
import ast.ValDeclarationAssignment;
import ast.VarDeclaration;
import ast.VarDeclarationAssignment;
import ast.VariableDeclaration;
import ast.WhileStatement;
import symbol.FunctionInfo;
import symbol.FunctionUtils;
import symbol.Identifier;
import symbol.IdentifierType;
import symbol.Info;
import symbol.LocalIndexPool;
import symbol.ParameterInfo;
import symbol.SymbolTable;
import symbol.VariableInfo;
import types.TypeUtils;

/**
 * Compilers 2020 Harokopio University of Athens, Dept. of Informatics and
 * Telematics.
 * @teacher michail@hua.gr
 * @author it21622
 */

 /**
  * BytecodeGeneratorASTVisitor: A visitor responsible for converting the AST representation of the
  * program to Java Virtual Machine instructions. In order to support nested function declarations,
  * this visitor creates a new Inner class for every nested method and assigns that method to the nested class.
  * Bytecode is produces for each and every nested class, written to a class file and linked to the main class
  * of the program.
  */
public class BytecodeGeneratorASTVisitor implements ASTVisitor {
    /**
	 * static field Logger used for proper Logging and debugging.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(BytecodeGeneratorASTVisitor.class);
    /**
     * A stack useful when the visitor encounters nested function declarations.
     * The boolean value declares that the visitor is in function declaration state.
     * When a nested function declaration [ e.g. fun foo(){fun bar(){}} ] is active this stack ensures that
     * the last activation record (nth record) will not alter the function declaration state to false but it will alter
     * the state at the top of the stack hence the first or second or nth - 1 state will remain true. 
     */
	private final Deque<Boolean> nested;
	/**
     * A stack useful when the visitor encounters nested function calls.
     * The boolean value declares that the visitor is in function call state.
     * When a nested function call [ e.g. foo( a = bar() ) ] is active this stack ensures that
     * the last activation record (nth record) will not alter the function call state to false but it will alter
     * the state at the top of the stack hence the first or second or nth - 1 state will remain true. 
     */
	private final Deque<Boolean> functionCallStack;
	/**
	 * Holds a mapping between a nested class name and its bytecode.
	 */
	private final Map<String, byte[]> nestedMethodClasses;
	/**
	 * Holds a mapping whether a method is nested inside another method or not.
	 */
	private final Map<String, Boolean> nestedMethod;
	/**
	 * A mapping between a function identifier and its inner class name. In other words,
	 * the name of the class that contains the method.
	 */
	private final Map<String, String> nestedMethodIdentifierMapping;
	/**
	 * A stack used to store method nodes.
	 */
	private final Deque<MethodNode> methodNodeStack;
	/**
	 * A stack used to store class nodes.
	 */
	private final Deque<ClassNode> classNodeStack;
	/**
	 * A stack used to keep track of the previous index pool of a node.
	 * Used in nested function declarations in order to build an ASTORE instruction for the
	 * nested class that contains a nested method.
	 */
	private final Deque<LocalIndexPool> previousIndexPool;
	/**
     * A stack holding  FunctionInfo objects. Useful due to the recursive
     * calls that occur due to the AST traversal. When needed, the top of the stack is retrieved
     * and used.
     */
	private final Deque<Boolean> forLoopControlStack;
	/**
	 * Used to build the nested class names.
	 */
	private final Map<Integer, String> stringCache;
	/**
	 * A stack used to store method invocations in order to change the normal flow of execution
	 * and give the ability to call the method when needed.
	 */
	private final Deque<Runnable> methodInvocationStack;
	/**
	 * A value representing whether the visitor is inside a boolean expression or not.
	 */
	private  Boolean bool = false;
	/**
	 * A field used to count the number of return statements inside a statement list.
	 */
	private Integer returnCounter;
	/**
	 * A counter used to count the number of method declared inside a root method node.
	 */
	private int nestedMethodCounter = 0;
	/**
	 * A counter used produce unique inner class names.
	 */
	private int innerCounter = 0;
	/**
	 * Sets the state to declare that there is a contains operator(in, !in).
	 */
	private boolean containsOperator = false;
	/**
	 * Temporary field for a binary expression.
	 */
	private BinaryExpression tempNode;
	/**
	 * The name of the root class of the program.
	 */
	private final String className;
	/**
	 * A deque used due to the recursive nature of the ASTVisitor.
	 * If the value of the head is true it means that the operator was (.. -> range to).
	 * If the value of the head is false it means that the operator was (downTo).
	 * Hence the name loopDirection.
	 */
	private final Deque<Boolean> loopCounterDirectionStack;
	/**
	 * An ordered map based of the order of the named parameters.
	 * Used to wire the named parameters accordingly when a function call occurs.
	 * (e.g. foo(z=1,w=3);)
	 */
	private final TreeMap<Integer, Runnable> namedParameterMappings;

	/**
	 * Generates a unique name for a nested class.
	 * Nested classes are used in order to support nested methods.
	 * @return a unique inner class name.
	 */
	private String generateNestedClassName(){
		StringBuilder sb;
		if( nestedMethodCounter > 1 ){
			String prefix = stringCache.get(nestedMethodCounter);
			sb = new StringBuilder(prefix);
			++innerCounter;
			sb.append("$1Inner" + innerCounter);
			stringCache.put((nestedMethodCounter + 1),  sb.toString());
		} else {
			sb = new StringBuilder(className+"$1");
			++innerCounter;
			sb.append("Inner" + innerCounter);
			stringCache.put((nestedMethodCounter + 1),  sb.toString());
		}
		return sb.toString();
	}

	/**
	 * Creates a class node for the nested method.
	 * @param name The unique name of the inner class.
	 * @param node FunctionDeclaration node holds information about a method(identifier, parameters, return type, statement list etc).
	 * @throws ASTVisitorException
	 */
	private void innerClassGenerator(String name, FunctionDeclaration node) throws ASTVisitorException {
        ClassNode cn = new ClassNode();
        cn.access = Opcodes.ACC_PUBLIC;
        cn.version = Opcodes.V1_5;
        cn.name = name;
		cn.superName = "java/lang/Object";
		
		classNodeStack.push(cn);
        
        { // create default constructor
            MethodNode mn = new MethodNode(Opcodes.ACC_PUBLIC, "<init>", "()V", null, null);
            mn.instructions.add(new VarInsnNode(Opcodes.ALOAD, 0));
            mn.instructions.add(new MethodInsnNode(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V"));
            mn.instructions.add(new InsnNode(Opcodes.RETURN));
            mn.maxLocals = 1;
            mn.maxStack = 1;
            cn.methods.add(mn);
		}
		String id = node.getIdentifier();
		String signature = FunctionUtils.aqquireSingnatureRev(id, node.getFunctionParamaters());
		Identifier identifier = new Identifier(id, IdentifierType.FUNCTION.value(), signature);
		SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
		LocalIndexPool localIndexPool = ASTUtils.getSafeLocalIndexPool(node);
		FunctionInfo info = (FunctionInfo)symbolTable.lookup(identifier);
		MethodNode mn = new MethodNode(Opcodes.ACC_PUBLIC, id, info.getMethodType().getDescriptor(), null, null);
		//create nested method
		//push to stack
		methodNodeStack.push(mn);
		previousIndexPool.push(localIndexPool);
		returnCounter = 0;
		node.getStatement().accept(this);
		if(returnCounter == 0 && node.getReturnType() == null){
			methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.RETURN));
		} else if(returnCounter == 0 && node.getReturnType() != null ){
			ASTUtils.error(node, "Missing of return statement in function: " + node.getIdentifier());
		}else{
			returnCounter--;
		}

		previousIndexPool.pop();
		classNodeStack.peekFirst().methods.add((methodNodeStack.pop()));
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        TraceClassVisitor cv = new TraceClassVisitor(cw, new PrintWriter(System.out));
        classNodeStack.pop().accept(cv);
        nestedMethodClasses.put(name, cw.toByteArray());
    }
	
	/**
	 * Base constructor.
	 * @param className The name of the root class of the program.
	 */
	public BytecodeGeneratorASTVisitor(String className) {
		this.className = className;
		nested = new ArrayDeque<>();
		nestedMethodClasses = new LinkedHashMap<>();
		methodNodeStack = new ArrayDeque<>();
		classNodeStack = new ArrayDeque<>();
		previousIndexPool = new ArrayDeque<>();
		functionCallStack = new ArrayDeque<>();
		forLoopControlStack = new ArrayDeque<>();
		stringCache = new HashMap<>();
		nestedMethod = new HashMap<>();
		nestedMethodIdentifierMapping = new HashMap<>();
		loopCounterDirectionStack = new ArrayDeque<>();
		methodInvocationStack = new ArrayDeque<>();
		namedParameterMappings = new TreeMap<>();
		// create Main class
		ClassNode cn = new ClassNode();
		cn.access = Opcodes.ACC_PUBLIC;
		cn.version = Opcodes.V1_5;
		cn.name = className;
		cn.superName = "java/lang/Object";
		classNodeStack.push(cn);

		// create constructor
		MethodNode mn = new MethodNode(Opcodes.ACC_PUBLIC, "<init>", "()V", null, null);
		mn.instructions.add(new VarInsnNode(Opcodes.ALOAD, 0));
		mn.instructions.add(new MethodInsnNode(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V"));
		mn.instructions.add(new InsnNode(Opcodes.RETURN));
		mn.maxLocals = 1;
		mn.maxStack = 1;
		cn.methods.add(mn);

	}

	public ClassNode getClassNode() {
		return classNodeStack.peekFirst();
	}

	public Deque<ClassNode> getAllClassNodes(){
		return classNodeStack;
	}

	public Map<String, byte[]> getAllNestedClassNodes(){
		return nestedMethodClasses;
	}

    @Override
    public void visit(KotlinFile node) throws ASTVisitorException {
		MethodNode mn = new MethodNode(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);
		methodNodeStack.push(mn);
		node.getCompUnit().accept(this);
		mn.maxLocals = ASTUtils.getSafeLocalIndexPool(node).getMaxLocals() + 1;
		// IMPORTANT: this should be dynamically calculated
		// use COMPUTE_MAXS when computing the ClassWriter,
		// e.g. new ClassWriter(ClassWriter.COMPUTE_MAXS)
		mn.maxStack = 32;
		classNodeStack.peekFirst().methods.add(mn);
    }

    @Override
    public void visit(CompUnit node) throws ASTVisitorException {
        for (FunctionDeclaration functionDeclaration : node.getFunctionDeclarations()) {
			functionDeclaration.accept(this);
		}
        
        for (PropertyDeclaration propertyDeclaration : node.getPropertyDeclarations()) {
			propertyDeclaration.accept(this);
		}
    }

    @Override
    public void visit(VariableDeclaration node) throws ASTVisitorException {
        // Nothing

    }

    @Override
    public void visit(TypeK node) throws ASTVisitorException {
        // Nothing
    }

    @Override
    public void visit(VarDeclaration node) throws ASTVisitorException {
        // Nothing
    }

    @Override
    public void visit(ValDeclaration node) throws ASTVisitorException {
        // Nothing
    }

    @Override
    public void visit(VarDeclarationAssignment node) throws ASTVisitorException {
		node.getExpression().accept(this);
		SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
		VariableInfo info  =(VariableInfo)symbolTable.lookup(new Identifier(node.getVariableDeclaration().getIdentifier(), IdentifierType.VARIABLE.value()));
		methodNodeStack.peekFirst().instructions.add(new VarInsnNode(info.getType().getOpcode(Opcodes.ISTORE), info.getIndex()));
    }

    @Override
    public void visit(ValDeclarationAssignment node) throws ASTVisitorException {
		node.getExpression().accept(this);
		SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
		VariableInfo info  =(VariableInfo)symbolTable.lookup(new Identifier(node.getVariableDeclaration().getIdentifier(), IdentifierType.VARIABLE.value()));
		methodNodeStack.peekFirst().instructions.add(new VarInsnNode(info.getType().getOpcode(Opcodes.ISTORE), info.getIndex()));
    }

    @Override
    public void visit(FunctionDeclaration node) throws ASTVisitorException {
		
		String id = node.getIdentifier();
		if(!id.equals("main")){
			String signature = FunctionUtils.aqquireSingnatureRev(id, node.getFunctionParamaters());
			Identifier identifier = new Identifier(id, IdentifierType.FUNCTION.value(), signature);
			SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
			FunctionInfo info = (FunctionInfo)symbolTable.lookup(identifier);
			LocalIndexPool localIndexPool = ASTUtils.getSafeLocalIndexPool(node);
			if( info == null )
				ASTUtils.error(node, "Internal compiler error function");

			//nested function
			if( nested.peekFirst() != null && nested.peekFirst()){
				nestedMethodCounter++;
				nested.push(true);

				
				String innerClassInternalName = generateNestedClassName();
				String innerClassName = innerClassInternalName.substring(innerClassInternalName.lastIndexOf("$") + 2);
				InnerClassNode icn = new InnerClassNode(innerClassInternalName, null, innerClassName, 0);
				nestedMethod.put(id, true);
				nestedMethodIdentifierMapping.put(id, innerClassInternalName);

				classNodeStack.peekFirst().innerClasses.add(icn);

				//astore Main$1Inner to previous stack frame
				methodNodeStack.peekFirst().instructions.add(new TypeInsnNode(Opcodes.NEW, innerClassInternalName));
				LocalIndexPool previousLocalIndexPool = previousIndexPool.peekFirst();
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.DUP));
				methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ASTORE, previousLocalIndexPool.setClassReferenceIndex()));
				methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKESPECIAL, innerClassInternalName, "<init>", "()V", false));
				classNodeStack.peekFirst().innerClasses.add(icn);
				
				//create inner class
				innerClassGenerator(innerClassInternalName, node);
				
				nestedMethodCounter--;
				nested.pop();
			} else {
				//normal static method inside main function

				MethodNode methodNode = new MethodNode(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, id, info.getMethodType().getDescriptor(), null, null);
				methodNodeStack.push(methodNode);
				nested.push(true);
				previousIndexPool.push(localIndexPool);
				returnCounter = 0;
				node.getStatement().accept(this);
				if(returnCounter == 0 && node.getReturnType() == null ){
					methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.RETURN));
				} else if(returnCounter == 0 && node.getReturnType() != null ){
					ASTUtils.error(node, "Missing of return statement in function: " + node.getIdentifier());
				} else {
					returnCounter--;

				}
				classNodeStack.peekFirst().methods.add(methodNodeStack.pop());
				previousIndexPool.pop();
				nested.pop();
				//end of normal function
			}
		}else{
			returnCounter = 0;
			node.getStatement().accept(this);
			if(returnCounter == 0){
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.RETURN));
			}else{
				returnCounter--;
				
			}
		}
		
    }

    @Override
    public void visit(FunctionParameters node) throws ASTVisitorException {
        // Nothing to do here
    }

    @Override
    public void visit(StringLiteralExpression node) throws ASTVisitorException {
		if (ASTUtils.isBooleanExpression(node)) {
            ASTUtils.error(node, "Strings cannot be used as boolean expressions");
        } else {
            methodNodeStack.peekFirst().instructions.add(new LdcInsnNode(node.getLiteral()));
        }
    }

    @Override
    public void visit(DoubleLiteralExpression node) throws ASTVisitorException {
		if (ASTUtils.isBooleanExpression(node)) {
			JumpInsnNode i = new JumpInsnNode(Opcodes.GOTO, null);
			methodNodeStack.peekFirst().instructions.add(i);
			if (node.getLiteral() != 0d) {
				ASTUtils.getTrueList(node).add(i);
			} else {
				ASTUtils.getFalseList(node).add(i);
			}
		} else {
			Double d = node.getLiteral();
			methodNodeStack.peekFirst().instructions.add(new LdcInsnNode(d));
		}
    }

    @Override
    public void visit(IntegerLiteralExpression node) throws ASTVisitorException {
		if (ASTUtils.isBooleanExpression(node)) {
			JumpInsnNode i = new JumpInsnNode(Opcodes.GOTO, null);
			methodNodeStack.peekFirst().instructions.add(i);
			if (node.getLiteral() != 0) {
				ASTUtils.getTrueList(node).add(i);
			} else {
				ASTUtils.getFalseList(node).add(i);
			}
		} else {
			methodNodeStack.peekFirst().instructions.add(new LdcInsnNode(node.getLiteral()));
		}
    }

    @Override
    public void visit(CharLiteralExpression node) throws ASTVisitorException {
		if (ASTUtils.isBooleanExpression(node)) {
            ASTUtils.error(node, "Chars cannot be used as boolean expressions");
        } else {
			int literal = (int)node.getLiteral();
			methodNodeStack.peekFirst().instructions.add(new LdcInsnNode(literal));
        }
    }

    @Override
    public void visit(BooleanLiteralExpression node) throws ASTVisitorException {
		if (ASTUtils.isBooleanExpression(node)) {
			JumpInsnNode i = new JumpInsnNode(Opcodes.GOTO, null);
			methodNodeStack.peekFirst().instructions.add(i);
			if (node.isLiteral()) {
                ASTUtils.getTrueList(node).add(i);
            } else {
                ASTUtils.getFalseList(node).add(i);
            }
        } else {
			int literal = node.isLiteral() ? 1 : 0;
			methodNodeStack.peekFirst().instructions.add(new LdcInsnNode(literal));
        }
    }

    @Override
    public void visit(NullLiteralExpression node) throws ASTVisitorException {
		if (ASTUtils.isBooleanExpression(node)) {
            ASTUtils.error(node, "Nulls cannot be used as boolean expressions");
        } else {
			methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.ACONST_NULL));
        }
    }

    @Override
    public void visit(IdentifierExpression node) throws ASTVisitorException {
		SymbolTable<Info> safeSymbolTable = ASTUtils.getSafeSymbolTable(node);
		VariableInfo lookup = (VariableInfo)safeSymbolTable.lookup(new Identifier(node.getIdentifier(), IdentifierType.VARIABLE.value()));
		Type type = lookup.getType();
		Integer index = lookup.getIndex();
		if(ASTUtils.isBooleanExpression(node)){
			methodNodeStack.peekFirst().instructions.add(new VarInsnNode(type.getOpcode(Opcodes.ILOAD), index));
			methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.ICONST_1));
			JumpInsnNode jmp = new JumpInsnNode(Opcodes.IF_ICMPNE, null);
			methodNodeStack.peekFirst().instructions.add(jmp);
			ASTUtils.getFalseList(node).add(jmp);
		} else if(containsOperator){
			ASTUtils.setParamIndexProperty(node, index);
		}else{
			if(functionCallStack.peekFirst() != null && functionCallStack.peekFirst()){
				if(index < 0)
					index = ASTUtils.getParamIndexProperty(node);
			}
			methodNodeStack.peekFirst().instructions.add(new VarInsnNode(type.getOpcode(Opcodes.ILOAD), index));
		}
	}

    @Override
    public void visit(ParenthesisExpression node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(BinaryExpression node) throws ASTVisitorException {

		if(forLoopControlStack.peekFirst() != null && forLoopControlStack.peekFirst()){

			Runnable expr1 = () -> {
				try {
					node.getExpression1().accept(this);
				} catch (ASTVisitorException e) {
					LOGGER.error(e.getMessage());
				}
			};
			methodInvocationStack.push(expr1);
			
			Runnable expr2 = () -> {
				try {
					node.getExpression2().accept(this);
				} catch (ASTVisitorException e) {
					LOGGER.error(e.getMessage());
				}
			};
			methodInvocationStack.push(expr2);
	
	
			if(node.getOperator().equals(Operator.RANGE_TO)){
				loopCounterDirectionStack.push(true);
			} else {
				loopCounterDirectionStack.push(false);
			}

			forLoopControlStack.pop();
		}else{

			
			if(node.getOperator().isContains() || node.getOperator().isNotContains()){
				tempNode = node;
				containsOperator = true;
				node.getExpression1().accept(this);
				containsOperator = false;
				Type expr1Type = ASTUtils.getSafeType(node.getExpression1());
				int idx = ASTUtils.getParamIndexProperty((IdentifierExpression)node.getExpression1());
				if(expr1Type.equals(Type.CHAR_TYPE) || expr1Type.equals(TypeUtils.STRING_TYPE)){
					LocalIndexPool localIndexPool = ASTUtils.getSafeLocalIndexPool(node);
					Type expr2Type = ASTUtils.getSafeType(node.getExpression2());
					if(!expr2Type.equals(TypeUtils.STRING_TYPE))
						ASTUtils.error(node, "Invalid use of in operator!");
					methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ILOAD,  idx));
					widen(TypeUtils.STRING_TYPE, expr1Type);
					int index = localIndexPool.getLocalIndex();
					methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ASTORE,  index));
					node.getExpression2().accept(this);
					methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ALOAD,  index));
					methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/String", "contains", "(Ljava/lang/CharSequence;)Z", false));
	
					if(node.getOperator().isContains()){
						JumpInsnNode jmp = new JumpInsnNode(Opcodes.IFEQ, null);
						methodNodeStack.peekFirst().instructions.add(jmp);
						ASTUtils.getFalseList(tempNode).add(jmp);
					}else{
						JumpInsnNode jmp = new JumpInsnNode(Opcodes.IFNE, null);
						methodNodeStack.peekFirst().instructions.add(jmp);
						ASTUtils.getFalseList(tempNode).add(jmp);	
					}
				}else{
					node.getExpression2().setProperty("idx", idx);
					node.getExpression2().setProperty("operator", node.getOperator());
					node.getExpression2().accept(this);
				}
			} else if(node.getOperator().isRange()){
				int idx = (int)node.getProperty("idx");
				methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ILOAD,  idx));
				node.getExpression1().accept(this);
				JumpInsnNode jmp = new JumpInsnNode(Opcodes.IF_ICMPLT, null);
				methodNodeStack.peekFirst().instructions.add(jmp);
				
				methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ILOAD,  idx));
				node.getExpression2().accept(this);
				JumpInsnNode jmp2 = new JumpInsnNode(Opcodes.IF_ICMPGT, null);
				methodNodeStack.peekFirst().instructions.add(jmp2);
				
				Operator operator = (Operator)node.getProperty("operator");
				
				if(operator.isContains()){
					ASTUtils.getFalseList(tempNode).add(jmp);
					ASTUtils.getFalseList(tempNode).add(jmp2);
				}else{
					ASTUtils.getTrueList(tempNode).add(jmp);
					ASTUtils.getTrueList(tempNode).add(jmp2);
				}
				

			}else{
				Type expr1Type = ASTUtils.getSafeType(node.getExpression1());
				Type expr2Type = ASTUtils.getSafeType(node.getExpression2());
				Type maxType = TypeUtils.maxType(expr1Type, expr2Type);
				node.getExpression1().accept(this);
				
				widen(maxType, expr1Type);
				node.getExpression2().accept(this);
				widen(maxType, expr2Type);

				if(ASTUtils.isBooleanExpression(node)){
					handleBooleanOperator(node, node.getOperator(), maxType);
				} else if(maxType.equals(TypeUtils.STRING_TYPE)){
					methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.SWAP));
					handleStringOperator(node, node.getOperator());
				}else{
					handleNumberOperator(node, node.getOperator(), maxType);
				}
			}
		}
    }

    @Override
    public void visit(PrefixUnaryExpression node) throws ASTVisitorException {
		node.getExpression().accept(this);
		if (node.getOperator().equals(Operator.MINUS)) {
			Type type = ASTUtils.getSafeType(node.getExpression());
			methodNodeStack.peekFirst().instructions.add(new InsnNode(type.getOpcode(Opcodes.INEG)));
		} else if (node.getOperator().equals(Operator.NOT)) {
			List<JumpInsnNode> trueList = ASTUtils.getTrueList(node.getExpression());
			List<JumpInsnNode> falseList = ASTUtils.getFalseList(node.getExpression());
			List<JumpInsnNode> tempList = new ArrayList<>(trueList);
			trueList.clear();
			trueList.addAll(falseList);
			falseList.clear();
			falseList.addAll(tempList);
		} else if (node.getOperator().isIncremental()) {
			if( !(node.getExpression() instanceof IdentifierExpression) )
				ASTUtils.error(node, "Illegal unary expression");
			IdentifierExpression idexp = (IdentifierExpression)node.getExpression();
			String identifier = idexp.getIdentifier();
			SymbolTable<Info> safeSymbolTable = ASTUtils.getSafeSymbolTable(node);
			VariableInfo info = (VariableInfo)safeSymbolTable.lookup(new Identifier(identifier, IdentifierType.VARIABLE.value()));
			if(info == null)
				ASTUtils.error(node, "Unary expression runtime error");
			Integer index = info.getIndex();
			if(bool){

				if(node.getOperator().equals(Operator.PLUSPLUS))
					methodNodeStack.peekFirst().instructions.add(new IincInsnNode(index, 1));
				else
					methodNodeStack.peekFirst().instructions.add(new IincInsnNode(index, -1));
				
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.POP));
				methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ILOAD, index));

			}else{
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.ICONST_1));
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.IADD));
				methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ISTORE, index));
			}
		} else {
			ASTUtils.error(node, "Operator not recognized.");
		}
    }

    @Override
    public void visit(PostfixUnaryExpression node) throws ASTVisitorException {
		node.getExpression().accept(this);
		if (node.getOperator().isIncremental()) {
			if( !(node.getExpression() instanceof IdentifierExpression) )
				ASTUtils.error(node, "Illegal unary expression");
			IdentifierExpression idexp = (IdentifierExpression)node.getExpression();
			String identifier = idexp.getIdentifier();
			SymbolTable<Info> safeSymbolTable = ASTUtils.getSafeSymbolTable(node);
			VariableInfo info = (VariableInfo)safeSymbolTable.lookup(new Identifier(identifier, IdentifierType.VARIABLE.value()));
			Integer index = info.getIndex();
			if(bool){
				if(node.getOperator().equals(Operator.PLUSPLUS))
				methodNodeStack.peekFirst().instructions.add(new IincInsnNode(index, 1));
				else
				methodNodeStack.peekFirst().instructions.add(new IincInsnNode(index, -1));
			}else{
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.ICONST_1));
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.IADD));
				methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ISTORE, index));
			}
		} else {
			ASTUtils.error(node, "Operator not recognized.");
		}
    }

    @Override
    public void visit(AssignmentExpression node) throws ASTVisitorException {
		if( functionCallStack.peekFirst() != null && functionCallStack.peekFirst() ){
			if(node.getExpression1() instanceof IdentifierExpression){
				SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
				String namedParamId = ((IdentifierExpression)node.getExpression1()).getIdentifier();
				FunctionInfo funcInfo = (FunctionInfo)node.getProperty("funcInfo");
				if(funcInfo == null)
				ASTUtils.error(node, "Internal compiler error!");
				ParameterInfo parameterInfo = funcInfo.getFormalParameters().get(namedParamId);
				if(parameterInfo == null)
					ASTUtils.error(node, "Unknown named parameter: " + namedParamId);
				if(node.getExpression2() instanceof IdentifierExpression){
					String id = ((IdentifierExpression)node.getExpression2()).getIdentifier();
					VariableInfo info = (VariableInfo)symbolTable.lookup(new Identifier(id, IdentifierType.VARIABLE.value()));
					Type type = info.getType();
					

					Runnable addInsn = () ->  {
						methodNodeStack.peekFirst().instructions.add(new VarInsnNode(type.getOpcode(Opcodes.ILOAD), info.getIndex()));
					};
					namedParameterMappings.put(parameterInfo.getIndex(), addInsn);
				}else{
					Runnable addInsn = () ->  {
						try {
							node.getExpression2().accept(this);
						} catch (ASTVisitorException e) {
							LOGGER.error(e.getLocalizedMessage());
						}
					};
					namedParameterMappings.put(parameterInfo.getIndex(), addInsn);
					
				}
			}
		}else{
			IdentifierExpression idexp = (IdentifierExpression)node.getExpression1();
			String identifier = idexp.getIdentifier();
			SymbolTable<Info> safeSymbolTable = ASTUtils.getSafeSymbolTable(node);
			VariableInfo lookup = (VariableInfo)safeSymbolTable.lookup(new Identifier(identifier, IdentifierType.VARIABLE.value()));
			Type ltype = lookup.getType();
			node.getExpression2().accept(this);
			Type rtype = ASTUtils.getSafeType(node.getExpression2());
			widen(ltype, rtype);
			methodNodeStack.peekFirst().instructions.add(new VarInsnNode(ltype.getOpcode(Opcodes.ISTORE), lookup.getIndex()));
		}
    }

    @Override
    public void visit(FunctionCallParameter node) throws ASTVisitorException {
        
    }

    @Override
    public void visit(FunctionCall node) throws ASTVisitorException {
		functionCallStack.push(true);
		String signature = ASTUtils.getFunctionSignature(node);
		String identifier = signature.substring(0, signature.indexOf("("));
		Identifier id = new Identifier(identifier, IdentifierType.FUNCTION.value(), signature);
		SymbolTable<Info> safeSymbolTable = ASTUtils.getSafeSymbolTable(node);
		FunctionInfo info = (FunctionInfo)safeSymbolTable.lookup(id);
		LocalIndexPool localIndexPool = ASTUtils.getSafeLocalIndexPool(node);
		if(info == null)
			ASTUtils.error(node, "Internal compiler error");
		List<FunctionCallParameter> parLst = node.getFunctionCallParameters();
		ListIterator<FunctionCallParameter> iterator = parLst.listIterator(parLst.size());
		//static invocation from main method
		if(nestedMethod.get(node.getIdentifier()) == null ){
			if(identifier.equals("print")){
			methodNodeStack.peekFirst().instructions.add(new FieldInsnNode(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;"));
			Expression paramExpression;
			namedParameterMappings.clear();
			while(iterator.hasPrevious()){
				paramExpression = iterator.previous().getExpression();		
				if(paramExpression instanceof AssignmentExpression){
					paramExpression.setProperty("funcInfo", info);
					paramExpression.accept(this);
				}
				paramExpression.accept(this);
			}
			if(namedParameterMappings.size() > 0){
				Set<Integer> keys = namedParameterMappings.keySet();
				Iterator<Integer> keyIterator = keys.iterator();
				while(keyIterator.hasNext()){
					namedParameterMappings.get(keyIterator.next()).run();
				}
			}
			methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "print", info.getMethodType().getDescriptor()));
		
		}else{
			Expression paramExpression;
			namedParameterMappings.clear();
			while(iterator.hasPrevious()){
				paramExpression = iterator.previous().getExpression();		
				if(paramExpression instanceof AssignmentExpression){
					paramExpression.setProperty("funcInfo", info);
					paramExpression.accept(this);
				}
				paramExpression.accept(this);
			}
			if(namedParameterMappings.size() > 0){
				Set<Integer> keys = namedParameterMappings.keySet();
				Iterator<Integer> keyIterator = keys.iterator();
				while(keyIterator.hasNext()){
					namedParameterMappings.get(keyIterator.next()).run();
				}
			}
			methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, className, identifier, info.getMethodType().getDescriptor(), false));
		}
			//inner call or recursion call from inner
		} else {
			//aload the class refernce
			methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ALOAD, localIndexPool.getClassReferenceIndex()));
			Expression paramExpression;
			namedParameterMappings.clear();
			while(iterator.hasPrevious()){
				paramExpression = iterator.previous().getExpression();		
				if(paramExpression instanceof AssignmentExpression){
					paramExpression.setProperty("funcInfo", info);
					paramExpression.accept(this);
				}
				paramExpression.accept(this);
			}
			if(namedParameterMappings.size() > 0){
				Set<Integer> keys = namedParameterMappings.keySet();
				Iterator<Integer> keyIterator = keys.iterator();
				while(keyIterator.hasNext()){
					namedParameterMappings.get(keyIterator.next()).run();
				}
			}
			methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, nestedMethodIdentifierMapping.get(node.getIdentifier()), identifier,info.getMethodType().getDescriptor(), false));
		}
		functionCallStack.pop();
    }

    @Override
    public void visit(FunctionCallExpression node) throws ASTVisitorException {
        node.getFunctionCall().accept(this);
    }

    @Override
    public void visit(AssignmentStatement node) throws ASTVisitorException {
		IdentifierExpression idexp = (IdentifierExpression)node.getExpression1();
		String identifier = idexp.getIdentifier();
		SymbolTable<Info> safeSymbolTable = ASTUtils.getSafeSymbolTable(node);
		VariableInfo lookup = (VariableInfo)safeSymbolTable.lookup(new Identifier(identifier, IdentifierType.VARIABLE.value()));
		Type ltype = lookup.getType();
		node.getExpression2().accept(this);
		Type rtype = ASTUtils.getSafeType(node.getExpression2());
		widen(ltype, rtype);
		methodNodeStack.peekFirst().instructions.add(new VarInsnNode(ltype.getOpcode(Opcodes.ISTORE), lookup.getIndex()));
    }

    @Override
    public void visit(CompoundStatement node) throws ASTVisitorException {

		List<JumpInsnNode> breakList = new ArrayList<JumpInsnNode>();
		List<JumpInsnNode> continueList = new ArrayList<JumpInsnNode>();
		Statement s = null, ps;
		Iterator<Statement> it = node.getStatements().iterator();
		while (it.hasNext()) {
			ps = s;
			s = it.next();
			if (ps != null && !ASTUtils.getNextList(ps).isEmpty()) {
				LabelNode labelNode = new LabelNode();
				methodNodeStack.peekFirst().instructions.add(labelNode);
				backpatch(ASTUtils.getNextList(ps), labelNode);
			}
			s.accept(this);
			breakList.addAll(ASTUtils.getBreakList(s));
			continueList.addAll(ASTUtils.getContinueList(s));
		}
	
		if (s != null && !ASTUtils.getNextList(s).isEmpty()) {
			LabelNode labelNode = new LabelNode();
			methodNodeStack.peekFirst().instructions.add(labelNode);
			backpatch(ASTUtils.getNextList(s), labelNode);
		}
		ASTUtils.setBreakList(node, breakList);
		ASTUtils.setContinueList(node, continueList);

    }

    @Override
    public void visit(ExpressionStatement node) throws ASTVisitorException {
        node.getExpression().accept(this);
    }

    @Override
    public void visit(PropertyDeclarationStatement node) throws ASTVisitorException {
        node.getPropertyDeclaration().accept(this);
    }

    @Override
    public void visit(FunctionDeclarationStatement node) throws ASTVisitorException {
        node.getFunctionDeclaration().accept(this);
    }

    @Override
    public void visit(IfStatement node) throws ASTVisitorException {
		ASTUtils.setBooleanExpression(node.getExpression(), true);
		bool = true;
		node.getExpression().accept(this);
		bool = false;
		LabelNode labelNode = new LabelNode();
		methodNodeStack.peekFirst().instructions.add(labelNode);
		backpatch(ASTUtils.getTrueList(node.getExpression()), labelNode);
		node.getStatement().accept(this);

		ASTUtils.getBreakList(node).addAll(ASTUtils.getBreakList(node.getStatement()));
		ASTUtils.getContinueList(node).addAll(ASTUtils.getContinueList(node.getStatement()));

		ASTUtils.getNextList(node).addAll(ASTUtils.getFalseList(node.getExpression()));
		ASTUtils.getNextList(node).addAll(ASTUtils.getNextList(node.getStatement()));
    }

    @Override
    public void visit(IfElseStatement node) throws ASTVisitorException {
		ASTUtils.setBooleanExpression(node.getExpression(), true);
		bool = true;
		node.getExpression().accept(this);
		bool = false;
		LabelNode stmt1StartLabelNode = new LabelNode();
		methodNodeStack.peekFirst().instructions.add(stmt1StartLabelNode);
		node.getStatement1().accept(this);

		JumpInsnNode skipGoto = new JumpInsnNode(Opcodes.GOTO, null);
		methodNodeStack.peekFirst().instructions.add(skipGoto);

		LabelNode stmt2StartLabelNode = new LabelNode();
		methodNodeStack.peekFirst().instructions.add(stmt2StartLabelNode);
		node.getStatement2().accept(this);

		backpatch(ASTUtils.getTrueList(node.getExpression()), stmt1StartLabelNode);
		backpatch(ASTUtils.getFalseList(node.getExpression()), stmt2StartLabelNode);

		ASTUtils.getNextList(node).addAll(ASTUtils.getNextList(node.getStatement1()));
		ASTUtils.getNextList(node).addAll(ASTUtils.getNextList(node.getStatement2()));
		ASTUtils.getNextList(node).add(skipGoto);

		ASTUtils.getBreakList(node).addAll(ASTUtils.getBreakList(node.getStatement1()));
		ASTUtils.getBreakList(node).addAll(ASTUtils.getBreakList(node.getStatement2()));

		ASTUtils.getContinueList(node).addAll(ASTUtils.getContinueList(node.getStatement1()));
		ASTUtils.getContinueList(node).addAll(ASTUtils.getContinueList(node.getStatement2()));
    }

    @Override
    public void visit(ReturnStatement node) throws ASTVisitorException {
		returnCounter++;
		if(node.getExpression() != null){
			Type type = ASTUtils.getSafeType(node.getExpression());
			node.getExpression().accept(this);
			methodNodeStack.peekFirst().instructions.add(new InsnNode(type.getOpcode(Opcodes.IRETURN)));
		}
		else
			methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.RETURN));
    }

    @Override
    public void visit(ContinueStatement node) throws ASTVisitorException {
		JumpInsnNode jmp = new JumpInsnNode(Opcodes.GOTO, null);
		methodNodeStack.peekFirst().instructions.add(jmp);
		ASTUtils.getContinueList(node).add(jmp);
    }

    @Override
    public void visit(BreakStatement node) throws ASTVisitorException {
		JumpInsnNode jmp = new JumpInsnNode(Opcodes.GOTO, null);
		methodNodeStack.peekFirst().instructions.add(jmp);
		ASTUtils.getBreakList(node).add(jmp);
    }

    @Override
    public void visit(WhileStatement node) throws ASTVisitorException {
		ASTUtils.setBooleanExpression(node.getExpression(), true);

		LabelNode beginLabelNode = new LabelNode();
		methodNodeStack.peekFirst().instructions.add(beginLabelNode);
		bool = true;
		node.getExpression().accept(this);
		bool = false;
		LabelNode trueLabelNode = new LabelNode();
		methodNodeStack.peekFirst().instructions.add(trueLabelNode);
		backpatch(ASTUtils.getTrueList(node.getExpression()), trueLabelNode);

		node.getStatement().accept(this);

		backpatch(ASTUtils.getNextList(node.getStatement()), beginLabelNode);
		backpatch(ASTUtils.getContinueList(node.getStatement()), beginLabelNode);

		methodNodeStack.peekFirst().instructions.add(new JumpInsnNode(Opcodes.GOTO, beginLabelNode));

		ASTUtils.getNextList(node).addAll(ASTUtils.getFalseList(node.getExpression()));
		ASTUtils.getNextList(node).addAll(ASTUtils.getBreakList(node.getStatement()));
    }

    @Override
    public void visit(DoWhileStatement node) throws ASTVisitorException {
		ASTUtils.setBooleanExpression(node.getExpression(), true);

		LabelNode beginLabelNode = new LabelNode();
		methodNodeStack.peekFirst().instructions.add(beginLabelNode);

		node.getStatement().accept(this);
		ASTUtils.getNextList(node).addAll(ASTUtils.getBreakList(node.getStatement()));

		LabelNode beginExprLabelNode = new LabelNode();
		methodNodeStack.peekFirst().instructions.add(beginExprLabelNode);
		backpatch(ASTUtils.getNextList(node.getStatement()), beginExprLabelNode);
		backpatch(ASTUtils.getContinueList(node.getStatement()), beginExprLabelNode);
		bool = true;
		node.getExpression().accept(this);
		bool = false;
		ASTUtils.getNextList(node).addAll(ASTUtils.getFalseList(node.getExpression()));
		backpatch(ASTUtils.getTrueList(node.getExpression()), beginLabelNode);
    }

    @Override
    public void visit(ForStatement node) throws ASTVisitorException {
		SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
		String loopVariable = node.getVariableDeclaration().getIdentifier();
		VariableInfo loopVariableInfo = (VariableInfo)symbolTable.lookup(new Identifier(loopVariable, IdentifierType.VARIABLE.value()));
		LocalIndexPool localIndexPool = ASTUtils.getSafeLocalIndexPool(node);
		List<JumpInsnNode> falseList = new ArrayList<>();
		int comparisonVarIndex;
		forLoopControlStack.push(true);

		node.getExpression().accept(this);
		//istore left operand
		methodInvocationStack.removeLast().run();
		methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ISTORE, loopVariableInfo.getIndex()));
		//istore right operand
		methodInvocationStack.removeLast().run();
		comparisonVarIndex = localIndexPool.getLocalIndex(Type.INT_TYPE);
		methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ISTORE, comparisonVarIndex));

		//Label
		LabelNode beginLabelNode = new LabelNode();
		methodNodeStack.peekFirst().instructions.add(beginLabelNode);
		methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ILOAD, loopVariableInfo.getIndex()));
		//iload
		methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ILOAD, comparisonVarIndex));
		JumpInsnNode jmp;
		if(loopCounterDirectionStack.peekFirst()){
			jmp = new JumpInsnNode(Opcodes.IF_ICMPGT, null);
			methodNodeStack.peekFirst().instructions.add( jmp );		
		}else{
			jmp = new JumpInsnNode(Opcodes.IF_ICMPLT, null);
			methodNodeStack.peekFirst().instructions.add( jmp );
		}
		falseList.add(jmp);
		node.getStatement().accept(this);


        if(loopCounterDirectionStack.pop())
			methodNodeStack.peekFirst().instructions.add(new IincInsnNode(loopVariableInfo.getIndex(), 1));
          else 
			methodNodeStack.peekFirst().instructions.add(new IincInsnNode(loopVariableInfo.getIndex(), -1));
            
        
		JumpInsnNode gotoBeginInstr = new JumpInsnNode(Opcodes.GOTO, beginLabelNode);
        methodNodeStack.peekFirst().instructions.add(gotoBeginInstr);

		backpatch(ASTUtils.getNextList(node.getStatement()), beginLabelNode);
		backpatch(ASTUtils.getContinueList(node.getStatement()), beginLabelNode);
		ASTUtils.getNextList(node).addAll(falseList);
		ASTUtils.getNextList(node).addAll(ASTUtils.getBreakList(node.getStatement()));
    }

    @Override
    public void visit(ForStatementWithStep node) throws ASTVisitorException {
		SymbolTable<Info> symbolTable = ASTUtils.getSafeSymbolTable(node);
		String loopVariable = node.getVariableDeclaration().getIdentifier();
		VariableInfo loopVariableInfo = (VariableInfo)symbolTable.lookup(new Identifier(loopVariable, IdentifierType.VARIABLE.value()));
		LocalIndexPool localIndexPool = ASTUtils.getSafeLocalIndexPool(node);
		List<JumpInsnNode> falseList = new ArrayList<>();
		int comparisonVarIndex;
		forLoopControlStack.push(true);

		node.getExpression().accept(this);
		//istore left operand
		methodInvocationStack.removeLast().run();
		methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ISTORE, loopVariableInfo.getIndex()));
		//istore right operand
		methodInvocationStack.removeLast().run();
		comparisonVarIndex = localIndexPool.getLocalIndex(Type.INT_TYPE);
		methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ISTORE, comparisonVarIndex));

		//Label
		LabelNode beginLabelNode = new LabelNode();
		methodNodeStack.peekFirst().instructions.add(beginLabelNode);
		methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ILOAD, loopVariableInfo.getIndex()));
		//iload
		methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ILOAD, comparisonVarIndex));
		JumpInsnNode jmp;
		if(loopCounterDirectionStack.peekFirst()){
			jmp = new JumpInsnNode(Opcodes.IF_ICMPGT, null);
			methodNodeStack.peekFirst().instructions.add( jmp );		
		}else{
			jmp = new JumpInsnNode(Opcodes.IF_ICMPLT, null);
			methodNodeStack.peekFirst().instructions.add( jmp );
		}
		falseList.add(jmp);
		node.getStatement().accept(this);


        loopCounterDirectionStack.pop();
		//iload loopVariableInfo.getIndex()
		methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ILOAD, loopVariableInfo.getIndex()));
		node.getStepExpression().accept(this);
		methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.IADD));
		methodNodeStack.peekFirst().instructions.add(new VarInsnNode(Opcodes.ISTORE, loopVariableInfo.getIndex()));

        
		JumpInsnNode gotoBeginInstr = new JumpInsnNode(Opcodes.GOTO, beginLabelNode);
        methodNodeStack.peekFirst().instructions.add(gotoBeginInstr);

		backpatch(ASTUtils.getNextList(node.getStatement()), beginLabelNode);
		backpatch(ASTUtils.getContinueList(node.getStatement()), beginLabelNode);
		ASTUtils.getNextList(node).addAll(falseList);
		ASTUtils.getNextList(node).addAll(ASTUtils.getBreakList(node.getStatement()));
    }

    @Override
    public void visit(Parameter node) throws ASTVisitorException {
        // Nothing to do here
    }

    private void backpatch(List<JumpInsnNode> list, LabelNode labelNode) {
		if (list == null) {
			return;
		}
		for (JumpInsnNode instr : list) {
			instr.label = labelNode;
		}
	}


	private void widen(Type target, Type source) {
		if (source.equals(target)) {
			return;
		}

		if (source.equals(Type.VOID_TYPE)) {
			return;
		}

		if (source.equals(Type.BOOLEAN_TYPE)) {
			if (target.equals(Type.CHAR_TYPE)) {
				// nothing
			} else if (target.equals(Type.INT_TYPE)) {
				// nothing
			} else if (target.equals(Type.DOUBLE_TYPE)) {
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.I2D));
			} else if (target.equals(TypeUtils.STRING_TYPE)) {
				methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Boolean", "toString",
						"(Z)Ljava/lang/String;"));
			}
		} else if (source.equals(Type.CHAR_TYPE)) {
			if (target.equals(Type.INT_TYPE)) {
				//nothing
			}else if (target.equals(Type.DOUBLE_TYPE)) {
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.I2D));
			} else if (target.equals(TypeUtils.STRING_TYPE)) {
				methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Character", "toString",
						"(C)Ljava/lang/String;"));
			}
		} else if (source.equals(Type.INT_TYPE)) {
			if (target.equals(Type.DOUBLE_TYPE)) {
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.I2D));
			} else if (target.equals(TypeUtils.STRING_TYPE)) {
				methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Integer", "toString",
						"(I)Ljava/lang/String;"));
			}
		} else if (source.equals(Type.DOUBLE_TYPE)) {
			if (target.equals(TypeUtils.STRING_TYPE)) {
				methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Double", "toString",
						"(D)Ljava/lang/String;"));
			}
		}
	}

	private void handleBooleanOperator(Expression node, Operator op, Type type) throws ASTVisitorException {
		List<JumpInsnNode> trueList = new ArrayList<JumpInsnNode>();

		if (type.equals(TypeUtils.STRING_TYPE)) {
			methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.SWAP));
			JumpInsnNode jmp = null;
			methodNodeStack.peekFirst().instructions.add(
					new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z"));
			switch (op) {
			case EQUAL:
				jmp = new JumpInsnNode(Opcodes.IFNE, null);
				break;
			case NOTEQUAL:
				jmp = new JumpInsnNode(Opcodes.IFEQ, null);
				break;
			default:
				ASTUtils.error(node, "Operator not supported on strings");
				break;
			}
			methodNodeStack.peekFirst().instructions.add(jmp);
			trueList.add(jmp);
		} else if (type.equals(Type.DOUBLE_TYPE)) {
			methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.DCMPG));
			JumpInsnNode jmp = null;
			switch (op) {
			case EQUAL:
				jmp = new JumpInsnNode(Opcodes.IFEQ, null);
				methodNodeStack.peekFirst().instructions.add(jmp);
				break;
			case NOTEQUAL:
				jmp = new JumpInsnNode(Opcodes.IFNE, null);
				methodNodeStack.peekFirst().instructions.add(jmp);
				break;
			case GREATER:
				jmp = new JumpInsnNode(Opcodes.IFGT, null);
				methodNodeStack.peekFirst().instructions.add(jmp);
				break;
			case GREATEREQ:
				jmp = new JumpInsnNode(Opcodes.IFGE, null);
				methodNodeStack.peekFirst().instructions.add(jmp);
				break;
			case LESS:
				jmp = new JumpInsnNode(Opcodes.IFLT, null);
				methodNodeStack.peekFirst().instructions.add(jmp);
				break;
			case LESSEQ:
				jmp = new JumpInsnNode(Opcodes.IFLE, null);
				methodNodeStack.peekFirst().instructions.add(jmp);
				break;
			default:
				ASTUtils.error(node, "Operator not supported");
				break;
			}
			trueList.add(jmp);
			List<JumpInsnNode> falseList = new ArrayList<>();
			JumpInsnNode jmpInsnNode = new JumpInsnNode(Opcodes.GOTO, null);
			methodNodeStack.peekFirst().instructions.add(jmpInsnNode);
			falseList.add(jmpInsnNode);
			ASTUtils.setFalseList(node, falseList);
		} else {
			JumpInsnNode jmp = null;
			switch (op) {
			case EQUAL:
				jmp = new JumpInsnNode(Opcodes.IF_ICMPEQ, null);
				methodNodeStack.peekFirst().instructions.add(jmp);
				break;
			case NOTEQUAL:
				jmp = new JumpInsnNode(Opcodes.IF_ICMPNE, null);
				methodNodeStack.peekFirst().instructions.add(jmp);
				break;
			case GREATER:
				jmp = new JumpInsnNode(Opcodes.IF_ICMPGT, null);
				methodNodeStack.peekFirst().instructions.add(jmp);
				break;
			case GREATEREQ:
				jmp = new JumpInsnNode(Opcodes.IF_ICMPGE, null);
				methodNodeStack.peekFirst().instructions.add(jmp);
				break;
			case LESS:
				jmp = new JumpInsnNode(Opcodes.IF_ICMPLT, null);
				methodNodeStack.peekFirst().instructions.add(jmp);
				break;
			case LESSEQ:
				jmp = new JumpInsnNode(Opcodes.IF_ICMPLE, null);
				methodNodeStack.peekFirst().instructions.add(jmp);
				break;
			default:
				ASTUtils.error(node, "Operator not supported");
				break;
			}
			trueList.add(jmp);
		}
		ASTUtils.setTrueList(node, trueList);
		List<JumpInsnNode> falseList = new ArrayList<JumpInsnNode>();
		JumpInsnNode jmp = new JumpInsnNode(Opcodes.GOTO, null);
		methodNodeStack.peekFirst().instructions.add(jmp);
		falseList.add(jmp);
		ASTUtils.setFalseList(node, falseList);
	}


	private void handleStringOperator(ASTNode node, Operator op) throws ASTVisitorException {
		if (op.equals(Operator.PLUS)) {
			methodNodeStack.peekFirst().instructions.add(new TypeInsnNode(Opcodes.NEW, "java/lang/StringBuilder"));
			methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.DUP));
			methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V"));
			methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.SWAP));
			methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "append",
					"(Ljava/lang/String;)Ljava/lang/StringBuilder;"));
					methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.SWAP));
					methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "append",
					"(Ljava/lang/String;)Ljava/lang/StringBuilder;"));
					methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "toString",
					"()Ljava/lang/String;"));
		} else if (op.isRelational()) {
			LabelNode trueLabelNode = new LabelNode();
			switch (op) {
			case EQUAL:
			methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/String", "equals",
						"(Ljava/lang/Object;)Z"));
						methodNodeStack.peekFirst().instructions.add(new JumpInsnNode(Opcodes.IFNE, trueLabelNode));
				break;
			case NOTEQUAL:
			methodNodeStack.peekFirst().instructions.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/String", "equals",
						"(Ljava/lang/Object;)Z"));
						methodNodeStack.peekFirst().instructions.add(new JumpInsnNode(Opcodes.IFEQ, trueLabelNode));
				break;
			default:
				ASTUtils.error(node, "Operator not supported on strings");
				break;
			}
			methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.ICONST_0));
			LabelNode endLabelNode = new LabelNode();
			methodNodeStack.peekFirst().instructions.add(new JumpInsnNode(Opcodes.GOTO, endLabelNode));
			methodNodeStack.peekFirst().instructions.add(trueLabelNode);
			methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.ICONST_1));
			methodNodeStack.peekFirst().instructions.add(endLabelNode);
		} else {
			ASTUtils.error(node, "Operator not recognized");
		}
	}

	private void handleNumberOperator(ASTNode node, Operator op, Type type) throws ASTVisitorException {
		if (op.equals(Operator.PLUS)) {
			methodNodeStack.peekFirst().instructions.add(new InsnNode(type.getOpcode(Opcodes.IADD)));
		} else if (op.equals(Operator.MINUS)) {
			methodNodeStack.peekFirst().instructions.add(new InsnNode(type.getOpcode(Opcodes.ISUB)));
		} else if (op.equals(Operator.MULTIPLY)) {
			methodNodeStack.peekFirst().instructions.add(new InsnNode(type.getOpcode(Opcodes.IMUL)));
		} else if (op.equals(Operator.DIVISION)) {
			methodNodeStack.peekFirst().instructions.add(new InsnNode(type.getOpcode(Opcodes.IDIV)));
		} else if (op.equals(Operator.MODULO)) {
			methodNodeStack.peekFirst().instructions.add(new InsnNode(type.getOpcode(Opcodes.IREM)));
		} else if (op.isRelational()) {
			if (type.equals(Type.DOUBLE_TYPE)) {
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.DCMPG));
				JumpInsnNode jmp = null;
				switch (op) {
				case EQUAL:
					jmp = new JumpInsnNode(Opcodes.IFEQ, null);
					methodNodeStack.peekFirst().instructions.add(jmp);
					break;
				case NOTEQUAL:
					jmp = new JumpInsnNode(Opcodes.IFNE, null);
					methodNodeStack.peekFirst().instructions.add(jmp);
					break;
				case GREATER:
					jmp = new JumpInsnNode(Opcodes.IFGT, null);
					methodNodeStack.peekFirst().instructions.add(jmp);
					break;
				case GREATEREQ:
					jmp = new JumpInsnNode(Opcodes.IFGE, null);
					methodNodeStack.peekFirst().instructions.add(jmp);
					break;
				case LESS:
					jmp = new JumpInsnNode(Opcodes.IFLT, null);
					methodNodeStack.peekFirst().instructions.add(jmp);
					break;
				case LESSEQ:
					jmp = new JumpInsnNode(Opcodes.IFLE, null);
					methodNodeStack.peekFirst().instructions.add(jmp);
					break;
				default:
					ASTUtils.error(node, "Operator not supported");
					break;
				}
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.ICONST_0));
				LabelNode endLabelNode = new LabelNode();
				methodNodeStack.peekFirst().instructions.add(new JumpInsnNode(Opcodes.GOTO, endLabelNode));
				LabelNode trueLabelNode = new LabelNode();
				jmp.label = trueLabelNode;
				methodNodeStack.peekFirst().instructions.add(trueLabelNode);
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.ICONST_1));
				methodNodeStack.peekFirst().instructions.add(endLabelNode);
			} else if (type.equals(Type.INT_TYPE)) {
				LabelNode trueLabelNode = new LabelNode();
				switch (op) {
				case EQUAL:
				methodNodeStack.peekFirst().instructions.add(new JumpInsnNode(Opcodes.IF_ICMPEQ, trueLabelNode));
					break;
				case NOTEQUAL:
				methodNodeStack.peekFirst().instructions.add(new JumpInsnNode(Opcodes.IF_ICMPNE, trueLabelNode));
					break;
				case GREATER:
				methodNodeStack.peekFirst().instructions.add(new JumpInsnNode(Opcodes.IF_ICMPGT, trueLabelNode));
					break;
				case GREATEREQ:
				methodNodeStack.peekFirst().instructions.add(new JumpInsnNode(Opcodes.IF_ICMPGE, trueLabelNode));
					break;
				case LESS:
				methodNodeStack.peekFirst().instructions.add(new JumpInsnNode(Opcodes.IF_ICMPLT, trueLabelNode));
					break;
				case LESSEQ:
				methodNodeStack.peekFirst().instructions.add(new JumpInsnNode(Opcodes.IF_ICMPLE, trueLabelNode));
					break;
				default:
					break;
				}
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.ICONST_0));
				LabelNode endLabelNode = new LabelNode();
				methodNodeStack.peekFirst().instructions.add(new JumpInsnNode(Opcodes.GOTO, endLabelNode));
				methodNodeStack.peekFirst().instructions.add(trueLabelNode);
				methodNodeStack.peekFirst().instructions.add(new InsnNode(Opcodes.ICONST_1));
				methodNodeStack.peekFirst().instructions.add(endLabelNode);
			} else {
				ASTUtils.error(node, "Cannot compare such types.");
			}
		} else {
			ASTUtils.error(node, "Operator not recognized.");
		}
	}
}

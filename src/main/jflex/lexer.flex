/**
 *  Compilers assignment 2019-2020
 *  Harokopio University of Athens, Dept. of Informatics and Telematics.
 */

import java_cup.runtime.Symbol;

%%

%class VerySimpleKotlinLexer
%unicode
%public
%final
%integer
%line
%column
%cup

%eofval{
    return createSymbol(sym.EOF);
%eofval}

%{

    StringBuffer sb = new StringBuffer();
    
    private Symbol createSymbol(int type) {
        return new Symbol(type, yyline+1, yycolumn+1);
    }

    private Symbol createSymbol(int type, Object value) {
        return new Symbol(type, yyline+1, yycolumn+1, value);
    }

%} 

LineTerminator = \r|\n|\r\n

WhiteSpace     = {LineTerminator} | [ \t\f]

EndOfLineComment = "//"  [^\r\n]* {LineTerminator}?
CommentContent       = ( [^*] | \*+ [^/*] )*
Comment        = "/*" [^*] ~"*/" | "/*"{CommentContent}"*"+"/" | {EndOfLineComment}

Identifier     = [:jletter:] [:jletterdigit:]*

IntegerLiteral = 0 | [1-9][0-9]*

Exponent = [eE][\+\-]?[0-9]+
Float1= [0-9]+ \. [0-9]+ {Exponent}? [fF]?
Float2 =\. [0-9]+ {Exponent}? [fF]?
Float3 = [0-9]+ {Exponent} [fF]?
Float4 = [0-9]+ [fF]
FloatLiteral = {Float1} | {Float2} | {Float3} | {Float4}

%state STRING
%state CHAR

%%
/*YYINITIAL -> The predefined state at which the lexer begins scanning*/
<YYINITIAL> {
    /* reserved keywords */
    "val"           				{ return createSymbol(sym.VAL); 		                             }
    "Int"           				{ return createSymbol(sym.INT); 		                             }
    "Float"         				{ return createSymbol(sym.FLOAT); 	                             }
    "Char"          				{ return createSymbol(sym.CHAR); 		                             }
    "Boolean"       				{ return createSymbol(sym.BOOLEAN); 	                             }
    "String"        				{ return createSymbol(sym.STRING); 	                             }
    "fun"           				{ return createSymbol(sym.FUN); 		                             }
    "if"            				{ return createSymbol(sym.IF); 		                             }
    "var"           				{ return createSymbol(sym.VAR); 		                             }
    "else"          				{ return createSymbol(sym.ELSE); 		                             }
    "true"          				{ return createSymbol(sym.BOOLEAN_LITERAL, Boolean.TRUE); 		                             }
    "false"         				{ return createSymbol(sym.BOOLEAN_LITERAL, Boolean.FALSE); 	                             }
    "for"           				{ return createSymbol(sym.FOR); 		                             }
    "in"            				{ return createSymbol(sym.IN); 		                             }
    "downTo"        				{ return createSymbol(sym.DOWN_TO); 	                             }
    "step"          				{ return createSymbol(sym.STEP); 		                             }
    "while"         				{ return createSymbol(sym.WHILE); 	                             }
    "do"            				{ return createSymbol(sym.DO); 		                             }
    "return"        				{ return createSymbol(sym.RETURN); 	                             }
    "break"         				{ return createSymbol(sym.BREAK); 	                             }
    "continue"      				{ return createSymbol(sym.CONTINUE); 	                             }
    "Unit"          				{ return createSymbol(sym.UNIT); 		                             }
    "is"            				{ return createSymbol(sym.IS); 		                             }
    "null"          				{ return createSymbol(sym.NULL_LITERAL); 		                             }
    "!in"           				{ return createSymbol(sym.NOT_IN); 	                             }
    
    /* Separators */
    "{"                             { return createSymbol(sym.LCURLY);                       }
    "}"                             { return createSymbol(sym.RCURLY);                      }
    ";"                             { return createSymbol(sym.SEMICOLON);                              }
    "("                             { return createSymbol(sym.LPAREN);                       }
    ")"                             { return createSymbol(sym.RPAREN);                      }
    ","                             { return createSymbol(sym.COMMA);                                   }
    ":"                             { return createSymbol(sym.COLON);                                  }


    /* Operators */
    "+"                             { return createSymbol(sym.PLUS);                                   }
    "-"							    { return createSymbol(sym.MINUS);                                  }
    "*"							    { return createSymbol(sym.TIMES);                         }
    "/"							    { return createSymbol(sym.DIVISION);                               }
    "%"                             { return createSymbol(sym.MODULO);                                 }
    "++"                            { return createSymbol(sym.INCREMENT);                              }
    "--"                            { return createSymbol(sym.DECREMENT);                              }
    "="                             { return createSymbol(sym.ASSIGN);                                 }
    "&&"                            { return createSymbol(sym.LOG_AND);                            }
    "||"                            { return createSymbol(sym.LOG_OR);                             }
    "!"                             { return createSymbol(sym.LOG_NOT);                            }
    "=="                            { return createSymbol(sym.EQ);                                 }
    "!="                            { return createSymbol(sym.NEQ);                             }
    "<"                             { return createSymbol(sym.LT);                              }
    ">"                             { return createSymbol(sym.GT);                           }
    "<="                            { return createSymbol(sym.LTEQ);                     }
    ">="                            { return createSymbol(sym.GTEQ);                  }
    "?"                             { return createSymbol(sym.QUESTION);                       }
    ".."                            { return createSymbol(sym.RANGE_TO);                               }

    /* identifiers */ 
    {Identifier}                    { return createSymbol(sym.IDENTIFIER, yytext());                         }

    /* Literals */
    {IntegerLiteral}                { return createSymbol(sym.INTEGER_LITERAL, Integer.valueOf(yytext())); }
    {FloatLiteral}                  { return createSymbol(sym.FLOAT_LITERAL, Double.valueOf(yytext())); }

    /*character literal*/
    \'                              { yybegin(CHAR);                                         }
    /* String literal */
    \"                              { sb.setLength(0); yybegin(STRING);                      }


    /* comments */
    {Comment}                       { /* ignore */                                           }


    /* whitespace */
    {WhiteSpace}                    { /* ignore */                                           }

}

<CHAR>{
    [^\n\r\'\\]\'                   { yybegin(YYINITIAL);  return createSymbol(sym.CHAR_LITERAL, yytext().charAt(0));                                       }
    \\t\'                           { yybegin(YYINITIAL);  return createSymbol(sym.CHAR_LITERAL, '\t');                                                   }
    \\n\'                           { yybegin(YYINITIAL);  return createSymbol(sym.CHAR_LITERAL, '\n');                                                     }
    \\r\'                           { yybegin(YYINITIAL);  return createSymbol(sym.CHAR_LITERAL, '\r');                                                    }
    \\b\'                           { yybegin(YYINITIAL);  return createSymbol(sym.CHAR_LITERAL, '\b');                                                    }
    {LineTerminator}                { throw new RuntimeException("Unterminated string at end of line");                                             }
    /*For better error messaging*/ 
    [^]                             { throw new RuntimeException((yyline+1) + ":" + (yycolumn+1) + ": unclosed character literal <"+ yytext()+">"); } 
}

<STRING> {
    \"                             {
                                        yybegin(YYINITIAL);
                                       return createSymbol(sym.STRING_LITERAL, sb.toString());
                                   }

    [^\n\r\"\\]+                   { sb.append(yytext()); }
    \\t                            { sb.append('\t');     }
    \\n                            { sb.append('\n');    }
    \\b                            { sb.append('\b');     }
    \\r                            { sb.append('\r');     }
    \\\"                           { sb.append('\"');     }
    \\                             { sb.append('\\');     }
    {LineTerminator}               { throw new RuntimeException("Unterminated string literal at end of line"); }
}

/* error fallback */
[^]                                { throw new RuntimeException((yyline+1) + ":" + (yycolumn+1) + ": illegal character <"+ yytext()+">"); }